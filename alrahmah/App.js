import React, { Component } from 'react';
import FastImage from 'react-native-fast-image';
import { Dimensions, View, Image, Platform } from 'react-native';
import { colors } from './src/component/style';
import AppRoot from './src/component/router/Router';
import { OtherContent } from './src/component/Constent/AppConstent';
import firebase, { Notification, NotificationOpen } from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSplashVisible: true,
        };

    }
    async componentDidMount() {
        console.log('---pl----', Platform.OS);
        await this.Call_On_ComponentDidMount();
    }

    async componentWillUnmount() {
        await this.Call_On_ComponentWillUnMount();
    }

    check_permission = async() => {
        const p = await firebase.messaging().hasPermission();
        if (p) {
            this.get_token();
        } else {
            this.get_permission();
        }
    }

    get_permission = async() => {
        try {
            await firebase.messaging().requestPermission();
            this.check_permission();
        } catch (e) {
            console.log(e);
        }
    }

    get_token = async() => {
        const token = await firebase.messaging().getToken();
        if (token) {
            OtherContent.setDeviceToken(token);
            await AsyncStorage.setItem('fcm_token', token);
            console.log("---fcm token----", token);

        } else {
            this.check_permission();
        }
    }

    token_refresh = async() => {
        if (Platform.OS === 'ios') {
            this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken => {
                console.log('----fcm refresh token----', fcmToken);
                OtherContent.setDeviceToken(fcmToken);
                AsyncStorage.setItem('fcm_token', fcmToken);
            });
        } else {
            return true;
        }

    }

    receive_message = async() => {
        this.messageListener = firebase.messaging().onMessage((message1) => {
            if (message1) {
                console.log('----message----', message1);
                const { title, message } = message1._data;
                this.create_notification(title, message);
            } else {
                this.receive_notification();
            }

        });
    }

    create_channel = async() => {
        const channel = await new firebase.notifications.Android.Channel(
                "default_notification_channel_id",
                "default_notification_channel_id",
                firebase.notifications.Android.Importance.High
            ).setDescription("Used for getting reminder notification")
            .setSound('default');

        if (Platform.OS === 'android') {
            firebase.notifications().android.createChannel(channel);
        } else {
            return true;
        }

    }

    receive_notification = async() => {
        if (Platform.OS === 'ios') {
            this.removeNotificationDisplayedListener = firebase.notifications().
            onNotificationDisplayed((notification: Notification) => {
                if (notification) {
                    const { title, body } = notification;
                    console.log("----Notification for ios---", notification);

                    this.create_notification(title, body);
                }


            });
        } else {
            this.removeNotificationListener = firebase.notifications().
            onNotification((notification: Notification) => {
                if (notification) {
                    const { title, body } = notification;
                    console.log("----Notification for android---", notification);
                    this.create_notification(title, body);
                } else {
                    return true;
                }

            });
        }
    }

    create_notification = async(title, body) => {
        const notification = new firebase.notifications.Notification({
                sound: 'default',
                show_in_foreground: true,
            })
            .setNotificationId('1')
            .setSound('default')
            .setTitle(title)
            .setBody(body)
            .android.setPriority(firebase.notifications.Android.Priority.High) // set priority in Android
            .android.setChannelId("default_notification_channel_id") // should be the same when creating channel for Android
            .android.setAutoCancel(true) // To remove notification when tapped on i
            .android.setLargeIcon('ic_lanucher')
            .android.setSmallIcon('ic_launcher');

        firebase.notifications().displayNotification(notification);
    }

    open_notification = async() => {
        this.removeNotificationOpenedListener = firebase.notifications().
        onNotificationOpened((notificationOpen) => {
            // Get the action triggered by the notification being opened
            const action = notificationOpen.action;

            // Get information about the notification that was opened
            const notification = notificationOpen.notification;
            console.log('----NotificationOpen---', notification);

        });
    }

    getInitialNotification = async() => {
        const notificationOpen: NotificationOpen = await firebase.notifications()
            .getInitialNotification();
        if (notificationOpen) {
            // App was opened by a notification
            // Get the action triggered by the notification being opened
            const action = notificationOpen.action;
            // Get information about the notification that was opened
            const notification: Notification = notificationOpen.notification;
            console.log("----AppOpen----", notification);

        }
    }

    Call_On_ComponentDidMount = async() => {
        this.check_permission();
        this.token_refresh();
        this.create_channel();
        this.receive_message();
        this.receive_notification();
        //this.open_notification();
        //this.getInitialNotification();
    }

    Call_On_ComponentWillUnMount = async() => {
        this.token_refresh();
        //this.receive_message();
        //this.receive_notification();
        //this.open_notification();
        //this.getInitialNotification();
    }



    render() {
        return ( <
            AppRoot / >

        );

    }
}


const styles = {
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: colors.green,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logoStyle: {
        height: 250,
        width: 200,
        resizeMode: 'contain',
        position: 'absolute'
    }
}

export default App;