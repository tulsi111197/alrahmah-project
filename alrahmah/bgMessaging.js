import firebase from 'react-native-firebase';
// Optional flow type
import type { RemoteMessage } from 'react-native-firebase';

export default async (message1: RemoteMessage) => {
    const {title, message} = message1_data;
    const channel = await new firebase.notifications.Android.Channel(
        "default_notification_channel_id",
        "default_notification_channel_id",
        firebase.notifications.Android.Importance.High
    ).setDescription("Used for getting reminder notification")
        .setSound('default');
    firebase.notifications().android.createChannel(channel);


    const notification = new firebase.notifications.Notification({
        sound: 'default',
        show_in_foreground: true,
      })
        .setNotificationId('1')
        .setSound('default')
        .setTitle(title)
        .setBody(message)
        .android.setPriority(firebase.notifications.Android.Priority.High) // set priority in Android
        .android.setChannelId("default_notification_channel_id") // should be the same when creating channel for Android
        .android.setAutoCancel(true) // To remove notification when tapped on i
        .android.setLargeIcon('ic_lanucher')
        .android.setSmallIcon('ic_launcher');
  
      firebase.notifications().displayNotification(notification);
      console.log('-----yes------yes');
    return Promise.resolve();
}