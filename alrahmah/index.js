
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import React from 'react';
import reducer from './src/component/store/reducer';
import { createStore } from 'redux';
import App from './App';
import { name as appName } from './app.json';
import bgMessaging from './bgMessaging';
import AppRoot from './src/component/router/Router';
import SplashScreen from './src/component/splash/SplashScreen';
import firebase from 'react-native-firebase';

Appcomponents = () => {
    return (
        <Provider store={createStore(reducer)}>
            <App />
        </Provider>
    )
}

AppRegistry.registerHeadlessTask('AppBackgroundMessage', () => bgMessaging);
AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging);
AppRegistry.registerComponent(appName, () => Appcomponents);
