export class OtherContent {
    static fcm_token;
    static token;

    // links 
    static contact_us = {
        en: 'http://104.248.3.101/alrahmah/#contact-us',
        ar: 'http://104.248.3.101/alrahmah/#contact-us',
    };

    static how_we_work = {
        en: 'http://104.248.3.101/alrahmah/how_we_work',
        ar: 'http://104.248.3.101/alrahmah/how_we_work2'
    };

    static about_us = {
        en: 'http://104.248.3.101/alrahmah/',
        ar: 'http://104.248.3.101/alrahmah/'
    };

    static getDeviceToken() {
        return this.fcm_token
    }
    static setDeviceToken(token) {
        this.fcm_token = token
    }

    static getToken() {
        return this.token
    }
    static setToken(token) {
        this.token = token
    }

    static getContactUSLink() {
        return this.contact_us
    }

    static getAboutUSLink() {
        return this.about_us
    }

    static getHowWeWorkLink() {
        return this.how_we_work
    }
}