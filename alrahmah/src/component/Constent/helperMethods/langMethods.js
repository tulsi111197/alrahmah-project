import React from 'react';


export function getFlexDirection(lang) {
    if (lang == 'ar') {
        return 'row-reverse'
    }
    return 'row'
}

export function getAlignItems(lang) {
    if (lang == 'ar') {
        return 'flex-start'

    }
    return 'flex-end'
}

export function isLangArabic(lang) {
    if (lang == 'ar') {
        return true
    }
    return false
}

export function getTextAlign(lang) {
    if (lang == 'ar') {
        return 'right'
    }
    return 'left'
}

