import { Dimensions } from 'react-native';

export const dimensions = {
  fullWidth: Dimensions.get('window').width,
  fullHeight: Dimensions.get('window').height
}

export const colors = {
  green: '#00cece',
  inputColor: '#F3F3F3',
  white: '#ffffff',
  black: '#000000',
  red: '#FF3737',
  borderColor: '#C7C7C7',
  labelColor: '#3A3A3A',
  textColor: '#707070',
  yellow:'#FFCE00',
  pink:'#EA0968',
  blue:'#00b1ff',
  textMuted:'#707070',
  btnBorder:'#9E9E9E'
}

export const padding = {
  sm: 5,
  md: 10,
  lg: 15,
  xl: 20
}

export const margin = {
  sm: 5,
  md: 10,
  lg: 15,
  xl: 20
}

export const fontFamily = {

}

export const radius = {
  sm: 5,
  md: 10,
  lg: 15
}

export const compHeight = {
  sm: 10,
  md: 20,
  lg: 30,
  xl: 40,
  xxl: 50
}
export const borderWidth = {
  none: 0,
  sm: 1,
  md: 2,
  lg: 3,
  xl: 4,
}

export const fonts = {
  sm: 12,
  md: 14,
  lg: 16,
  xl: 18,
  xxl: 20,
  xml:24,
  xlarge:30
}

export const noShadow = {
  shadowOpacity: 0,
  shadowRadius: 0,
  shadowOffset: {
    height: 0,
    width: 0
  },
  elevation: 0
}
export const shadow = {
  shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 5
}

export const baseStyle = {
  appContainer: {
    height: dimensions.fullHeight,
    width:dimensions.fullWidth,
    backgroundColor: colors.white,
    flex: 1

  },
shadowLg:{
  shadow:shadow
},
  appContent: {
    padding: padding.lg
  },
  contentHeading: {
    fontSize: fonts.xxl,
    marginBottom:margin.md,
  },
  subHeading: {
    fontSize: fonts.lg,
    color: colors.textColor,
    marginBottom:margin.lg
  },
  flexRow: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  flexColumn: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'nowrap',
    flex: 1
  },
  fontMd: {
    fontSize: fonts.md
  },
  fontLg: {
    fontSize: fonts.lg
  },
  marginSm: {
    margin: margin.sm
  },
  marginMd: {
    margin: margin.md
  },
  marginLg: {
    margin: margin.lg
  },
  marginXl: {
    margin: margin.xl
  },
  marginTopSm: {
    marginTop: margin.sm
  },
  marginBottomSm: {
    marginBottom: margin.sm
  },
  marginLeftSm: {
    marginLeft: margin.sm
  },
  marginRightSm: {
    marginRight: margin.sm
  },
  marginTopMd: {
    marginTop: margin.md
  },
  marginBottomMd: {
    marginBottom: margin.md
  },
  marginLeftMd: {
    marginLeft: margin.md
  },
  marginRightMd: {
    marginRight: margin.md
  },
  marginTopLg: {
    marginTop: margin.lg
  },
  marginBottomLg: {
    marginBottom: margin.lg
  },
  marginLeftLg: {
    marginLeft: margin.lg
  },
  marginRightLg: {
    marginRight: margin.lg
  },
  marginTopXl: {
    marginTop: margin.xl
  },
  marginBottomXl: {
    marginBottom: margin.xl
  },
  marginLeftXl: {
    marginLeft: margin.xl
  },
  marginRightXl: {
    marginRight: margin.xl
  },
  marginVerticalSm: {
    marginVertical: margin.sm
  },
  marginVerticalMd: {
    marginVertical: margin.md
  },
  marginVerticalLg: {
    marginVertical: margin.lg
  },
  marginVerticalXl: {
    marginVertical: margin.xl
  },
  marginHorizontalSm: {
    marginHorizontal: margin.sm
  },
  marginHorizontalMd: {
    marginHorizontal: margin.md
  },
  marginHorizontalLg: {
    marginHorizontal: margin.lg
  },
  marginHorizontalXl: {
    marginHorizontal: margin.xl
  },
  marginZero: {
    marginBottom: 0,
    marginTop: 0,
    marginRight: 0,
    marginLeft: 0
  },
  containerFluid: {
    paddingVertical: padding.lg,
    flex: 1,
    backgroundColor: colors.white
  },
  containerFull: {
    backgroundColor: colors.white
  },
  section: {
    paddingVertical: padding.lg,
    paddingHorizontal: padding.md
  },

  paddingSm: {
    padding: padding.sm
  },
  paddingMd: {
    padding: padding.md
  },
  paddingLg: {
    padding: padding.lg
  },
  paddingXl: {
    padding: padding.xl
  },
  paddingTopSm: {
    paddingTop: padding.sm
  },
  paddingBottomSm: {
    paddingBottom: padding.sm
  },
  paddingLeftSm: {
    paddingLeft: padding.sm
  },
  paddingRightSm: {
    paddingRight: padding.sm
  },
  paddingTopMd: {
    paddingTop: padding.md
  },
  paddingBottomMd: {
    paddingBottom: padding.md
  },
  paddingLeftMd: {
    paddingLeft: padding.md
  },
  paddingRightMd: {
    paddingRight: padding.md
  },
  paddingTopLg: {
    paddingTop: padding.lg
  },
  paddingBottomLg: {
    paddingBottom: padding.lg
  },
  paddingLeftLg: {
    paddingLeft: padding.lg
  },
  paddingRightLg: {
    paddingRight: padding.lg
  },
  paddingTopXl: {
    paddingTop: padding.xl
  },
  paddingBottomXl: {
    paddingBottom: padding.xl
  },
  paddingLeftXl: {
    paddingLeft: padding.xl
  },
  paddingRightXl: {
    paddingRight: padding.xl
  },
  paddingVerticalSm: {
    paddingVertical: padding.sm
  },
  paddingVerticalMd: {
    paddingVertical: padding.md
  },
  paddingVerticalLg: {
    paddingVertical: padding.lg
  },
  paddingVerticalXl: {
    paddingVertical: padding.xl
  },
  paddingHorizontalSm: {
    paddingHorizontal: padding.sm
  },
  paddingHorizontalMd: {
    paddingHorizontal: padding.md
  },
  paddingHorizontalLg: {
    paddingHorizontal: padding.lg
  },
  paddingHorizontalXl: {
    paddingHorizontal: padding.xl
  },
  paddingZero: {
    padding: 0
  },
  marginSm: {
    margin: margin.sm
  },
  marginMd: {
    margin: margin.md
  },
  marginLg: {
    margin: margin.lg
  },
  marginXl: {
    margin: margin.xl
  },
  marginTopSm: {
    marginTop: margin.sm
  },
  marginBottomSm: {
    marginBottom: margin.sm
  },
  marginLeftSm: {
    marginLeft: margin.sm
  },
  marginRightSm: {
    marginRight: margin.sm
  },
  marginTopMd: {
    marginTop: margin.md
  },
  marginBottomMd: {
    marginBottom: margin.md
  },
  marginLeftMd: {
    marginLeft: margin.md
  },
  marginRightMd: {
    marginRight: margin.md
  },
  marginTopLg: {
    marginTop: margin.lg
  },
  marginBottomLg: {
    marginBottom: margin.lg
  },
  marginLeftLg: {
    marginLeft: margin.lg
  },
  marginRightLg: {
    marginRight: margin.lg
  },
  marginTopXl: {
    marginTop: margin.xl
  },
  marginBottomXl: {
    marginBottom: margin.xl
  },
  marginLeftXl: {
    marginLeft: margin.xl
  },
  marginRightXl: {
    marginRight: margin.xl
  },
  marginVerticalSm: {
    marginVertical: margin.sm
  },
  marginVerticalMd: {
    marginVertical: margin.md
  },
  marginVerticalLg: {
    marginVertical: margin.lg
  },
  marginVerticalXl: {
    marginVertical: margin.xl
  },
  marginHorizontalSm: {
    marginHorizontal: margin.sm
  },
  marginHorizontalMd: {
    marginHorizontal: margin.md
  },
  marginHorizontalLg: {
    marginHorizontal: margin.lg
  },
  marginHorizontalXl: {
    marginHorizontal: margin.xl
  },
  marginZero: {
    marginBottom: 0,
    marginTop: 0,
    marginRight: 0,
    marginLeft: 0
  },
  radiusXs: {
    borderRadius: radius.xs
  },
  radiusSm: {
    borderRadius: radius.sm
  },
  radiusMd: {
    borderRadius: radius.md
  },
  radiusLg: {
    borderRadius: radius.lg
  },
  radiusXl: {
    borderRadius: radius.xl
  },
  radiusXxl: {
    borderRadius: radius.xxl
  },
  radiusCircle: {
    borderRadius: radius.circle
  },
  borderWidthNone: {
    borderWidth: borderWidth.none
  },
  borderWidthSm: {
    borderWidth: borderWidth.sm
  },
  borderWidthMd: {
    borderWidth: borderWidth.md
  },
  borderWidthLg: {
    borderWidth: borderWidth.lg
  },
  borderWidthXl: {
    borderWidth: borderWidth.xl
  },

  borderWhiteColor: {
    borderColor: colors.white
  },
  borderBlackColor: {
    borderColor: colors.black
  },
  bgBlackColor: {
    backgroundColor: colors.black
  },
  bgLabelBlackColor: {
    backgroundColor: colors.labelColor
  },
  bgOffBlackColor: {
    backgroundColor: colors.offBlack
  },
  bgWhiteColor: {
    backgroundColor: colors.white
  },
bgYellowColor:{
  backgroundColor:colors.yellow
},
bgPinkColor:{
  backgroundColor:colors.pink
},
bgBlueColor:{
  backgroundColor:colors.blue
},
bgRedColor:{
  backgroundColor:colors.red
},
  borderGreenColor: {
    borderColor: colors.green
  },
  fontsSm: {
    fontSize: fonts.sm
  },
  fontsMd: {
    fontSize: fonts.md
  },
  fontsLg: {
    fontSize: fonts.lg
  },
  fontsXl: {
    fontSize: fonts.xl
  },
  lightText: {
    color: colors.textMuted
  },

  whiteText: {
    color: colors.white
  },
  blackText: {
    color: colors.black
  },

  yellowText: {
    color: colors.yellow
  },
  greenText: {
    color: colors.green
  },
  compHeightSm: {
    height: compHeight.sm
  },
  compHeightMd: {
    height: compHeight.md
  },
  compHeightLg: {
    height: compHeight.lg
  },
  compHeightXl: {
    height: compHeight.xl
  },
  compHeightXxl: {
    height: compHeight.xxl
  },
  compHeighthp: {
    height: '100%'
  },
  fullHeight: {
    height: dimensions.fullHeight
  },
  fullWidth: {
    width: dimensions.fullWidth
  },
  hpWidth: {
    width: '100%'
  },
  flexOne: {
    flex: 1
  },
  row: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  colOne: {
    width: '25%',
  },
  colTow: {
    width: '50%',
  },
  colThree: {
    width: '75%',
    paddingRight: 10
  },
  colFour: {
    width: '100%',
    paddingRight: 10
  },
  colFive: {
    width: '33.333%',
    paddingRight: 10
  },
  flexRow: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  flexColumn: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'nowrap',
    flex: 1
  },
  justifyContentStart: {
    justifyContent: 'flex-start'
  },
  justifyContentCenter: {
    justifyContent: 'center'
  },
  justifyContentBetween: {
    justifyContent: 'space-between'
  },
  justifyContentAround: {
    justifyContent: 'space-around'
  },
  justifyContentEnd: {
    justifyContent: 'flex-end'
  },
  alignItemsCenter: {
    alignItems: 'center'
  },

  alignItemsEnd: {
    alignItems: 'flex-end'
  },
  alignItemsStretch: {
    alignItems: 'stretch'
  },

  alignSelfCenter: {
    alignSelf: 'center'
  },
  alignSelfEnd: {
    alignSelf: 'flex-end'
  },
  alignSelfStart: {
    alignSelf: 'flex-start'
  },

  itemHorizontalCenter: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center'
  },
  ItemRowStyle: {
    flexDirection: 'row',
    paddingHorizontal: padding.xl,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  itemVerticalCenter: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'nowrap',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  media: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  mediaLeft: {
    marginRight: 15
  },
  mediaRight: {
    marginLeft: 15
  },

  mediaBody: {
    flex: 1,
  },
  textAlignRight: {
    textAlign: 'right'
  },
  textAlignLeft: {
    textAlign: 'left'
  },
  textAlignCenter: {
    textAlign: 'center'
  },

};

