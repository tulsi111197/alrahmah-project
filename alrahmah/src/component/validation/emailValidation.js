import React from 'react';

function emailValidation(mail) {
    if (mail == '')
        return 'required'
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
        return ''
    else
        return 'Invalid Email'
}
export default emailValidation;
