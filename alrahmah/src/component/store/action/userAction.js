export const setUser = (type,value) => {
    return {
        type : type,
        payload : value
    }
}

export const setError = (type,value)=> {
     return {
         type : type,
         payload : value
     }
}

export const resetError = () => {
    return {
        type : 'RESET_ERRORS'   
    }
}
