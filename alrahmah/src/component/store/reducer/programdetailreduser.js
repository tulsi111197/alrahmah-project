initState = {
    programDetail: []
}

export default programDetailReduser = (state = 0, action) => {
    switch (action.type) {
        case 'SET_API_PROGRAM_DETAIL':
            return { ...state, programDetail: action.payload }
        default:
            return state;
    }

}
