import { Arabic, English } from "../../../static language/staticlanguage"

export const setUser = (type, value) => {
    return {
        type: type,
        payload: value
    }
}

export const setError = (type, value) => {
    return {
        type: type,
        payload: value
    }
}

export const setLanguageArabic = () => {
    return {
        type: 'CHANGE_LANGUAGE',
        payload: 'ar',
    }
}

export const setLanguageEnglish = () => {
    return {
        type: 'CHANGE_LANGUAGE',
        payload: 'en',
    }
}

export const set_Arabic = () => {
    return {
        type: 'ar',
        payload: Arabic,
    }
}

export const set_English = () => {
    return {
        type: 'en',
        payload: English
    }
}
