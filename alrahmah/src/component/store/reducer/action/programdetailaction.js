export const setProgramDetails = (apijson) => {
    return {
        type: 'SET_API_PROGRAM_DETAIL',
        payload: apijson
    }
}
