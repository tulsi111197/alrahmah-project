export const setStoryDetails = (apijson) => {
    return {
        type: 'SET_API_STORY_DETAIL',
        payload: apijson
    }
}
