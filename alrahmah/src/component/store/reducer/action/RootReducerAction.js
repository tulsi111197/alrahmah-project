import { Arabic, English } from '../../../static language/staticlanguage';

export const setRootReduser = (apijson) => {
    return {
        type: 'SET_API_ROOT_REDUCER',
        payload: apijson
    }
}

export const setMyDonation = (donation) => {
    return {
        type: 'SET_MY_DONATION',
        payload: donation,
    }
}

export const setProgramList = (program) => {
    return {
        type: 'SET_PROGRAM_LIST',
        payload: program,
    }
}

export const setBlogList = (blog) => {
    return {
        type: 'SET_BLOG_LIST',
        payload: blog,
    }
}

export const setStoryList = (story) => {
    return {
        type: 'SET_STORY_LIST',
        payload: story,
    }
}

export const setMyProgram = (myprogram) => {
    return {
        type: 'SET_MY_PROGRAM',
        payload: myprogram,
    }
}


export const setStaticLangArabic = () => {
    return {
        type: 'SET_USER_LANGUAGE',
        payload: Arabic
    }
}

export const setStaticLangEnglish = () => {
    return {
        type: 'SET_USER_LANGUAGE',
        payload: English
    }
}

export const setTotalDonateAmount = (amount) => {
    return {
        type: 'UPDATE_DONATE_AMOUNT',
        payload: amount
    }
}



