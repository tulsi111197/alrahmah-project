export const setBlogDetails = (apijson) => {
    return {
        type: 'SET_API_BLOG_DETAIL',
        payload: apijson
    }
}
