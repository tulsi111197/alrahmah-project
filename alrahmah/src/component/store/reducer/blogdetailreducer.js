initState = {
    blogDetail: []
}

export default programDetailReduser = (state = 0, action) => {
    switch (action.type) {
        case 'SET_API_BLOG_DETAIL':
            return { ...state, blogDetail: action.payload }
        default:
            return state;

    }
}
