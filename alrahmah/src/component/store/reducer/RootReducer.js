const initState = {
    dashboardresponse: [],
    mydonation: [],
    programlist: [],
    bloglist: [],
    storylist: [],
    myprogram: [],
    userLanguage: '',
    lang: ''
}

export default RootReducer = (state = initState, action) => {
    switch (action.type) {
        case 'SET_API_ROOT_REDUCER':
            return { ...state, dashboardresponse: action.payload }
        case 'SET_PROGRAM_LIST':
            return { ...state, programlist: action.payload }
        case 'SET_MY_DONATION':
            return { ...state, mydonation: action.payload }
        case 'SET_MY_PROGRAM':
            return { ...state, myprogram: action.payload }
        case 'SET_BLOG_LIST':
            return { ...state, bloglist: action.payload }
        case 'SET_STORY_LIST':
            return { ...state, storylist: action.payload }
        case 'UPDATE_DONATE_AMOUNT':
            let dashboard = state.dashboardresponse;
            dashboard.total_donate = action.payload
            return { ...state, dashboardresponse: dashboard }
        case 'SET_USER_LANGUAGE':
            return { ...state, userLanguage: action.payload }
        case 'ar':
            return{
                ...state, userLanguage: action.payload
            }
        case 'en':
            return{
                ...state, userLanguage: action.payload
            } 
        default:
            return state;
    }
    return state;

}
