initState = {
    f_name: '',
    l_name: '',
    image: false,
    mobile: '',
    city: '',
    state: '',
    username: '',
    address: '',
    userlanguage: 'en',
    email: '',
    password: '',
    confirmpassword: '',
    passworderror: '',
    confirmpassworderror: '',
    emailerror: '',
    nameerror: '',
    token: '',
    countery: ''
}

export default UserDetail = (state = initState, action) => {
    switch (action.type) {
        case 'USERNAME':
            return { ...state, username: action.payload }
        case 'IMAGE_URI':
            return { ...state, image: action.payload }
        case 'F_NAME':
            return { ...state, f_name: action.payload }
        case 'Countery':
            return { ...state, countery: action.payload }
        case 'L_NAME':
            return { ...state, l_name: action.payload }
        case 'MOBILE':
            return { ...state, mobile: action.payload }
        case 'ADDRESS':
            return { ...state, address: action.payload }
        case 'CITY':
            return { ...state, city: action.payload }
        case 'STATE':
            return { ...state, state: action.payload }
        case 'TOKEN':
            return { ...state, token: action.payload }
        case 'EMAIL':
            return { ...state, email: action.payload }
        case 'PASSWORD':
            return { ...state, password: action.payload }
        case 'CONFIRMPASSWORD':
            return { ...state, confirmpassword: action.payload }
        case 'CHANGE_LANGUAGE':
            return { ...state, userlanguage: action.payload }
        case 'NAME_ERROR':
            return { ...state, nameerror: action.payload }
        case 'EMAIL_ERROR':
            return { ...state, emailerror: action.payload }
        case 'PASSWORD_ERROR':
            return { ...state, passworderror: action.payload }
        case 'CONFIRMPASSWORD_ERROR':
            return { ...state, confirmpassworderror: action.payload }
        case 'RESET_ERRORS':
            return {
                ...state,
                confirmpassworderror: '',
                passworderror: '',
                emailerror: '',
                nameerror: ''
            }
        case 'ar':
            return {
                ...state, userlanguage: 'ar'
            }
        case 'en':
            return {
                ...state, userlanguage: 'en'
            }
        default:
            return state;
    }
}