initState = {
    SotryDetail: []
}

export default storyDetailReduser = (state = initState, action) => {
    switch (action.type) {
        case 'SET_API_STORY_DETAIL':
            return { ...state, SotryDetail: action.payload }
        default:
            return state;

    }

}
