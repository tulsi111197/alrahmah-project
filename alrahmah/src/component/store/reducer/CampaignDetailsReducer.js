initState = {
    campaignDetail: []
}

export default campaignDetailReduser = (state = 0, action) => {
    switch (action.type) {
        case 'SET_API_CAPMPAIGN_DETAIL':
            return { ...state, campaignDetail: action.payload }
        default:
            return state;

    }

}
