import { combineReducers } from 'redux';
import RootReducer from './RootReducer';
import UserReduser from './UserReducer';
import CampaingDetailsReduser from './CampaignDetailsReducer';
import ProgramDetailReduser from './programdetailreduser';
import BlogDetail from './blogdetailreducer';
import StoryDetailReduser from './sotryDetail';

export default combineReducers({
    rootReducer: RootReducer,
    campaingDetails: CampaingDetailsReduser,
    userDetail: UserReduser,
    programDetails: ProgramDetailReduser,
    blogDetail : BlogDetail,
    storyDetail : StoryDetailReduser,

})