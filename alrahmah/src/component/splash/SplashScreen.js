import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { Dimensions, View, Image } from 'react-native';
import { connect } from 'react-redux';
import FastImage from 'react-native-fast-image';
import { colors } from '../style';
import {
    dashboard, mydonationuri, programlist, bloglist, storylist, myprogramlist,
    campaingdetails, prgramdetail
} from '../api utility/apiutility';
import {
    setStaticLangEnglish, setStaticLangArabic, setRootReduser,
    setMyDonation, setProgramList, setBlogList, setStoryList, setMyProgram
} from '../store/reducer/action/RootReducerAction';
import { setLanguageArabic, setLanguageEnglish, setUser } from '../store/reducer/action/userAction';
import fetchUserDataByApi from '../api unitlity function/fetchuserdata';
import firebase, { Notification, NotificationOpen } from 'react-native-firebase';
import { OtherContent } from '../Constent/AppConstent';
import { setCampaignDetails } from '../store/reducer/action/campaingDetailsAction';
import { setProgramDetails } from '../store/reducer/action/programdetailaction';

class SplashScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            status: '',
            timeout: '',
            token: '',
            lang: 'en',
        };

    }

    async componentDidMount() {
        this.openNotification();
        const status = await this.getAppStatus('STATUS');
        const timeout = await this.getAppStatus('TIME');
        let token = await this.getAppStatus('APITOKEN');
        OtherContent.setToken(token);
        this.setState({ token });
        await this.getLanguage();
        this.setState({ status });
        this.setState({ timeout });
        //await this.serviceWithoutLogin();
        await this.renderViewAsyncStorage();
    }

    fetchDetailsInRedux = async (token, id, uri) => {
        let url = `${uri}${id}`;
        let result = await fetch(url, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`,
                'X-localization': this.state.lang ? this.state.lang : 'en',
            }
        })
        let response = await result.json();
        return response;
    }


    openNotification = async () => {
        this.removeNotificationOpenedListener = firebase.notifications().
            onNotificationOpened(async notificationOpen => {

                console.log('---open----', notificationOpen);

                if(this.state.status === 'LoggedIn')
                {
                    let url = null;
                    if (true) {
                        url = prgramdetail;
                    }
                    else {
                        url = campaingdetails;
                    }
                    let response = await this.fetchDetailsInRedux(null, 1, url);
                    if (true) {
                        await this.props.setProgramDetails(response.data);
                        this.props.navigation.navigate('programDetail');
                    }
                    else {
                        await this.props.setCampaignDetails(response.data);
                        this.props.navigation.navigate('campaignDetail');
                    }
                }
                else
                {
                    console.log('jjj');
                }
                
            });
    }
    getAppStatus = (status) => {
        const value = AsyncStorage.getItem(status);
        return value;
    }

    getLanguage = async () => {
        const language = await this.getAppStatus('LANGUAGE');
        // this.setState({ lang: language });
        if (language === 'ar') {
            this.setState({ lang: 'ar' });
            await this.props.setLanguageArabic();
            await this.props.setStaticLangArabic();
        }
        else {
            this.setState({ lang: 'en' });
            await this.props.setLanguageEnglish();
            await this.props.setStaticLangEnglish();
        }
    }


    serviceWithoutLogin = async () => {
        let response = await fetchUserDataByApi
            (dashboard, null, this.state.lang);
        let program = await fetchUserDataByApi
            (programlist, null, this.state.lang);
        let blog = await fetchUserDataByApi
            (bloglist, null, this.state.lang);
        let story = await fetchUserDataByApi
            (storylist, null, this.state.lang);
        await this.props.setStoryList
            (story.data);
        await this.props.setBlogList
            (blog.data);
        await this.props.setProgramList
            (program.data.reverse());
        await this.props.setRootReduser
            (response.data);
    }

    renderViewAsyncStorage = async () => {
        switch (this.state.status) {
            case 'LoggedIn':
                try {
                    await this.serviceWithoutLogin();
                    this.props.navigation.navigate('dashboard');
                }
                catch (error) {
                    this.props.navigation.navigate('welcome');
                }
                return;
            case 'VerifyScreen':
                const timeout = new Date().getTime() - this.state.timeout;
                if (timeout < 60000 * 3)
                    this.props.navigation.navigate('verify');
                else
                    this.props.navigation.navigate('welcome');
                return;
            default:
                this.props.navigation.navigate('welcome');
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <FastImage />
                <Image source={require('../assets/logo.png')} style={styles.logoStyle} />
            </View>
        )
    }
}

const styles = {
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: colors.white,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logoStyle: {
        height: 250,
        width: 200,
        resizeMode: 'contain',
        position: 'absolute'
    }
}

const mapStateToProp = state => {
    return {
        response: state,

    }
}

export default connect(mapStateToProp, {
    setUser, setStaticLangArabic, setCampaignDetails,
    setStaticLangEnglish, setMyProgram, setStoryList, setProgramList, setBlogList, setRootReduser,
    setLanguageArabic, setMyDonation, setLanguageEnglish, setProgramDetails
})(SplashScreen);    