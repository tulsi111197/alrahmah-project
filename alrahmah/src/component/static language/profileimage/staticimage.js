import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import AppAvatar from '../../utitlity/AppAvatar';
import { baseStyle } from '../../style';
import FastImage from 'react-native-fast-image';

class ProfileImage extends Component {
    constructor() {
        super();
    }
    render() {
        if (this.props.profile != 'false' && this.props.profile != '"false"') {
            return (
                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('account')}>
                    <FastImage
                        source={{ uri: this.props.profile ? this.props.profile.replace(/"/g, '') : this.props.profile }}
                        style={{
                            width: 40,
                            height: 40,
                            borderRadius: 100 / 2,
                            ...baseStyle.marginLeftLg
                        }}
                    />
                </TouchableWithoutFeedback>
            )
        }
        else
            return (
                <AppAvatar source={require('../../assets/user-account.png')}
                    containerStyle={baseStyle.marginLeftLg}
                    onPress={() => this.props.navigation.navigate('account')} />
            )
    }
}

const mapStateToProp = state => {
    return {
        profile: state.userDetail.image
    }
}

export default connect(mapStateToProp)(ProfileImage);