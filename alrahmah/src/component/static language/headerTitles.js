import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

class HeaderTitle extends Component {
    constructor(props) {
        super(props)

    }
    componentDidMount() {
    }
    render() {
        switch (this.props.screen) {
            case 'edit':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>ملفي</Text> : <Text style={style.headerStyle}>My Profile</Text>
            case 'My Donation':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>تبرعي</Text> : <Text style={style.headerStyle}>My Donation</Text>
            case 'My Program':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>برنامجي</Text> : <Text style={style.headerStyle}>My Program</Text>
            case 'My Campaign':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>حملتي</Text> : <Text style={style.headerStyle}>My Campaign</Text>
            case 'Settings':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>الإعدادات</Text> : <Text style={style.headerStyle}>Settings</Text>
            case 'Language Setting':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>إعدادات اللغة</Text> : <Text style={style.headerStyle}>Language Settings</Text>
            case 'My Account':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>حسابي</Text> : <Text style={style.headerStyle}>My Account</Text>
            case 'Treatment Needy':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>علاج المحتاجين</Text> : <Text style={style.headerStyle}>Treatment Needy</Text>
            case 'Program':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>برنامج</Text> : <Text style={style.headerStyle}>Program</Text>
            case 'Donate Now':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>تبرع الآن</Text> : <Text style={style.headerStyle}>Donate Now</Text>
            case 'Payment Done':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>تم الدفع</Text> : <Text style={style.headerStyle}>Payment Done</Text>
            case 'Become a Volunteer':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>تصبح متطوعا</Text> : <Text style={style.headerStyle}>Become a Volunteer</Text>
            case 'Blog & Stories':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>بلوق وقصص</Text> : <Text style={style.headerStyle}>Blog & Stories</Text>
            case 'upcoming_program':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>البرامج القادمة</Text> : <Text style={style.headerStyle}>Upcoming Programmes</Text>
            case 'History':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>التاريخ</Text> : <Text style={style.headerStyle}>History</Text>
            case 'Blog':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>مدونة</Text> : <Text style={style.headerStyle}>Blog</Text>
            case 'Stories':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>قصص</Text> : <Text style={style.headerStyle}>Stories</Text>
            case 'howwework':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>كيف نعمل</Text> : <Text style={style.headerStyle}>How We Work</Text>
            case 'aboutus':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>معلومات عنا</Text> : <Text style={style.headerStyle}>About Us</Text>
            case 'contactus':
                return this.props.language == 'ar' ? <Text style={style.headerStyle}>اتصل بنا</Text> : <Text style={style.headerStyle}>ContactUs</Text>
        }
    }
}

const style = StyleSheet.create({
    headerStyle: {
        fontSize: 22,
    }
})


const mapStateToProps = state => {
    return {
        language: state.userDetail.userlanguage,
    }
}

export default connect(mapStateToProps)(HeaderTitle);