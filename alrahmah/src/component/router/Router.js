import React from 'react';
import { Image } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { colors, padding, baseStyle, margin } from '../style';
import AppAvatar from '../utitlity/AppAvatar';
import ErrorScreen from '../authScreen/errorScreen/ErrorScreen';
import HeaderLogo from '../utitlity/HeaderLogo';
import WelcomScreen from '../authScreen/onBoardScreen/WelcomeScreen';
import SignUpScreen from '../authScreen/onBoardScreen/SignUpScreen';
import LoginScreen from '../authScreen/onBoardScreen/LoginScreen';
import ForgotPasswordScreen from '../authScreen/onBoardScreen/ForgotPasswordScreen';
import NewPasswordScreen from '../authScreen/onBoardScreen/NewPasswordScreen';
import VerifyScreen from '../authScreen/onBoardScreen/VerifyScreen';
import VerifyScreen2 from '../authScreen/onBoardScreen/verifyScreen2';
import TermsCondition from '../authScreen/onBoardScreen/TermsCondition';
import Dashboard from '../authScreen/campaignScreen/Dashboard';
import MyAccount from '../authScreen/MyAccount/MyAccount';
import EditProfile from '../authScreen/MyAccount/EditProfile';
import ProfileSettings from '../authScreen/MyAccount/ProfileSettings';
import ProfileLanguage from '../authScreen/MyAccount/ProfileLanguage';
import MyCampaign from '../authScreen/MyAccount/MyCampaign';
import MyDonation from '../authScreen/MyAccount/MyDonation';
import Program from '../authScreen/campaignScreen/Program';
import ProgramDetail from '../authScreen/campaignScreen/ProgramDetail';
import BecomeVolunteer from '../authScreen/campaignScreen/BecomeVolunteer';
import BlogDetail from '../authScreen/Blog&Story/BlogDetail';
import StoryDetail from '../authScreen/Blog&Story/StoryDetail';
import TreatmentNeedy from '../authScreen/campaignScreen/TreatmentNeddy';
import CampaignDetail from '../authScreen/campaignScreen/CampaignDetail';
import DonateNow from '../authScreen/campaignScreen/DonateNow';
import PaymentDone from '../authScreen/campaignScreen/PaymentDone';
import CreateNewCampaign from '../authScreen/campaignScreen/CreateNewCampaign';
import TabRoot from '../authScreen/MyAccount/MyProgram';
import BlogRoot from '../authScreen/Blog&Story/BlogAndStory';
import SplashScreen from '../splash/SplashScreen';
import HeaderTitle from '../static language/headerTitles';
import ProfileImage from '../static language/profileimage/staticimage';
import HowWeWord from '../authScreen/MyAccount/History';
import AboutUs from '../authScreen/MyAccount/AboutUs';
import ContactUs from '../../component/authScreen/MyAccount/contact_us';
import AppLanguage from '../utitlity/AppLanguage';
const AuthLoad = createStackNavigator({
    SplashScreen: {
        screen: SplashScreen,
        navigationOptions: {
            header: null
        }
    },
});

const Authetication = createStackNavigator(
    {
        welcome: {
            screen: WelcomScreen,
            navigationOptions: {
                headerBackTitleStyle: { color: 'white' },
                headerLeft: (() => <HeaderLogo />),
                headerRight: (() => <AppLanguage />),
                headerStyle: { borderBottomColor: 'transparent', alignItems: 'center', justifyContent: 'space-between' }
            }
        },
        Error: {
            screen: ErrorScreen,
            navigationOptions: {
                headerShown: false,
                headerBackTitleStyle: { color: 'white' },
            },
        },
        create: {
            screen: SignUpScreen,
            navigationOptions: {
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitle: 'hello',
                headerBackTitleStyle: { color: 'white' },
            }
        },
        login: {
            screen: LoginScreen,
            navigationOptions: {
                headerBackTitleStyle: { color: 'white' },
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
            }
        },
        forgotPassword: {
            screen: ForgotPasswordScreen,
            navigationOptions: {
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerBackTitle: ' '
            }
        },
        newPassword: {
            screen: NewPasswordScreen,
            navigationOptions: {
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
            }
        },
        verify: {
            screen: VerifyScreen,
            navigationOptions: {
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
            }
        },
        verify2: {
            screen: VerifyScreen2,
            navigationOptions: {
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
            }
        },
        terms: {
            screen: TermsCondition,
            navigationOptions: {
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
            }
        }
    })

const rootStack = createStackNavigator(
    {
        dashboard: {
            screen: Dashboard,
            navigationOptions: ({ navigation }) => ({
                // headerLeft: (() => <AppAvatar source={require('../assets/user-account.png')}
                //     containerStyle={baseStyle.marginLeftLg}
                //     onPress={() => navigation.navigate('account')} />),
                headerLeft: (() => <ProfileImage navigation={navigation} />),
                headerRight: (() => <HeaderLogo imgStyle={styles.headerImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerStyle: { borderBottomColor: 'transparent', alignItems: 'center', flex: 1, justifyContent: 'flex-end' }
            })
        },
        treatmentNeedy: {
            screen: TreatmentNeedy,
            navigationOptions: {
                headerTitle: () => <HeaderTitle screen='Treatment Needy' />,
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
            }
        },
        program: {
            screen: Program,
            navigationOptions: {
                headerTitle: () => <HeaderTitle screen='Program' />,
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerStyle: { borderBottomColor: 'transparent', }
            }
        },
        howwework: {
            screen: HowWeWord,
            navigationOptions: {
                headerTitle: () => <HeaderTitle screen='howwework' />,
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerStyle: { borderBottomColor: 'transparent', }
            }
        },
        aboutus: {
            screen: AboutUs,
            navigationOptions: {
                headerTitle: () => <HeaderTitle screen='aboutus' />,
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerStyle: { borderBottomColor: 'transparent', }
            }
        },
        contactus: {
            screen: ContactUs,
            navigationOptions: {
                headerTitle: () => <HeaderTitle screen='contactus' />,
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerStyle: { borderBottomColor: 'transparent', }
            }
        },
        programDetail: {
            screen: ProgramDetail,
            navigationOptions: {
                headerTransparent: true,
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },

            }
        },
        voulenteer: {
            screen: BecomeVolunteer,
            navigationOptions: {
                headerTitle: () => <HeaderTitle screen='Become a Volunteer' />,
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
            }
        },
        blog: {
            screen: BlogRoot,
            navigationOptions: {
                headerTitle: () => <HeaderTitle screen='Blog & Stories' />,
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
            }
        },
        blogDetail: {
            screen: BlogDetail,
            navigationOptions: ({ navigation }) => ({
                headerTransparent: true,
                headerBackImage: (() => <Image source={require('../assets/back_white.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerStyle: { borderBottomColor: 'transparent', }
            })
        },
        storyDetail: {
            screen: StoryDetail,
            navigationOptions: ({ navigation }) => ({
                headerTransparent: true,
                headerBackImage: (() => <Image source={require('../assets/back_white.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerStyle: { borderBottomColor: 'transparent', }
            })
        },
        campaignDetail: {
            screen: CampaignDetail,
            navigationOptions: ({ navigation }) => ({
                headerTransparent: true,
                headerBackImage: (() => <Image source={require('../assets/back_white.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerStyle: { borderBottomColor: 'transparent', }
            })
        },

        donate: {
            screen: DonateNow,
            navigationOptions: {
                headerTitle: () => <HeaderTitle screen='Donate Now' />,
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerStyle: { borderBottomColor: 'transparent', }
            }
        },
        payment: {
            screen: PaymentDone,
            navigationOptions: {
                headerTitle: () => <HeaderTitle screen='Payment Done' />,
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerStyle: { borderBottomColor: 'transparent', }
            }
        },
        createCampaign: {
            screen: CreateNewCampaign,
            navigationOptions: {
                headerTitle: "Create New Campaign",
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerStyle: { borderBottomColor: 'transparent', }
            }
        },
        account: {
            screen: MyAccount,
            navigationOptions: {
                headerTitle: () => <HeaderTitle screen='My Account' />,
                headerBackImage: (() => <Image source={require('../assets/back_white.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerTitleStyle: { color: colors.white },
                headerStyle: { backgroundColor: colors.green, borderBottomColor: 'transparent', }
            }
        },
        editProfile: {
            screen: EditProfile,
            navigationOptions: {
                headerTitle: () => <HeaderTitle screen='edit' />,
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerStyle: { borderBottomColor: 'transparent', }
            }
        },
        profileSetting: {
            screen: ProfileSettings,
            navigationOptions: {
                headerTitle: () => <HeaderTitle screen="Settings" />,
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerStyle: { borderBottomColor: 'transparent', }
            }
        },
        profileLanguage: {
            screen: ProfileLanguage,
            navigationOptions: {
                headerTitle: () => <HeaderTitle screen="Language Setting" />,
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerStyle: { borderBottomColor: 'transparent', }
            }
        },
        myProgram: {
            screen: TabRoot,
            navigationOptions: {
                headerTitle: () => <HeaderTitle screen="My Program" />,
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerStyle: { borderBottomColor: 'transparent', }
            }
        },
        campaign: {
            screen: MyCampaign,
            navigationOptions: {
                headerTitle: () => <HeaderTitle screen="My Campaign" />,
                headerBackImage: (() => <Image source={require('../assets/back.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerStyle: { borderBottomColor: 'transparent', }
            }
        },
        donation: {
            screen: MyDonation,
            navigationOptions: {
                headerTitle: () => <HeaderTitle screen="My Donation" />,
                headerBackImage: (() => <Image source={require('../assets/back_white.png')} style={styles.backImg} />),
                headerBackTitleStyle: { color: 'white' },
                headerTitleStyle: { color: colors.white },
                headerStyle: { backgroundColor: colors.green, borderBottomColor: 'transparent', }
            }
        },

    },
    {
        initialRouteName: 'dashboard',
        headerBackTitleVisible: false,
        headerLayoutPreset: 'center',
        headerBackTitleVisible: false,
        headerBackTitle: ' '
    }
);

const AppState = createSwitchNavigator({
    AuthLoad: AuthLoad,
    Auth: Authetication,
    Root: rootStack

})

const AppRoot = createAppContainer(AppState)

const styles = {
    backImg: {
        width: 25,
        height: 20,
        resizeMode: 'contain',
        marginLeft: padding.lg
    },
    rightImg: {
        width: 20,
        height: 15,
        resizeMode: 'contain',
        marginRight: padding.lg
    },
    headerImg: {
        height: 80,
        width: 80,
        marginRight: margin.lg
    }
}
export default AppRoot;
