import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, TouchableHighlight, ScrollView } from 'react-native';
import { baseStyle, margin, padding } from '../../style';
import { Text } from 'react-native-elements';
import AppInput from '../../utitlity/AppInput';
import AppButton from '../../utitlity/AppButton';
import { reset_password } from '../../api utility/apiutility';

const langobj = {
    en: {
       
        password: 'Enter password',
        confirm_password: 'Enter confirm password',
        password_not_match: 'Password and confirm password not match',
        password_must: 'Password must be 8 character long'
    },
    ar: {

        password: 'أدخل كلمة المرور',
        confirm_password: 'أدخل تأكيد كلمة المرور',
        password_not_match: 'كلمة السر ليست جيدة',
        password_must: 'يجب أن تتكون كلمة المرور من 8 أحرف'
    }
}


class NewPasswordScreen extends Component {

    state = {
        password: '',
        con_password: '',
        error: ''
    }

    validateUserPassword = () => {
        if (this.state.password.length < 8)
            this.setState({ error: this.props.language == 'ar' ? langobj.ar.password_must : langobj.en.password_must });
        else if (this.state.password != this.state.con_password)
            this.setState({ error: this.props.language == 'ar' ? langobj.ar.password_not_match : langobj.en.password_not_match });
        else this.setState({ error: '' });
    }


    updatePassword = async () => {
        let result = await fetch(reset_password, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.props.email,
                password: this.state.password
            })
        })
        return await result.json();
    }

    navigateUser = async () => {
        await this.validateUserPassword();
        if (this.state.error == '') {
            let result = await this.updatePassword();
            if (result.success) {
                alert('password changed successfully')
                this.props.navigation.navigate('login');
            }
        }
    }

    render() {
        const lang = this.props.language;
        return (
            <View style={baseStyle.appContainer}>
                <ScrollView>
                    <View style={[baseStyle.appContent, baseStyle.paddingLg]}>

                        <Text style={{...baseStyle.contentHeading, textAlign: lang == 'ar' ? 'right' : 'left' }}>{this.props.userlang.create_new_password}</Text>

                        <AppInput
                            label={this.props.userlang.create_new_password}

                            placeholder="**********"
                            inputContainerStyle={{ marginBottom: 0 }}
                            secureTextEntry={true}
                            onSubmitEditing={() => this.conf_password.focus()}
                            onChangeText={(text) => this.setState({ password: text })}
                            lang={this.props.language}
                        />
                        <AppInput
                            label={this.props.userlang.confirm_password}
                            inputRef={ref => this.conf_password = ref}
                            inputContainerStyle={{ marginBottom: 0 }}
                            secureTextEntry={true}
                            placeholder="**********"
                            onChangeText={(text) => this.setState({ con_password: text })}
                            lang={this.props.language}
                        />
                        <Text style={{
                            color: 'red',
                            textAlign: this.props.language == 'ar' ? 'right' : 'left'
                        }} >{this.state.error}</Text>
                        <AppButton
                            title={this.props.userlang.continue}
                            containerStyle={styles.containerStyle}
                            secureTextEntry={true}
                            onPress={() => this.navigateUser()} />
                    </View>
                </ScrollView>
                <View style={styles.btnContainer}>
                    <Text style={baseStyle.textAlignCenter}>{this.props.userlang.by_continoue}</Text>
                    <TouchableHighlight>
                        <Text style={[baseStyle.greenText, baseStyle.textAlignCenter]}>{this.props.userlang.term_and_condition}</Text>
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
}
const styles = {
    btnContainer: {
        justifyContent: 'flex-end',
        flex: 1,
        marginBottom: margin.xl,
        padding: padding.lg
    },
    containerStyle: {
        marginTop: margin.xl
    }
}

const mapStateToProp = state => {
    return {
        email: state.userDetail.email,
        password: state.userDetail.password,
        confirmpassword: state.userDetail.confirmpassword,
        passworderror: state.userDetail.passworderror,
        confirmpassworderror: state.userDetail.confirmpassworderror,
        userlang: state.rootReducer.userLanguage,
        language: state.userDetail.userlanguage,
    }
}


export default connect(mapStateToProp, null)(NewPasswordScreen);