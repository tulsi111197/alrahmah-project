//This is an example code for Net Info// 
import React, { Component } from 'react';
import NetInfo from '@react-native-community/netinfo';
import { AppState, Text, Button, View } from 'react-native';

class NetworkStatus extends Component {
    state = {
        isConnected: false
    }
    async componentDidMount() {
        await NetInfo.fetch().then(state => {
            if (state.isConnected === true) {
                this.setState({ isConnected: true })
            }
            else {
                this.setState({ isConnected: false })
            }
        });
    }

    static networkConnectionInfo = async () => {
        await NetInfo.fetch().then(state => {
            if (state.isConnected === true) {
                this.setState({ isConnected: true })
            }
            else {
                this.setState({ isConnected: false })
            }
        });
    }

    static checkForConnection = () => {
        this.networkconnectionInfo();
        return this.state.isConnected
    }

    render() {
        return (
            <></>
        )
    }
}
export default NetworkStatus;