import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import { baseStyle, margin, padding } from '../../style';
import { Text } from 'react-native-elements';
import AppInput from '../../utitlity/AppInput';
import AppButton from '../../utitlity/AppButton';
import { connect } from 'react-redux';

class TermsCondition extends Component {
    render() {
        const lang = this.props.selected_lag
        return (
            <View style={{ flex: 1 }}>
                <ScrollView>
                    <View style={styles.contentStyle}>
                        <Text style={{ ...baseStyle.contentHeading, textAlign: lang == 'ar' ? 'right' : 'left' }}>{
                            this.props.lang.term_condition
                        }</Text>

                        {
                            this.props.selected_lag == 'en' ?
                                <>
                                    <Text style={baseStyle.subHeading}>
                                        These terms and conditions outline the rules and regulations for the use of the Alrahmah website
                            </Text>

                                    <Text style={baseStyle.subHeading}>
                                        Alrahmah NGO is located at: South Africa
                            </Text>

                                    <Text style={baseStyle.subHeading}>
                                        The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice, and any or all Agreements: “Client”, “You” and “Your” refers to you, the person accessing this website and accepting the Company’s terms and conditions. “The Company”, “Ourselves”, “We”, “Our” and “Us”, refers to our Company. “Party”, “Parties”, or “Us”, refers to both the Client and ourselves, or either the Client or ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner, whether by formal meetings of a fixed duration, or any other means, for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services/products, in accordance with and subject to, prevailing law of South Africa. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.
                            </Text>
                                </>
                                :
                                <>
                                    <Text style={{ ...baseStyle.subHeading, textAlign: lang == 'ar' ? 'right' : 'left' }}>
                                        توضح هذه الشروط والأحكام القواعد واللوائح الخاصة باستخدام موقع الرحمة الإلكتروني
                </Text>

                                    <Text style={{ ...baseStyle.subHeading, textAlign: lang == 'ar' ? 'right' : 'left' }}>
                                        تقع منظمة الرحمة غير الحكومية في: جنوب أفريقيا
                </Text>

                                    <Text style={{ ...baseStyle.subHeading, textAlign: lang == 'ar' ? 'right' : 'left' }}>
                                        تنطبق المصطلحات التالية على هذه الشروط والأحكام وبيان الخصوصية وإشعار إخلاء المسؤولية وأي أو كل الاتفاقيات: يشير "العميل" و "أنت" و "الخاص بك" إليك ، والشخص الذي يدخل إلى هذا الموقع ويقبل بشروط وأحكام الشركة. تشير "الشركة" و "أنفسنا" و "نحن" و "لنا" و "نحن" إلى شركتنا. يشير مصطلح "الطرف" أو "الأطراف" أو "نحن" إلى كل من العميل وأنفسنا أو إما العميل أو أنفسنا. تشير جميع الشروط إلى العرض والقبول والنظر في الدفع اللازم لإجراء عملية مساعدتنا للعميل بالطريقة الأنسب ، سواء عن طريق الاجتماعات الرسمية لمدة محددة ، أو بأي وسيلة أخرى ، لغرض صريح من تلبية احتياجات العميل فيما يتعلق بتقديم خدمات / منتجات الشركة المعلنة ، وفقًا للقانون السائد في جنوب إفريقيا والخضوع له. أي استخدام للمصطلحات المذكورة أعلاه أو كلمات أخرى في صيغة المفرد والجمع والرسملة و / أو هو أو هي ، يتم اعتبارها قابلة للتبادل وبالتالي تشير إلى نفس الشيء.
                </Text>
                                </>

                        }

                        
                    </View>
                </ScrollView>
                <View style={{ flex: 1 }}>
                            <AppButton title={this.props.lang.i_accept}
                                onPress={() => this.props.navigation.navigate('dashboard')}
                                buttonStyle={{alignSelf:'flex-end',paddingHorizontal:80}}
                            />
                        </View>
            </View>

        );
    }
}
const styles = {
    contentStyle: {
        flex: 1,
        padding: padding.lg,
        alignItem: 'center',
    }
}

const mapStateToProp = state => {
    return {
        lang: state.rootReducer.userLanguage,
        selected_lag: state.userDetail.userlanguage,
    }
}

export default connect(mapStateToProp)(TermsCondition);
