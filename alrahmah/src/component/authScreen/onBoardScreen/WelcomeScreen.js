import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Modal, TouchableOpacity, ScrollView, Image, Dimensions, D, ImageBackground, Text, ActivityIndicator, PermissionsAndroid } from 'react-native';
import { dashboard, programlist, bloglist, storylist } from '../../api utility/apiutility';
import { baseStyle, margin, colors, fonts, padding } from '../../style/RootStyle';
import AppButton from '../../utitlity/AppButton';
import { slider, tokenforslider } from '../../api utility/apiutility'
import { FlatList } from 'react-native-gesture-handler';
import { setLanguageArabic, setLanguageEnglish } from '../../store/reducer/action/userAction';
import { setStaticLangArabic, setStaticLangEnglish } from '../../store/reducer/action/RootReducerAction';
import fetchUserDataByApi from '../../api unitlity function/fetchuserdata';
import { setRootReduser, setProgramList, setMyProgram, setBlogList, setStoryList } from '../../store/reducer/action/RootReducerAction';
import AppLanguage from '../../utitlity/AppLanguage';
import FastImage from 'react-native-fast-image';
import AsyncStorage from '@react-native-community/async-storage';
import { color } from 'react-native-reanimated';
import HeaderLogo from '../../utitlity/HeaderLogo';
const w = Dimensions.get('window').width - 10;
const h = Dimensions.get('window').height;

class WelcomeScreen extends Component {

    state = {
        toSlide: '',
        current_slide: 0,
        slide: [],
        isloading: true,
        activity: false
    }

    sliderByAPI = async () => {
        let result = await fetch(slider, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${tokenforslider}`
            },
        })
        return await result.json();

    }

    async componentDidMount() {
        this.props.setLanguageEnglish();
        this.props.setStaticLangEnglish();
        this.requestCameraPermission();
        this.requestStoragePermission();
        let slider = await this.sliderByAPI();
        this.setState({ slide: slider.data });
        setInterval(() => {
            this.setState({ toSlide: this.navigateToSlide() });
        }, 5000);
    }

    renderActivityGuest = () => {
        if (this.state.activity)
            return (
                <Modal
                    visible={true}
                    transparent={true}
                >
                    <View style={{
                        position: 'absolute',
                        backgroundColor: 'rgba(0,0,0,0.3)',
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }} >
                        <ActivityIndicator size="large" color={colors.green} />
                    </View>
                </Modal>

            )
        else
            return null
    }

    serviceWithoutLogin = async () => {
        let response = await fetchUserDataByApi
            (dashboard, null, this.props.selected_language);

        let program = await fetchUserDataByApi
            (programlist, null, this.props.selected_language);

        let blog = await fetchUserDataByApi
            (bloglist, null, this.props.selected_language);
        let story = await fetchUserDataByApi
            (storylist, null, this.props.selected_language);
        await this.props.setStoryList
            (story.data);
        await this.props.setBlogList
            (blog.data);
        await this.props.setProgramList
            (program.data.reverse());
        await this.props.setRootReduser
            (response.data);
    }

    navigateToSlide = () => {
        if (this.state.current_slide == 0) {
            this.setState({ current_slide: 1 });
            return [this.state.slide[0]]
        }
        else if (this.state.current_slide == 1) {
            this.setState({ current_slide: 2 });
            return [this.state.slide[1]]
        }
        else {
            this.setState({ current_slide: 0 });
            return [this.state.slide[2]]
        }
    }

    requestCameraPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA, null
            );
        } catch (err) {
        }
    }

    requestStoragePermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, null
            );
        } catch (err) {
        }
    }

    navigateAsGuest = async () => {
        this.setState({ activity: true });
        await this.serviceWithoutLogin();
        await AsyncStorage.setItem('guest', 'yes');
        await AsyncStorage.setItem('LANGUAGE', this.props.selected_language);
        await AsyncStorage.setItem('STATUS', 'LoggedIn');
        await AsyncStorage.setItem('IMAGE_URI', 'false');
        await AsyncStorage.setItem('EMAIL', '');
        await AsyncStorage.setItem('USERNAME', 'Guest');
        await AsyncStorage.setItem('F_NAME', 'Guest');
        await AsyncStorage.setItem('L_NAME', 'User');
        await AsyncStorage.setItem('MOBILE', '');
        await AsyncStorage.setItem('ADDRESS', '');
        await AsyncStorage.setItem('CITY', '');
        await AsyncStorage.setItem('STATE', '');
        this.setState({ activity: false });

        this.props.navigation.navigate('terms');
    }

    renderSlider = ({ uri, heading }) => {
        return (
            <View>
                <View style={{ paddingTop: 60 }}>
                    {console.log(uri)}
                    <FastImage
                        source={{ uri: uri }}
                        style={{
                            ...styles.imgStyle,

                        }}
                        onLoadEnd={() => this.setState({ isloading: false })}
                    />

                </View>

                <View>
                    <Text style={styles.headingStyle}>
                        {
                            this.props.selected_language == 'en' ?
                                heading.sub_title :
                                heading.sub_title
                        }
                    </Text>
                </View>
            </View>

        )
    }

    renderActivity = () => {
        if (this.state.isloading)
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                    <ActivityIndicator size="large" color={colors.green} />
                </View>
            )
        return null
    }

    updateLanguage = ({ lang }) => {
        if (lang == 'ar') {
            this.props.setStaticLangArabic();
            this.props.setLanguageArabic();
            return 0;
        }
        this.props.setLanguageEnglish();
        this.props.setStaticLangEnglish();
        return 0;
    }

    navigaToOtherScreen = async (navigateto) => {
        await AsyncStorage.setItem('LANGUAGE', this.props.selected_language);
        if (navigateto == 'create')
            this.props.navigation.navigate('create')
        else
            this.props.navigation.navigate('login')
    }


    render() {
        return (
            <View style={[baseStyle.appContainer]}>


                < this.renderActivity />
                <FlatList
                    data={this.state.toSlide}
                    showsHorizontalScrollIndicator={false}
                    scrollEnabled={false}
                    renderItem={({ item }) => <this.renderSlider uri={item.image} heading={item} />}
                />
                <this.renderActivityGuest />

                <View style={{ flex: 1, justifyContent: 'flex-end', marginBottom: 40, paddingHorizontal: padding.lg }}>
                    {/* <View style={{flexDirection:'row',alignItems:'center'}}>
 <Text style={{ textAlign: 'center', fontSize: 17, color: colors.green, }}>
              {
                  this.props.selected_language == 'en'
                      ?
                      'Language'
                      :
                      'لغة'
              }
          </Text>
          <Text> : </Text>
          <TouchableOpacity
              onPress={() => this.updateLanguage({ lang: 'en' })}
          >
              <Text style={{
                  textAlign: 'center', fontSize: 15, color: this.props.selected_language == 'en' ?
                      colors.green
                      :
                      'black'
              }}>
                  English
</Text>
          </TouchableOpacity>
          <Text> /</Text>
          <TouchableOpacity
              onPress={() => this.updateLanguage({ lang: 'ar' })}
          >
              <Text style={{
                  textAlign: 'center', fontSize: 15, color: this.props.selected_language == 'ar' ?
                      colors.green
                      :
                      'black'
              }}>
                  عربى
</Text>
          </TouchableOpacity>
 </View> */}

                    <View style={{ marginBottom: margin.xl }}>
                        <AppButton
                            title={this.props.userlang.create_new_account}
                            onPress={() => this.navigaToOtherScreen('create')}
                            buttonStyle={{ alignSelf: 'flex-end', paddingHorizontal: 40 }}
                            containerStyle={{ paddingBottom: padding.xl }}
                        />
                        {/* <Text style={styles.bottomText}>{this.props.userlang.already_have_account} </Text> */}
                        <AppButton
                            style={styles.loginText}
                            title={this.props.userlang.login}
                            buttonStyle={styles.loginButton}
                            onPress={() => this.navigaToOtherScreen('')}
                        />
                    </View>


                    <TouchableOpacity onPress={() => this.navigateAsGuest()} style={{ alignSelf: 'center' }}>
                        <Text style={styles.loginText}>{
                            this.props.selected_language == 'en' ?
                                'Skip'
                                :
                                'تخطى'
                        }
                        </Text>
                    </TouchableOpacity>

                </View>
            </View>

        );
    }
}
const styles = {

    headingStyle: {
        color: colors.black,
        fontSize: 24,
        textAlign: 'center',
        textTransform: 'lowercase',
        padding: padding.lg,
    },

    bottomText: {
        fontSize: fonts.lg,
        textAlign: 'center',
    },
    imgStyle: {
        height: h - 530,
        width: 410,
        resizeMode: 'contain',
        alignSelf: 'center',
        borderTopLeftRadius: 530 / 2,
        borderBottomLeftRadius: 530 / 2
    },
    loginButton: {
        alignSelf: 'flex-start',
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30,
        paddingHorizontal: 80
    },
    loginText: {
        color: colors.green,
        textTransform: 'uppercase',
        fontSize: 16
    }
}

const mapStateToProp = state => {
    return {
        selected_language: state.userDetail.userlanguage,
        userlang: state.rootReducer.userLanguage,
    }
}

export default connect(mapStateToProp, {
    setLanguageArabic,
    setLanguageEnglish,
    setStaticLangArabic,
    setStaticLangEnglish,
    setRootReduser,
    setProgramList,
    setMyProgram,
    setBlogList,
    setStoryList
})(WelcomeScreen);