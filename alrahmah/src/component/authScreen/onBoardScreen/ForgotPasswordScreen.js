import React, { Component } from 'react';
import { View, ScrollView, TouchableHighlight, ActivityIndicator } from 'react-native';
import { baseStyle, margin, padding } from '../../style';
import { Text } from 'react-native-elements';
import AppInput from '../../utitlity/AppInput';
import AppButton from '../../utitlity/AppButton';
import { setUser, opt_status_check } from '../../store/action/userAction';
import { connect } from 'react-redux';
import { forgot_password } from '../../api utility/apiutility';

const langobj = {
    en: {
        email: 'Enter email',
        invalid_email: 'Invalid email'

    },
    ar: {
        email: 'أدخل البريد الإلكتروني',
        invalid_email: 'بريد إلكتروني خاطئ'
    }
}


class ForgotPasswordScreen extends Component {

    state = {
        email: '',
        error: '',
        isloading: false,
    }

    sendOtpReq = async () => {
        try {

            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.email)) {
                this.setState({ error: '', isloading: true })
                const response = await fetch(forgot_password, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        email: this.state.email
                    })
                });
                const result = await response.json();
                console.log(result);
                if (result.success) {
                    this.props.setUser('EMAIL', this.state.email);
                    this.setState({ isloading: false })
                    alert(result.data.otp);
                    this.props.navigation.navigate('verify2');
                }
                else {
                    this.setState({ error: this.props.language == 'ar' ? langobj.ar.invalid_email : langobj.en.invalid_email });
                    this.setState({ isloading: false })
                }
            }
            else {
                this.setState({ error: this.props.language == 'ar' ? langobj.ar.invalid_email : langobj.en.invalid_email });
                this.setState({ isloading: false })
            }
        }
        catch (error) {
            this.setState({ isloading: false })
            if (error.message == 'Unexpected end of JSON input') {
                this.setState({ error: 'this email are not registered' })
            }
            else if (error.message == 'Network request failed') {
                this.props.navigation.navigate('Error');
            }
        }
    }


    renderActivity = () => {
        if (this.state.isloading) {
            return (
                <ActivityIndicator size="large" color="#4ABEA1" />
            )
        }
        else {
            return (
                <AppButton
                    title={this.props.userlang.continue}
                    containerStyle={styles.containerStyle}
                    onPress={() => this.sendOtpReq()} />
            )
        }
    }

    render() {
        const lang = this.props.language;
        return (
            <View style={baseStyle.appContainer}>
                <ScrollView>
                    <View style={[baseStyle.appContent, baseStyle.paddingLg]}>

                        <Text style={{ ...baseStyle.contentHeading, textAlign: lang == 'ar' ? 'right' : 'left' }}>{this.props.userlang.forgot_password}</Text>
                        <Text style={baseStyle.subHeading}></Text>
                        <AppInput
                            label={this.props.userlang.email}
                            autoCapitalize={'none'}
                            placeholder={this.props.userlang.enter_your_email}
                            keyboardType={'email-address'}
                            inputContainerStyle={{ marginBottom: 0,backgroundColor:'red' }}
                            onChangeText={(value) => {
                                //this.props.setUser('EMAIL', value);
                                this.setState({ email: value });
                            }}
                            lang={this.props.language}
                        />
                        <Text style={{ color: 'red', textAlign: this.props.language == 'ar' ? 'right' : 'left' }} >{this.state.error}</Text>
                        < this.renderActivity />
                    </View>
                </ScrollView>
                <View style={styles.btnContainer}>
                    <Text style={baseStyle.textAlignCenter}>{this.props.userlang.by_continoue}</Text>
                    <TouchableHighlight>
                        <Text style={[baseStyle.greenText, baseStyle.textAlignCenter]}>{this.props.userlang.term_and_condition}</Text>
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
}



const styles = {
    btnContainer: {
        justifyContent: 'flex-end',
        flex: 1,
        marginBottom: margin.xl,
        padding: padding.lg
    },
    containerStyle: {
        marginTop: margin.xl
    }
}

const mapStateToProp = state => {
    return {
        userlang: state.rootReducer.userLanguage,
        language: state.userDetail.userlanguage,
    }
}


export default connect(mapStateToProp, { setUser, opt_status_check })(ForgotPasswordScreen);