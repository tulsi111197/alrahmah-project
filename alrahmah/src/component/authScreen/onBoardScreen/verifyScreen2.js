import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ScrollView, TextInput, ActivityIndicator, TouchableWithoutFeedback } from 'react-native';
import { baseStyle, margin, padding, colors } from '../../style';
import { Text } from 'react-native-elements';
import AppButton from '../../utitlity/AppButton';
import * as Action from '../../store/action/userAction';
import { verifyotp, resend_otp } from '../../api utility/apiutility';


const langobj = {
    en: {
        otp: 'Enter 4 digit code',
    },
    ar: {
        otp: 'أدخل الرمز المكون من 4 أرقام',
    }
}


class VerifyScreen extends Component {

    state = {
        otp: [],
        otperror: '',
        isLoading: false,
        curTime: 60,
    }
    otpTextInput = [];

    componentDidMount() {
        this.otpTextInput[0].focus();
        setInterval(() => {
            if (this.state.curTime > 0) {
                this.setState({
                    curTime: this.state.curTime - 1
                })

            }
        }, 1000)
    }

    focusPrevious(key, index) {
        if (key === 'Backspace' && index !== 0)
            this.otpTextInput[index - 1].focus();
    }

    focusNext(index, value) {
        if (index < this.otpTextInput.length - 1 && value) {
            this.otpTextInput[index + 1].focus();
        }
        if (index === this.otpTextInput.length - 1) {
            this.otpTextInput[index].blur();
        }
        const otp = this.state.otp;
        otp[index] = value;
        this.setState({ otp });
    }
    navigateUser = async () => {
        if (this.state.otp.length === 4) {
            await this.setState({ isLoading: true })
            let response = await this.validateOtp();
            let result = await response.json();
            if (result.data.otp) {
                await this.setState({ isLoading: false })
                await this.props.navigation.navigate('newPassword');
            }
            else {
                await this.setState({ isLoading: false })
                this.setState({ otperror: 'Invalid Code' })
            }
        }
        else {
            this.setState({ otperror: this.props.language == 'ar' ? langobj.ar.otp : langobj.en.otp })
        }
    }

    validateOtp = async () => {
        let result = await fetch(verifyotp, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.props.email,
                otp: this.state.otp.join('')
            })
        })
        console.log(result);
        return result
    }

    resendOtp = async () => {
        if (this.state.curTime > 0)
            return 0;
        let result = await fetch(resend_otp, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.props.email,
                otp: this.state.otp.join('')
            })
        });
        let response = await result.json();
        console.log(response);
        alert(response.data.otp);
        this.setState({ curTime: 60 });
    }


    renderInputs() {
        const inputs = Array(4).fill(0);
        const txt = inputs.map((i, j) =>
            <TextInput
                placeholder='1'
                style={[styles.inputRadius, { borderRadius: 10 }]}
                containerStyle={styles.containerStyle}
                keyboardType="numeric"
                onChangeText={v => this.focusNext(j, v)}
                onKeyPress={e => this.focusPrevious(e.nativeEvent.key, j)}
                ref={ref => this.otpTextInput[j] = ref}
            />
        );
        return txt;
    }

    renderActivityIndicater = () => {
        if (this.state.isLoading) {
            return (
                <ActivityIndicator size="large" color="#4ABEA1" />
            )
        }
        else {
            return (
                <AppButton title={this.props.userlang.verify} onPress={() =>
                    this.navigateUser()
                    //await this.props.navigation.navigate('newPassword')
                } />
            )
        }
    }

    render() {
        const lang = this.props.language;
        return (
            <View style={[baseStyle.appContainer, styles.contentStyle]}>
                <ScrollView>
                    <Text style={{...baseStyle.contentHeading, textAlign: lang == 'ar' ? 'right' : 'left' }}>{this.props.userlang.veification_code}</Text>


                    <View style={{ flexDirection: 'row', justifyContent: "space-around" }}>
                        {this.renderInputs()}
                    </View>
                    <Text style={{
                        color: colors.red, marginTop: 20,
                        textAlign: this.props.language == 'ar' ? 'right' : 'left'
                    }}>{this.state.otperror}</Text>
                    <TouchableWithoutFeedback onPress={() => this.resendOtp()} >
                        <Text style={[baseStyle.textAlignCenter, baseStyle.marginBottomLg, baseStyle.marginTopXl]}
                        >{this.props.userlang.did_not_receive_code}</Text>
                    </TouchableWithoutFeedback>
                    < this.renderActivityIndicater />
                    <Text style={{ textAlign: lang == 'ar' ? 'right' : 'left' }}> {this.props.userlang.send_in} : {this.state.curTime}s </Text>
                </ScrollView>
            </View>

        );
    }
}
const styles = {
    btnContainer: {
        justifyContent: 'flex-end',
        flex: 1,
        marginBottom: margin.xl,
        padding: padding.lg
    },
    containerStyle: {
        width: "15%"
    },
    inputStyle: {
        textAlign: 'center',
        padding: null
    },
    contentStyle: {
        flex: 1,
        padding: padding.lg,
        alignItem: 'center',
    }
}


const mapStateToProp = state => {
    return {
        email: state.userDetail.email,
        userlang: state.rootReducer.userLanguage,
        language: state.userDetail.userlanguage,
    }
}

export default connect(mapStateToProp, Action)(VerifyScreen);