import React, { Component } from 'react';
import {
    View, ScrollView, TouchableHighlight, ActivityIndicator,
    TouchableOpacity, Platform, FlatList, Modal, TextInput
} from 'react-native';
import { connect } from 'react-redux';
import { Icon } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import { Text } from 'react-native-elements';
import AppInput from '../../utitlity/AppInput';
import AppButton from '../../utitlity/AppButton';
import * as Action from '../../store/action/userAction';
import emailValidation from '../../validation/emailValidation';
import nameValidation from '../../validation/nameValidation';
import passwordValidation from '../../validation/passwordValidation';
import { colors, fonts, baseStyle, padding, radius, margin } from '../../style';
import { signup } from '../../api utility/apiutility';
import Counterys from '../../api utility/Countries';


const langobj = {
    en: {
        name: 'Enter name',
        email: 'Enter email',
        password: 'Enter password',
        confirm_password: 'Enter confirm password',
        password_not_match: 'Password not match',
        password_must: 'Password must be 8 character long'
    },
    ar: {
        name: 'أدخل الاسم',
        email: 'أدخل البريد الإلكتروني',
        password: 'أدخل كلمة المرور',
        confirm_password: 'أدخل تأكيد كلمة المرور',
        password_not_match: 'كلمة السر ليست جيدة',
        password_must: 'يجب أن تتكون كلمة المرور من 8 أحرف'
    }
}

class SignUpScreen extends Component {
    state = {
        isLoading: false,
        selectedcode: '+ 91',
        flag: false,
        countery_name: 'India',
        iscodemodel: false,
        filterCounterys: Counterys,
        name: '',
        name_error: '',
        email: '',
        email_error: '',
        password: '',
        password_error: '',
        confirm_password: '',
        confirm_pass_error: '',
        phone: '',
        country: '',
        iscodemodelIOS: false,
        iscodemodelAndroid: false,
    }

    validateUser = async () => {
        const passlen = this.state.password;
        // await this.props.setError('NAME_ERROR', nameValidation(this.props.name));
        // await this.props.setError('EMAIL_ERROR', emailValidation(this.props.email));
        // await this.props.setError('PASSWORD_ERROR', passwordValidation(this.props.password));
        if (this.state.name == '') {
            this.setState({ name_error: this.props.language == 'ar' ? langobj.ar.name : langobj.en.name })
            return
        }
        else if (this.state.email == '') {
            this.setState({ email_error: this.props.language == 'ar' ? langobj.ar.email : langobj.en.email })
            return
        }
        else if (this.state.password == '') {
            this.setState({ password_error: this.props.language == 'ar' ? langobj.ar.password : langobj.en.password })
            return
        }
        else if (passlen.length < 7) {
            this.setState({ password_error: this.props.language == 'ar' ? langobj.ar.password_must : langobj.en.password_must })
            return
        }
        else if (this.state.confirm_password == '') {
            this.setState({ confirm_pass_error: this.props.language == 'ar' ? langobj.ar.confirm_password : langobj.en.confirm_password })
            return
        }
        else if (this.state.password !== this.state.confirm_password) {
            this.setState({ confirm_pass_error: this.props.language == 'ar' ? langobj.ar.password_not_match : langobj.en.password_not_match })
            return
        }
        else {
            this.setState({ confirm_pass_error: '', password_error: '' })
            this.props.setUser('EMAIL', this.state.email);
            await this.setState({ isLoading: true })
            let response = await this.createUserAccountByAPI();
            if (response.success) {
                alert(response.data.user.otp);
                await this.storeStatus('STATUS', 'VerifyScreen');
                await this.storeStatus('TIME', new Date().getTime());
                await this.setState({ isLoading: false })
                await this.props.navigation.navigate('verify');
            }
            else {
                console.log(response)
                await this.setState({ isLoading: false, email_error: response.data.email[0] })
                //await this.props.setError('EMAIL_ERROR', response.data.email[0]);
            }
        }
    }
    createUserAccountByAPI = async () => {
        let result = await fetch(signup, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: this.state.name,
                email: this.state.email,
                password: this.state.password
            })
        })
        return await result.json();
    }

    storeStatus = async (type, value) => {
        await AsyncStorage.setItem(type, value.toString());
    }

    renderActivityIndicater = () => {
        if (this.state.isLoading) {
            return (
                <ActivityIndicator size="large" color="#4ABEA1" />
            )
        }
        else {
            return (
                <AppButton 
                title={this.props.userlang.signup} 
                buttonStyle={{alignSelf:'flex-end',marginBottom:margin.lg}}
                onPress={() => this.validateUser()} />
            )
        }
    }

    findWordForEach = (toSearch, item) => {
        if (toSearch.trim().toLowerCase() >= 'a' && toSearch.trim().toLowerCase() <= 'z') {
            let response = item.name.toLowerCase().search(toSearch.toLowerCase());
            if (response >= 0)
                return item
        }
        else {
            let response = item.dial_code.replace(/[^0-9]/g, '').search(toSearch.replace(/[^0-9]/g, ''));
            if (response >= 0)
                return item
        }


    }


    searchCode = (text) => {
        if (text == '') {
            this.setState({ filterCounterys: Counterys })
        }
        else {
            let result = Counterys.filter(item => this.findWordForEach(text, item));
            this.setState({ filterCounterys: result });
        }

    }

    renderCounteryListIOS = () => {
        if (this.state.iscodemodelIOS) {
            return (
                <Modal
                    visible={this.state.iscodemodelIOS}
                    transparent={true}
                >
                    <View style={baseStyle.appContainer} >

                        <View style={{
                            flexDirection: 'row', justifyContent:'flex-end',marginTop:margin.xl,paddingHorizontal:padding.lg
                            
                        }}>

                            <Icon name='close' iconStyle={{ backgroundColor: 'rgba(0,0,0,0.2)', padding: 8, color: 'white', }} onPress={() => this.setState({ iscodemodelIOS: false, fromcountery: false })} />

                        </View>
                        <View style={{
                            flexDirection: 'row', alignItems: 'center',marginTop:margin.xl,paddingHorizontal:padding.lg
                            
                        }}>

                            <AppInput
                                placeholder={this.props.userlang.search_by_code}
                                // label={this.props.userlang.search}
                                containerStyle={{marginBottom:0 }}
                                onChangeText={(text) => this.searchCode(text)}
                                lang={this.props.language}
                            />
                            </View>


                        <View style={{
                            width: '100%',
                            paddingTop:padding.xl,
                            alignSelf: 'flex-end',
                            borderRadius: radius.xl,
                            paddingHorizontal: padding.lg,
                            backgroundColor: colors.white
                        }}>
                            <FlatList
                                data={this.state.filterCounterys}
                                renderItem={({ item }) =>
                                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', marginBottom: margin.md }}>
                                        <TouchableOpacity style={{
                                            borderWidth: 1,
                                            padding: padding.lg,
                                            borderRadius: radius.lg,
                                            width: '100%',
                                            backgroundColor: colors.offBlack
                                        }}
                                            onPress={() => this.setState({
                                                iscodemodelIOS: false, selectedcode: item.dial_code,
                                                countery_name: item.name
                                            })}
                                        >
                                            <Text
                                                style={{
                                                    color: colors.textColor
                                                }} >
                                                {
                                                    this.state.fromcountery ?
                                                        `${item.flag}   ${item.name} ` :

                                                        `${item.flag}   ${item.dial_code} ${item.name} `}</Text>
                                        </TouchableOpacity>
                                    </View>
                                }
                            />
                        </View>


                    </View>
                </Modal>
            )
        }
        else
            return null
    }





    renderCounteryListAndroid = () => {
        if (this.state.iscodemodelAndroid) {
            return (
                <Modal
                    visible={this.state.iscodemodelAndroid}
                    transparent={true}
                >
                    <View style={{
                        backgroundColor: 'rgba(237,228,228,0.6)',
                        flex: 1,
                        flexDirection: 'column',

                        alignItems: 'center'

                    }} >

                        <View style={{
                            flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, paddingHorizontal: padding.lg, paddingVertical: 10,
                            borderTopEndRadius: 20,
                            borderTopStartRadius: 20
                        }}>

                            <AppInput
                                placeholder={this.props.userlang.search_by_code}
                                label={this.props.userlang.search}
                                containerStyle={{ width: '90%' }}
                                onChangeText={(text) => this.searchCode(text)}
                            />
                            <Icon name='close' iconStyle={{ backgroundColor: 'rgba(0,0,0,0.3)', padding: 8, color: 'white', }} onPress={() => this.setState({ iscodemodelAndroid: false, fromcountery: false })} />

                        </View>

                        <View style={{
                            width: '100%',
                            height: 600,
                            alignSelf: 'flex-end',
                            borderRadius: radius.xl,
                            paddingHorizontal: padding.lg,
                            backgroundColor: colors.white
                        }}>
                            <FlatList
                                data={this.state.filterCounterys}
                                renderItem={({ item }) =>
                                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', marginBottom: margin.md }}>
                                        <TouchableOpacity style={{
                                            borderWidth: 1,
                                            padding: padding.lg,
                                            borderRadius: radius.lg,
                                            width: '100%',
                                            backgroundColor: colors.offBlack
                                        }}
                                            onPress={() => this.setState({
                                                iscodemodelAndroid: false, selectedcode: item.dial_code,
                                                countery_name: item.name
                                            })}
                                        >
                                            <Text
                                                style={{
                                                    color: colors.textColor
                                                }} >
                                                {
                                                    this.state.fromcountery ?
                                                        `${item.flag} ${item.name} ` :

                                                        `${item.flag} ${item.dial_code} ${item.name} `}</Text>
                                        </TouchableOpacity>
                                    </View>
                                }
                            />
                        </View>


                    </View>
                </Modal>
            )
        }
        else
            return null
    }



    render() {
        const lang = this.props.language;
        return (
            <View style={baseStyle.appContainer}>

                <this.renderCounteryListAndroid />
                <this.renderCounteryListIOS />
                <ScrollView>
                    <View style={baseStyle.appContent}>
                        <Text style={{...baseStyle.contentHeading, textAlign: lang == 'ar' ? 'right' : 'left' }}>{this.props.userlang.create_new_account}</Text>
                        <Text style={baseStyle.subHeading}></Text>
                        <AppInput
                            label={this.props.userlang.name}
                            inputContainerStyle={{ marginBottom: 0 }}
                            placeholder={this.props.userlang.enter_name}
                            inputRef={ref => this.name_ref = ref}
                            onChangeText={(value) => {
                                //this.props.setUser('USERNAME', value)
                                this.setState({ name: value })
                            }}
                            onSubmitEditing={() => this.email_ref.focus()}
                            returnKeyType={"next"}
                            errorMessage={this.state.name == '' ? this.state.name_error : ''}
                            lang={this.props.language}

                        />
                        <AppInput
                            label={this.props.userlang.email}
                            autoCapitalize={'none'}
                            inputRef={ref => this.email_ref = ref}
                            inputContainerStyle={{ marginBottom: 0 }}
                            keyboardType={'email-address'}
                            returnKeyType={"next"}
                            placeholder={this.props.userlang.enter_your_email}
                            onChangeText={(value) => {
                                // this.props.setUser('EMAIL', value)
                                this.setState({ email: value })
                            }}
                            onSubmitEditing={() => this.pass_ref.focus()}
                            errorMessage={this.state.email_error}
                            lang={this.props.language}
                        />

                        <Text style={{
                            fontSize: 16,
                            marginBottom: 5,
                            textAlign: this.props.language == 'ar' ? 'right' : 'left'
                        }}>{this.props.userlang.country}
                        </Text>
                        {
                            Platform.OS === 'android'
                                ?
                                <TouchableOpacity onPress={() => {
                                    this.setState({
                                        iscodemodelAndroid: true,
                                        fromcountery: true
                                    })
                                }}
                                    style={{
                                        paddingVertical: 13,
                                        paddingHorizontal: padding.lg,
                                        backgroundColor: colors.inputColor,
                                        borderRadius: 5,
                                        marginBottom: 15,
                                        alignItems: this.props.language == 'ar' ? 'flex-end' : 'flex-start'
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: colors.labelColor,
                                            fontSize: 16,
                                            textAlign: this.props.language == 'ar' ? 'right' : 'left'
                                        }}
                                    >
                                        {
                                            this.state.countery_name
                                        }
                                    </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => {
                                    this.setState({
                                        iscodemodelIOS: true,
                                        fromcountery: true
                                    })
                                }}
                                    style={{
                                        paddingVertical: 13,
                                        marginBottom:margin.xl,
                                        paddingVertical:padding.xl,
                                        paddingLeft:padding.xl,
                                        backgroundColor: colors.inputColor,
                                        borderRadius: 100,
                                        alignItems: this.props.language == 'ar' ? 'flex-end' : 'flex-start'
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: colors.textColor,
                                            fontSize: 19,
                                            textAlign: this.props.language == 'ar' ? 'right' : 'left'
                                        }}
                                    >
                                        {
                                            this.state.countery_name
                                        }
                                    </Text>
                                </TouchableOpacity>
                        }



                        <AppInput
                            label={this.props.userlang.password}
                            placeholder={this.props.userlang.enter_your_password}
                            returnKeyType={"next"}
                            inputContainerStyle={{ marginBottom: 0 }}
                            secureTextEntry={true}
                            inputRef={ref => this.pass_ref = ref}
                            onSubmitEditing={() => this.conf_pass_ref.focus()}
                            onChangeText={(value) => {
                                //this.props.setUser('PASSWORD', value)
                                this.setState({ password: value })
                            }}
                            errorMessage={this.state.password_error}
                            lang={this.props.language}

                        />
                        <AppInput
                            label={this.props.userlang.confirm_password}
                            inputContainerStyle={{ marginBottom: 0 }}
                            placeholder={this.props.userlang.enter_your_confirm_password}
                            secureTextEntry={true}
                            inputRef={ref => this.conf_pass_ref = ref}
                            onChangeText={(value) => {
                                //this.props.setUser('CONFIRMPASSWORD', value)
                                this.setState({ confirm_password: value })
                            }}
                            errorMessage={this.state.confirm_pass_error}
                            lang={this.props.language}
                        />

                    </View>

                    <View style={styles.btnContainer}>

                        <View>
                            <Text style={baseStyle.textAlignCenter}>{this.props.userlang.by_continoue}</Text>
                            <TouchableHighlight>
                                <Text style={[baseStyle.greenText, baseStyle.textAlignCenter]}>
                                    {this.props.userlang.term_and_condition}</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </ScrollView>
                < this.renderActivityIndicater />
            </View>
        );
    }
}
const styles = {
    btnContainer: {
        justifyContent: 'flex-end',
        flex: 1,
        marginBottom: margin.xl,
        padding: padding.lg
    }
}

const mapStateToProp = state => {
    return {
        name: state.userDetail.username,
        email: state.userDetail.email,
        password: state.userDetail.password,
        confirmpassword: state.userDetail.confirmpassword,
        passworderror: state.userDetail.passworderror,
        confirmpassworderror: state.userDetail.confirmpassworderror,
        emailerror: state.userDetail.emailerror,
        nameerror: state.userDetail.nameerror,
        userlang: state.rootReducer.userLanguage,
        language: state.userDetail.userlanguage,
    }
}

export default connect(mapStateToProp, Action)(SignUpScreen);