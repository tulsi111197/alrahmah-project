import React, { Component } from 'react';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { View, TouchableOpacity, ScrollView, ActivityIndicator, Platform } from 'react-native';
import { baseStyle, margin, padding } from '../../style';
import { Text } from 'react-native-elements';
import AppInput from '../../utitlity/AppInput';
import AppButton from '../../utitlity/AppButton';
import emailValidation from '../../validation/emailValidation';
import passwordValidation from '../../validation/passwordValidation';
import { setError, setUser } from '../../store/action/userAction';
import { login, dashboard, mydonationuri, programlist, myprogramlist, bloglist, storylist } from '../../api utility/apiutility';
import { setRootReduser, setProgramList, setMyProgram, setBlogList, setStoryList } from '../../store/reducer/action/RootReducerAction';
import fetchUserDataByApi from '../../api unitlity function/fetchuserdata';
import { setMyDonation } from '../../store/reducer/action/RootReducerAction';
import { OtherContent } from '../../Constent/AppConstent';

const langobj = {
    en: {
        name: 'Enter name',
        email: 'Enter email',
        password: 'Enter password',
        confirm_password: 'Enter confirm password',
        password_not_match: 'Password not match',
    },
    ar: {
        name: 'أدخل الاسم',
        email: 'أدخل البريد الإلكتروني',
        password: 'أدخل كلمة المرور',
        confirm_password: 'أدخل تأكيد كلمة المرور',
        password_not_match: 'كلمة السر ليست جيدة',
    }
}



class LoginScreen extends Component {
    state = {
        isloading: false,
        device_token: '',
        device_type: '',
        email: '',
        password: '',
        email_error: '',
        password_error: '',
    }

    async componentDidMount() {
        const token = await AsyncStorage.getItem('token');
        this.setState({ device_token: token });
        Platform.OS === 'ios' ? this.setState({ device_type: 'ios' }) : this.setState({ device_type: 'android' });
        console.log(OtherContent.getDeviceToken());
    }


    serviceWithoutLogin = async () => {
        let response = await fetchUserDataByApi
            (dashboard, null, this.props.selected_language);

        let program = await fetchUserDataByApi
            (programlist, null, this.props.selected_language);

        let blog = await fetchUserDataByApi
            (bloglist, null, this.props.selected_language);
        let story = await fetchUserDataByApi
            (storylist, null, this.props.selected_language);
        await this.props.setStoryList
            (story.data);
        await this.props.setBlogList
            (blog.data);
        await this.props.setProgramList
            (program.data.reverse());
        await this.props.setRootReduser
            (response.data);
    }

    validateuser = async () => {
        // await this.props.setUser('EMAIL_ERROR', emailValidation(this.props.email));
        // await this.props.setUser('PASSWORD_ERROR', passwordValidation(this.props.password));

        if (this.state.email == '') {
            this.setState({ email_error: this.props.language == 'ar' ? langobj.ar.email : langobj.en.email })
            return
        }
        else if (this.state.password == '') {
            this.setState({ password_error: this.props.language == 'ar' ? langobj.ar.password : langobj.en.password })
            return
        }
        else {
            await this.setState({ isloading: true, password_error: '' })
            let response = await this.loginUserByAPI();

            if (response.success) {
                this.props.setUser('EMAIL', this.state.email)
                this.props.setUser('PASSWORD', this.state.password)
                this.storeStatus('APITOKEN', response.data.token);
                OtherContent.setToken(response.data.token);
                await this.serviceWithoutLogin();
                await AsyncStorage.setItem('LANGUAGE', this.props.selected_language);
                if (response.data.user.user_image == null)
                    this.storeStatus('IMAGE_URI', false)
                else
                    this.storeStatus('IMAGE_URI', JSON.stringify(response.data.user.user_image));
                this.storeStatus('EMAIL', this.state.email);
                this.storeStatus('USERNAME', response.data.user.username ? response.data.user.username : '');
                this.storeStatus('F_NAME', response.data.user.f_name ? response.data.user.f_name : '');
                this.storeStatus('L_NAME', response.data.user.l_name ? response.data.user.l_name : '');
                this.storeStatus('MOBILE', response.data.user.mobile ? response.data.user.mobile : '');
                this.storeStatus('ADDRESS', response.data.user.address ? response.data.user.address : '');
                this.storeStatus('CITY', response.data.user.city ? response.data.user.city : '');
                this.storeStatus('STATE', response.data.user.state ? response.data.user.state : '');
                this.storeStatus('STATUS', 'LoggedIn');
                this.storeStatus('guest', 'no');
                await this.setState({ isloading: false })
                this.props.navigation.navigate('terms');
            }
            else {
                console.log(response);
                await this.setState({ isloading: false, password_error: response.message })
                //await this.props.setError('EMAIL_ERROR', response.message);
            }
        }

        this.setState({ isloading: false });
    }

    storeStatus = async (type, value) => {
        await AsyncStorage.setItem(type, value.toString());
    }

    loginUserByAPI = async () => {
        let device_token = await AsyncStorage.getItem('fcm_token');
        let device_type = Platform.OS === 'ios' ? 'ios' : 'android';
        console.log('device token ', OtherContent.getDeviceToken());
        let result = await fetch(login, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.state.email,
                password: this.state.password,
                device_type: Platform.OS,
                device_token: OtherContent.getDeviceToken()
            })
        })
        return await result.json();
    }

    renderActivity = () => {
        if (this.state.isloading) {
            return (
                <ActivityIndicator size="large" color="#4ABEA1" />
            )
        }

        return (
            <AppButton
                title={this.props.userlang.login}
                buttonStyle={styles.btnStyle}
                onPress={this.validateuser}
            />
        )
    }

    render() {
        const lang = this.props.language;
        return (
            <View style={baseStyle.appContainer, styles.containerStyle}>
                <ScrollView>
                    <View style={[baseStyle.appContent, baseStyle.paddingLg, styles.containerStyle]}>

                        <Text style={{...baseStyle.contentHeading,textAlign: lang == 'ar' ? 'right' : 'left' }}>{this.props.userlang.welcome_back}</Text>
                        <AppInput
                            label={this.props.userlang.email}
                            placeholder={this.props.userlang.enter_your_email}
                            keyboardType={'email-address'}
                            inputContainerStyle={{ marginBottom: 0 }}
                            autoCapitalize={'none'}
                            onChangeText={(value) => {
                                //this.props.setUser('EMAIL', value)
                                this.setState({ email: value })
                            }}
                            errorMessage={this.state.email_error}
                            lang={this.props.language}
                        />
                        <AppInput
                            label={this.props.userlang.password}
                            placeholder={this.props.userlang.enter_your_password}
                            secureTextEntry={true}
                            inputContainerStyle={{ marginBottom: 0 }}
                            onChangeText={(value) => {
                                //this.props.setUser('PASSWORD', value)
                                this.setState({ password: value })
                            }}
                            errorMessage={this.state.password_error}
                            lang={this.props.language}
                        />
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('forgotPassword')}>
                            <Text style={[baseStyle.alignSelfEnd, baseStyle.marginBottomLg]}>
                                {this.props.userlang.forgot_password}
                            </Text>
                        </TouchableOpacity>
                        
                    </View>
                </ScrollView>
                <View style={styles.btnContainer}>
                        <Text style={baseStyle.textAlignCenter}>{this.props.userlang.by_continoue}</Text>
                        <TouchableOpacity>
                            <Text style={[baseStyle.greenText, baseStyle.textAlignCenter]}>
                                {this.props.userlang.term_and_condition}
                            </Text>
                        </TouchableOpacity>

                    </View>
                < this.renderActivity />
            </View>
        );
    }
}
const styles = {
    btnContainer: {
        marginBottom: margin.xl,
        justifyContent: 'flex-end',
        padding: padding.lg,
        flex: 2,
    },
    containerStyle: {
        flex: 1,
       
    },
    btnStyle:{
        alignSelf:'flex-end',
        marginBottom:margin.lg
    }
}

const mapStateToProp = state => {
    return {
        email: state.userDetail.email,
        password: state.userDetail.password,
        passworderror: state.userDetail.passworderror,
        emailerror: state.userDetail.emailerror,
        userlang: state.rootReducer.userLanguage,
        selected_language: state.userDetail.userlanguage,
        language: state.userDetail.userlanguage,
    }
}


export default connect(mapStateToProp, {
    setBlogList,
    setStoryList, setMyProgram, setUser, setError,
    setRootReduser, setMyDonation, setProgramList,
})(LoginScreen);