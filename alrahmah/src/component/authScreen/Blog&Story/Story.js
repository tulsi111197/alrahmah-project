import React, { Component } from 'react';
import { View, FlatList, Text, StyleSheet, Linking, TouchableOpacity, Modal, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux';
import AppTile from '../../utitlity/AppTile';
import AppListItem from '../../utitlity/AppListItem';
import { colors, fonts, margin, radius } from '../../style';
import fetchStoryDetail from '../../api unitlity function/fetchuserdata';
import AsyncStorage from '@react-native-community/async-storage';
import { storydetail } from '../../api utility/apiutility';
import FastImage from 'react-native-fast-image';
import { setStoryDetails } from '../../store/reducer/action/storyDetailAction';
import {
    getFlexDirection,
    getAlignItems,
    isLangArabic,
    getTextAlign
} from '../../Constent/helperMethods/langMethods';


class Story extends Component {

    state = {
        lang: 'en',
        token: '',
        isloading: false,
    }

    openyoutube = (uri) => {
        Linking.openURL(uri)
    }
    componentDidMount = async () => {
        let token = await AsyncStorage.getItem('APITOKEN');
        this.setState({ lang: this.props.selected_language });
        // let lang = await AsyncStorage.getItem('LANGUAGE');
        // if (lang == 'ar')
        //     this.setState({ lang: 'ar' })
        this.setState({ token });
    }


    navigateUserToStoryDetails = async ({ item }) => {
        try {
            this.setState({ isloading: true })
            let result = await fetchStoryDetail(`${storydetail}${item.id}`, this.state.token, this.state.lang);
            await this.props.setStoryDetails(result.data);
            this.setState({ isloading: false })
            this.props.navigation.navigate('storyDetail');
        } catch (erro) {
            this.setState({ isloading: false })
        }
    }

    renderActivity = () => {
        if (this.state.isloading)
            return (< Modal visible={true}
                transparent={true} >
                <View style={
                    {
                        position: 'absolute',
                        backgroundColor: 'rgba(0,0,0,0.3)',
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }
                } >
                    <ActivityIndicator size="large"
                        color="#4ABEA1" />
                </View>
            </Modal >

            )
        else
            return null
    }


    render() {
        const lang = this.props.language;
        return (<View style={
            { flex: 1, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'space-between', marginTop: margin.lg }
        } >
            <this.renderActivity />
            <FlatList data={this.props.storylist}
                renderItem={
                    ({ item }) =>
                        // <AppListItem
                        //     title={this.state.lang == 'en' ? item.p_name : item.a_p_name}
                        //     titleStyle={{
                        //         ...styles.listTitle,
                        //         textAlign: getTextAlign(lang)
                        //     }}
                        //     subtitle={<View style={styles.subtitleView}>
                        //         <Text style={{
                        //             ...styles.subTitle,
                        //             textAlign: getTextAlign(lang)
                        //         }}>{this.state.lang == 'en' ? item.place : item.a_place}</Text>
                        //         <Text style={{
                        //             ...styles.subTitle,
                        //             textAlign: getTextAlign(lang)
                        //         }}>
                        //             Date-{item.date}</Text>
                        //     </View>}
                        //     onPress={() => this.navigateUserToStoryDetails({ item })}
                        //     containerStyle={{
                        //         backgroundColor: 'transparent', marginLeft: -20,
                        //         flexDirection: getFlexDirection(lang)
                        //     }}
                        //     leftAvatar={
                        //         <View>
                        //             <AppTile
                        //                 icon={{ name: 'play-circle', type: 'font-awesome', color: '#fff' }}
                        //                 width={130}
                        //                 height={130}
                        //                 source={{ uri: item.image }}
                        //                 overlayStyle={{ alignItems: 'center', justifyContent: 'flex-start' }}
                        //                 imageStyle={{ borderRadius: radius.lg }}
                        //                 onPress={() => this.openyoutube(item.link)}
                        //             />
                        //         </View>}
                        // />

                        <TouchableOpacity onPress={
                            () => this.navigateUserToStoryDetails({ item })
                        }
                            style={
                                {
                                    flex: 1,
                                    margin: margin.lg,
                                    //alignItems: this.props.language == 'ar' ? 'flex-end' : 'flex-start'
                                }
                            } >
                            <FastImage source={
                                { uri: item.image }
                            }
                                style={
                                    { height: 150, width: null, borderRadius: radius.lg }
                                }
                                resizeMode='cover' >
                            </FastImage> 
                            <View style={
                                {
                                    backgroundColor: 'rgba(0,0,0,0.5)',
                                    ...StyleSheet.absoluteFill,
                                    borderRadius: radius.lg,
                                    padding: 10,
                                    flex: 1
                                }
                            } >
                                <View >
                                    <Text style={styles.titleStyle} >
                                        Our donation is hopefor poor childrens
                                            </Text>

                                </View>
                                <View style={styles.storyBadge} >
                                    <Text style={{...styles.badgeTitle,}}>
                                        05
                                        </Text> 
                                       <Text style={{...styles.badgeTitle,}}>
                                        Aug 
                                        </Text>
                                </View>
                            </View>
                        </TouchableOpacity >

                } 
            />
        </View >
        );
    }
}

const styles = {

    titleStyle: {
        color: colors.white,
        fontSize: fonts.xl,
        fontWeight: '600',
        width: '80%'

    },

    badgeTitle: {
        fontSize: fonts.xl,
        fontWeight: 'bold',
        color: colors.white,
    },
    storyBadge: {
        position: 'absolute',
        top: 'auto',
        bottom: 0,
        right: 15,
        backgroundColor: colors.green,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        height: 70,
        maxHeight: 70,
        width: 70,
        borderTopRightRadius: 100,
        borderTopLeftRadius: 100
    }
}

const mapStateToProp = state => {
    return {
        storylist: state.rootReducer.storylist,
        selected_language: state.userDetail.userlanguage,
        language: state.userDetail.userlanguage,
    }
}


export default connect(mapStateToProp, { setStoryDetails })(Story);