import React, { Component } from 'react';
import { View, ScrollView, FlatList, Linking } from 'react-native';
import { Text } from 'react-native-elements'
import { connect } from 'react-redux';
import { baseStyle, padding, colors, fonts, margin } from '../../style';
import AppTile from '../../utitlity/AppTile';
import fetchStoryDetail from '../../api unitlity function/fetchuserdata';
import AsyncStorage from '@react-native-community/async-storage';
import { storydetail } from '../../api utility/apiutility';
import { setStoryDetails } from '../../store/reducer/action/storyDetailAction';

class StoryDetail extends Component {

    state = {
        lang: 'en',
        token: ''
    }

    componentDidMount = async () => {
        let token = await AsyncStorage.getItem('APITOKEN');
        let lang = await AsyncStorage.getItem('LANGUAGE');
        if (lang == 'ar')
            this.setState({ lang: 'ar' })
        this.setState({ token });

    }

    navigateUserToStoryDetails = async ({ item }) => {
        let result = await fetchStoryDetail(`${storydetail}${item.id}`, this.state.token, this.state.lang);
        await this.props.setStoryDetails(result.data);
        this.props.navigation.navigate('storyDetail');
    }

    render() {
        const lang = this.props.selected_language;
        return (
            <View style={baseStyle.appContainer}>
                <ScrollView>
                    <AppTile
                        height={350}
                        source={{ uri: this.props.story.image }}
                        onPress={() => Linking.openURL(this.props.story.link)}
                        containerStyle={{ borderRadius: 0 }}
                        overlayStyle={{ justifyContent: 'center' }}
                    />
                    <View style={baseStyle.paddingLg}>


                        <Text style={{...styles.heading, textAlign: lang == 'ar' ? 'right' : 'left'}}>{this.props.userlang.patent} - {`${
                            this.props.selected_language == 'en' ? this.props.story.p_name :
                                this.props.story.a_p_name
                            }`}
                        </Text>



                        <Text style={{...styles.text, textAlign: lang == 'ar' ? 'right' : 'left'}}>{`${this.props.story.date}`} | {this.props.selected_language == 'en' ? this.props.story.place : this.props.story.a_place} </Text>


                        <Text style={[styles.heading, baseStyle.greenText, { textAlign: lang == 'ar' ? 'right' : 'left' }]}>{this.props.userlang.other_storys}</Text>

                        <FlatList
                            data={this.props.storylist}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            renderItem={({ item }) =>
                                <View >
                                    <AppTile
                                        width={200}
                                        containerStyle={{ marginBottom:margin.lg,borderRadius:180/5,overflow:'hidden',marginLeft:0}}
                                        onPress={() => this.navigateUserToStoryDetails({ item })}
                                        source={{ uri: item.image }} />
                                </View>
                            }
                        />
                    </View>

                </ScrollView>
            </View>
        );
    }
}

const styles = {
    backImg: {
        width: 20,
        height: 15,
        resizeMode: 'contain',
        marginLeft: padding.lg
    },
    heading: {
        fontSize: fonts.xl,
        fontWeight: 'bold',
        marginVertical: margin.lg
    },
    bulletStyle: {
        colors: colors.green
    },
    textStyle: {
        flex: 1,
        paddingLeft: padding.lg,
        fontSize: fonts.xl,
    },
    text: {
        fontSize: fonts.lg,
        marginBottom: margin.md
    }
}

const mapStateToProps = state => {
    return {
        story: state.storyDetail.SotryDetail,
        storylist: state.rootReducer.storylist,
        selected_language: state.userDetail.userlanguage,
        userlang: state.rootReducer.userLanguage,
    }
}
export default connect(mapStateToProps, { setStoryDetails })(StoryDetail);