import React from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation'
import { fonts } from '../../style';
import Blog from './Blog';
import Story from './Story';
import HeaderTitle from '../../static language/headerTitles';

const BlogAndStory = createMaterialTopTabNavigator(
    {
        Tab1: {
            screen: Blog,
            navigationOptions:{
                tabBarLabel: ()=>(<HeaderTitle screen="Blog" />)
            }
        },
        Tab2: {
            screen: Story,
            navigationOptions:{
                tabBarLabel: ()=>(<HeaderTitle screen="Stories" />)
            }
        }
    },

    {
        tabBarOptions: {
            activeTintColor: "#4ABEA1",
            inactiveTintColor: "#707070",
            upperCaseLabel:false,
            style: {
                backgroundColor: '#fff'
            },
            indicatorStyle: {
                backgroundColor: '#4ABEA1',
            },
            labelStyle:{
                fontSize:fonts.lg
            }
        },

    }
);
const BlogRoot = createAppContainer(BlogAndStory);

export default BlogRoot;





