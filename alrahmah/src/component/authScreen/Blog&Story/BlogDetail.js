import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ScrollView, Dimensions } from 'react-native';
import { Text } from 'react-native-elements'
import { baseStyle, padding, colors, fonts, margin } from '../../style';
import AppTile from '../../utitlity/AppTile';
import HTML from 'react-native-render-html';

class BlogDetail extends Component {

    isLanguageEnglish = () => {
        if (this.props.language == 'en')
            return true
        return false
    }

    componentDidMount() {

    }

    render() {
        const lang = this.props.language;
        return (
            <View style={baseStyle.appContainer}>
                <ScrollView>
                    <AppTile
                        source={{ uri: this.props.blogs.main_image }}
                        height={450}
                        containerStyle={{ borderRadius: 0 }}
                    />

                    <View style={baseStyle.paddingLg}>
                        <Text style={{ ...styles.heading, textAlign: lang == 'ar' ? 'right' : 'left' }}>
                            {
                                this.isLanguageEnglish() ?
                                    this.props.blogs.heading :
                                    this.props.blogs.a_heading
                            }
                        </Text>
                        <Text style={{ ...styles.text, textAlign: lang == 'ar' ? 'right' : 'left' }}>
                            {this.props.blogs.dop}
                            {` | `}
                            {
                                this.isLanguageEnglish()
                                    ?
                                    this.props.blogs.cat
                                    :
                                    this.props.blogs.a_cat
                            }
                            ,
                            {
                                this.isLanguageEnglish ?
                                    this.props.blogs.desig
                                    :
                                    this.props.blogs.a_desig
                            }
                        </Text>
                        <Text style={[styles.text, baseStyle.greenText, { textAlign: lang == 'ar' ? 'right' : 'left' }]}>
                            {
                                this.isLanguageEnglish()
                                    ?
                                    this.props.blogs.name
                                    :
                                    this.props.blogs.a_name
                            }

                        </Text>
                        <View style={{ alignItems: lang  == 'ar' ? 'flex-end' : 'flex-start' }}>


                            <HTML

                                html={this.isLanguageEnglish() ?
                                    this.props.blogs.content : this.props.blogs.arabic_content
                                } imagesMaxWidth={Dimensions.get('window').width} />
                        </View>
                    </View>

                </ScrollView>
            </View>
        );
    }
}

const styles = {
    backImg: {
        width: 20,
        height: 15,
        resizeMode: 'contain',
        marginLeft: padding.lg
    },
    heading: {
        fontSize: fonts.xl,
        fontWeight: 'bold',
        marginVertical: margin.lg
    },
    bulletStyle: {
        colors: colors.green
    },
    textStyle: {
        flex: 1,
        paddingLeft: padding.lg,
        fontSize: fonts.xl,
    },
    text: {
        fontSize: fonts.lg,
        marginBottom: margin.md
    }
}

const mapStateToProp = state => {

    return {
        language: state.userDetail.userlanguage,
        blogs: state.blogDetail.blogDetail
    }
}


export default connect(mapStateToProp, null)(BlogDetail);