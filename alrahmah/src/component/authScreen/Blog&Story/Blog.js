import React, { Component } from 'react';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { View, FlatList, StyleSheet, Image, Text, TouchableOpacity, Modal, ActivityIndicator } from 'react-native'
import { padding, fonts, margin, colors, radius } from '../../style';
import fetchUserData from '../../api unitlity function/fetchuserdata';
import { setBlogDetails } from '../../store/reducer/action/blogdetailaction';
import { blogdetail } from '../../api utility/apiutility';
import FastImage from 'react-native-fast-image';

class Blog extends Component {

    state = {
        token: '',
        lang: 'en',
        isloading: false,
    }
    componentDidMount = async () => {
        let token = await AsyncStorage.getItem('APITOKEN');
        let lang = await AsyncStorage.getItem('LANGUAGE');
        this.setState({ lang })
        this.setState({ token });
    }

    isLanguageEnglish = () => {
        if (this.props.language == 'en')
            return true
        return false
    }

    navigateUserToBlogDetails = async ({ item, index }) => {
        try {
            this.setState({ isloading: true })
            let uri = `${blogdetail}${item.id}`
            let response = await fetchUserData(uri, this.state.token, this.state.lang);
            await this.props.setBlogDetails(response.data);
            this.setState({ isloading: false })
            this.props.navigation.navigate('blogDetail', { 'blogindex': index });
        }
        catch (error) {
            this.setState({ isloading: false })
        }
    }

    renderActivity = () => {
        if (this.state.isloading)
            return (
                <Modal
                    visible={true}
                    transparent={true}
                >
                    <View style={{
                        position: 'absolute',
                        backgroundColor: 'rgba(0,0,0,0.3)',
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }} >
                        <ActivityIndicator size="large" color="#4ABEA1" />
                    </View>
                </Modal>

            )
        else
            return null
    }

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'space-between', marginTop: margin.lg }}>
                < this.renderActivity />
                <FlatList
                    data={this.props.blogs}
                    numColumns={2}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity onPress={() => this.navigateUserToBlogDetails({ item, index })}
                                style={{
                                    flex: 1, margin: margin.lg,
                                    alignItems: 'stretch',
                                    //alignItems: this.props.language == 'ar' ? 'flex-end' : 'flex-start'
                                }}>
                                <FastImage source={{ uri: item.image }}
                                    style={{ height: 150, width: null, borderRadius: radius.lg }}
                                    resizeMode='cover'>
                                </FastImage>
                                <View style={{
                                    backgroundColor: 'rgba(0,0,0,0.5)',
                                    ...StyleSheet.absoluteFill,
                                    borderRadius: radius.lg,
                                    padding: 10,
                                    justifyContent: 'flex-end'
                                }}>
                                    <Text style={{
                                        ...styles.titleStyle,
                                        textAlign: this.props.language == 'ar' ? 'right' : 'left'
                                    }}>
                                        {
                                            this.props.selected_language == 'en' ?
                                                item.heading
                                                :
                                                item.a_heading
                                        }
                                    </Text>
                                </View>

                            </TouchableOpacity>
                        )
                    }

                    }
                />
            </View >
        );
    }
}

const styles = {
    titleStyle: {
        fontSize: fonts.lg,
        fontWeight: 'bold',
        textAlign: 'left',
        color: colors.white

    },
    overlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        alignSelf: 'stretch',
        flex: 1,
        padding: padding.md,
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexWrap: 'wrap',
        backgroundColor: 'rgba(0,0,0,0.5)',
        borderRadius: radius.lg
    },
}


const mapStateToProp = state => {
    return {
        language: state.userDetail.userlanguage,
        blogs: state.rootReducer.bloglist,
        selected_language: state.userDetail.userlanguage,

    }
}


export default connect(mapStateToProp, { setBlogDetails })(Blog);

