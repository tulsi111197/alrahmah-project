import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';

class OfflineScreen extends Component {

    handleConnectivityChange = async () => {
        try {
            await fetch('https://randomuser.me/api/');
            await this.props.navigation.goBack();
        }
        catch (erro) {
        }
    }


    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Image style={style.imageStyle}
                    source={require('../../assets/neterror.png')}
                />
                <TouchableOpacity onPress={() => this.handleConnectivityChange()} >
                    <Text style={style.textStyle}>     Try again..    </Text>
                    <Text style={{ paddingLeft: 10, color: 'red' }}>Press Try again Button</Text>
                </TouchableOpacity>
            </View>
        )
    }
}


const style = StyleSheet.create({
    imageStyle: {
        width: 200,
        height: 200,
    },

    textStyle: {
        color: 'white',
        fontSize: 24,
        backgroundColor: 'green',
        marginTop: 90,
        borderRadius: 10
    }


})


export default OfflineScreen;