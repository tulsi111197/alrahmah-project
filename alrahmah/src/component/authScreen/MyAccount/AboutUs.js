import React, { Component } from 'react';
import { View, FlatList, Text, Modal, ActivityIndicator, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import WebView from 'react-native-webview';
import * as Action from '../../store/action/userAction';
import { OtherContent } from '../../Constent/AppConstent';
const hegint = Dimensions.get('window').height;
const width = Dimensions.get('window').height;


class History extends Component {

    state = {
        activity: true
    }

    renderActivity = () => {
        if (this.state.activity)
            return (

                <View style={{
                    position: 'absolute',
                    backgroundColor: 'rgba(0,0,0,0)',
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                    alignItems: 'center',
                    justifyContent: 'center',
                    zIndex: 1
                }} >
                    <ActivityIndicator size="large" color="#4ABEA1" />
                </View>
            )
        else
            return null
    }

    render() {
        const how_we_work = OtherContent.getAboutUSLink();
        return (
            <>
                <this.renderActivity />
                <WebView source={{
                    uri: this.props.selected_language == 'en' ?
                        how_we_work.en
                        :
                        how_we_work.ar
                }}
                    onLoadEnd={() => this.setState({ activity: false })} />
            </>
        )
    }
}

const mapStateToProp = state => {
    console.log(state.userDetail.userlanguage)
    return {
        myprograms: state.rootReducer.myprogram,
        userlang: state.rootReducer.userLanguage,
        selected_language: state.userDetail.userlanguage,
    }
}

export default connect(mapStateToProp)(History);