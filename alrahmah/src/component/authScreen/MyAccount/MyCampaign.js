import React, { Component } from 'react';
import { View } from 'react-native'
import { Text } from 'react-native-elements';
import { baseStyle } from '../../style';

class MyCampaign extends Component {
    calculatePercentages = (item) => {
        let percentages = (item.gain_fund / item.needed_fund) * 100
        return (parseInt(percentages));
    }
    render() {
        return (
            <View style={[baseStyle.appContainer, baseStyle.paddingLg]} >
                <Text> No Campaign Found </Text>
            </View>
        );
    }
}

const styles = {
    circularWrapper: {
        position: 'absolute',
        bottom: 0,
        right: 20,
        left: 'auto'
    }
}


export default MyCampaign;