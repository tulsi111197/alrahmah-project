import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, FlatList, ActivityIndicator, Image, Modal } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { setMyProgram } from '../../store/reducer/action/RootReducerAction';
import { Tile, Text } from 'react-native-elements';
import { baseStyle, padding } from '../../style';
import fetchUserData from '../../api unitlity function/fetchuserdata';
import { prgramdetail, volunteer_list } from '../../api utility/apiutility';
import { OtherContent } from '../../Constent/AppConstent'
import { setProgramDetails } from '../../store/reducer/action/programdetailaction';

class Program extends Component {

    state = {
        token: '',
        lang: 'en',
        isloading: false,
    }

    componentDidMount = async () => {
        let token = OtherContent.getToken();;
        let lang = this.props.selected_language;
        this.setState({ token });
        this.setState({ lang });
        this.setState({ isloading: true });
        let result = await fetchUserData(volunteer_list, OtherContent.getToken(), this.props.selected_language);
       
        this.props.setMyProgram(result.data);
        this.setState({ isloading: false });
    }

    navigateUserToProgramDetails = async ({ item }) => {
        this.setState({ isloading: true });
        let uri = `${prgramdetail}${item.program_id}`
        try {
            let response = await fetchUserData(uri, this.state.token, this.state.lang);
            await this.props.setProgramDetails(response.data);
            this.setState({ isloading: false });
            this.props.navigation.navigate('programDetail',{ from : 'myprogram' });
        }
        catch (error) {
            this.setState({ isloading: false })
        }
    }

    renderActivity = () => {
        if (this.state.isloading)
            return (
                <Modal
                    visible={true}
                    transparent={true}
                >
                    <View style={{
                        position: 'absolute',
                        backgroundColor: 'rgba(0,0,0,0.3)',
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }} >
                        <ActivityIndicator size="large" color="#4ABEA1" />
                    </View>
                </Modal>

            )
        else
            return null
    }

    render() {
        return (
            <View style={baseStyle.appContainer}>
                <this.renderActivity />
                <FlatList
                    data={this.props.program_list}
                    keyExtractor={item => item.id}
                    renderItem={({ item }) => {
                        if (item.programs) {
                          
                            return (
                                <View style={styles.contentContainer}>
                                    <Tile
                                        imageSrc={{ uri: item.programs.image }}
                                        title={item.programs.heading}
                                        titleStyle={{ textAlign: 'center' }}
                                        imageContainerStyle={{ borderRadius: 50 }}
                                        containerStyle={{ width: '100%', alignSelf: 'center', borderRadius: 50 }}
                                        onPress={() => this.navigateUserToProgramDetails({ item })}
                                    >
                                        <Text style={{ textAlign: 'center' }}>
                                            {
                                                item.programs.program_short_desc
                                            }
                                        </Text>
                                    </Tile>
                                </View>)
                        }
                    }
                    }
                />
            </View>
        );
    }
}

const styles = {
    contentContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: padding.lg,
    },
    titleStyle: {
        textAlign: 'center'
    }

}


const mapStateToProp = state => {
    return {
        program_list: state.rootReducer.myprogram,
        userlang: state.rootReducer.userLanguage,
        selected_language: state.userDetail.userlanguage
    }
}


export default connect(mapStateToProp, { setProgramDetails, setMyProgram })(Program);