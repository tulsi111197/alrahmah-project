
import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { Icon } from 'react-native-elements';
import {
    KeyboardAvoidingView, View, StyleSheet, Text, ActivityIndicator, Modal, FlatList, TouchableOpacity, Platform
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import { colors, fonts, baseStyle, padding, radius, margin } from '../../style';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import { setUser } from '../../store/action/userAction';
import { profileupdate } from '../../api utility/apiutility';
import AppButton from '../../utitlity/AppButton';
import AppInput from '../../utitlity/AppInput';
import AppCircularIMg from '../../utitlity/AppCircularIMg';
import PhoneInputIOS from '../../utitlity/phoneInput-ios';
import PhoneInputAndroid from '../../utitlity/phoneInput-Android';

import Counterys from '../../api utility/Countries';

class EditProfile extends Component {
    state = {
        imageuri: this.props.imageuri,
        imageresponse: false,
        f_name: this.props.fname,
        f_name_error: '',
        l_name: this.props.lname,
        l_name_error: '',
        mobile: this.props.mobile,
        address: this.props.address,
        address_error: '',
        city: this.props.city,
        city_error: '',
        state: this.props.state,
        state_error: '',
        phone: this.props.mobile,
        phone_error: '',

        token: '',
        error: '',
        isloading: false,
        isimageavailable: false,
        selectedcode: '+ 91',
        iscodemodelIOS: false,
        iscodemodelAndroid: false,
        filterCounterys: Counterys,
        countery_name: 'India',
        fromcountery: false,
    }

    componentDidMount = async () => {
        this.setCodeandPhone();
        const value = await AsyncStorage.getItem('APITOKEN');
        await this.setState({ token: value });
        let pic_response = await AsyncStorage.getItem('IMAGE_URI');
        let pic_result = JSON.parse(pic_response);
        this.setState({ imageuri: pic_result });
    }

    setCodeandPhone = async () => {
        let mobile = await AsyncStorage.getItem('MOBILE');
        if (mobile) {
            let dome = mobile.split(" ");
            this.setState({ selectedcode: dome[0] });
            this.setState({ phone: dome[1] });
        }
    }

    options = {
        title: 'Select Avatar',
        storageOptions: {
            skipBackup: true,
            path: 'images',
        },
    };

    imageBox = () => {
        ImagePicker.showImagePicker(this.options, response => {
            if (response.error) {

            }
            if (!response.didCancel) {
                this.setState({ imageresponse: response });
                this.setState({ imageuri: response.uri });
            }
        })
    }

    validateUserInput = () => {
        if (this.state.f_name && this.state.l_name && this.state.state && this.state.city && this.state.address && this.state.phone) {
            return true
        }
        return false
    }

    uploadDetail = async () => {
        this.setState({ mobile: this.state.selectedcode + ' ' + this.state.phone }, () => console.log('ready --->', this.state.mobile));
        let data = new FormData();
        data.append('mobile', `${this.state.selectedcode} ${this.state.phone}`);
        data.append('city', this.state.city);
        data.append('f_name', this.state.f_name);
        data.append('l_name', this.state.l_name);
        data.append('address', this.state.address);
        data.append('state', this.state.state);

        if (this.state.imageresponse) {
            if (Platform.OS == 'android') {
                data.append('user_image', {
                    uri: `file://${this.state.imageresponse.path}`,
                    type: this.state.imageresponse.type,
                    name: this.state.imageresponse.fileName
                })
            }
            else {
                data.append('user_image', {
                    uri: this.state.imageresponse.uri,
                    type: this.state.imageresponse.type,
                    name: 'profile.jpg'
                })
            }
        }
        else {
            data.append('user_image', this.state.imageuri);
        }

        let response = await fetch(profileupdate, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${this.state.token}`
            },
            body: data
        });
        let result = await response.json();
        console.log('response -->', result);
        this.setState({ imageuri: result.data.user_image });
        await this.storeStatus('IMAGE_URI', this.state.imageuri);
        this.props.setUser('IMAGE_URI', result.data.user_image);
        this.setState({ isloading: false });
        if (result.success) {
            if (this.props.navigation.getParam('from') == 'program')
                this.props.navigation.navigate('voulenteer')
            else
                this.props.navigation.navigate('account')
        }
    }

    navigateUser = () => {

        if (this.validateUserInput()) {
            this.setState({ isloading: true });
            this.storeDetail();
            this.uploadDetail();
        }
        else {
            this.setState({ isloading: false });
            this.setState({ error: this.props.userlang.profile_error });
        }
    }

    storeDetail = async () => {
        await this.storeStatus('F_NAME', this.state.f_name);
        await this.storeStatus('L_NAME', this.state.l_name);
        await this.storeStatus('MOBILE', this.state.mobile);
        await this.storeStatus('ADDRESS', this.state.address);
        await this.storeStatus('CITY', this.state.city);
        await this.storeStatus('STATE', this.state.state);
        await this.storeStatus('IMAGE_URI', this.state.imageuri);
        this.props.setUser('F_NAME', this.state.f_name);
        this.props.setUser('L_NAME', this.state.l_name);
        this.props.setUser('MOBILE', this.state.mobile);
        this.props.setUser('ADDRESS', this.state.address);
        this.props.setUser('CITY', this.state.city);
        this.props.setUser('STATE', this.state.state);
    }

    storeStatus = async (type, value) => {
        await AsyncStorage.setItem(type, value.toString());
    }

    renderActivity = () => {
        if (this.state.isloading) {
            return (
                <ActivityIndicator size="large" color="#4ABEA1" />
            )
        }
        return (
            <AppButton
                title={this.props.userlang.update}
                onPress={() => this.navigateUser()}
                containerStyle={baseStyle.alignSelfEnd}
            />
        )
    }


    renderImageUser = () => {
        if (!this.state.imageuri) {
            return (
                <AppCircularIMg
                    onPress={() => this.imageBox()}
                    profileCircle={{ borderColor: colors.green }}
                    source={require('../../assets/user-account.png')} />
            )
        }
        return (
            <AppCircularIMg
                onPress={() => this.imageBox()}
                profileCircle={{ borderColor: colors.green }}
                source={{ uri: this.state.imageuri }} />
        )
    }

    findWordForEach = (toSearch, item) => {
        if (toSearch.trim().toLowerCase() >= 'a' && toSearch.trim().toLowerCase() <= 'z') {
            let response = item.name.toLowerCase().search(toSearch.toLowerCase());
            if (response >= 0)
                return item
        }
        else {
            let response = item.dial_code.replace(/[^0-9]/g, '').search(toSearch.replace(/[^0-9]/g, ''));
            if (response >= 0)
                return item
        }


    }


    searchCode = (text) => {
        if (text == '') {
            this.setState({ filterCounterys: Counterys })
        }
        else {
            let result = Counterys.filter(item => this.findWordForEach(text, item));
            this.setState({ filterCounterys: result });
        }

    }


    renderCounteryListIOS = () => {
        if (this.state.iscodemodelIOS) {
            return (
                <Modal
                    visible={this.state.iscodemodelIOS}
                    transparent={true}
                >
                    <View style={{
                        backgroundColor: 'rgba(237,228,228,0.6)',
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'flex-end',
                    }} >

                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            backgroundColor: colors.white,
                            paddingHorizontal: padding.lg,
                            paddingVertical: 10,
                            borderTopEndRadius: 20,
                            borderTopStartRadius: 20
                        }}>
                            <AppInput
                                placeholder={this.props.userlang.search_by_code}
                                label={this.props.userlang.search}
                                lang={this.props.language}
                                containerStyle={{ width: '90%' }}
                                onChangeText={(text) => this.searchCode(text)}
                            />
                            <Icon name='close' iconStyle={{ backgroundColor: 'rgba(0,0,0,0.3)', padding: 8, color: 'white', }} onPress={() => this.setState({ iscodemodelIOS: false, fromcountery: false })} />

                        </View>

                        <View style={{
                            width: '100%',
                            height: 600,
                            alignSelf: 'flex-end',
                            borderRadius: radius.xl,
                            paddingHorizontal: padding.lg,
                            backgroundColor: colors.white
                        }}>
                            <FlatList
                                data={this.state.filterCounterys}
                                renderItem={({ item }) =>
                                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', marginBottom: margin.md }}>
                                        <TouchableOpacity style={{
                                            borderWidth: 1,
                                            padding: padding.lg,
                                            borderRadius: radius.lg,
                                            width: '100%',
                                            backgroundColor: colors.offBlack
                                        }}
                                            onPress={() => this.setState({
                                                iscodemodelIOS: false, selectedcode: item.dial_code,
                                                countery_name: item.name
                                            })}
                                        >
                                            <Text
                                                style={{
                                                    color: colors.textColor
                                                }} >
                                                {
                                                    this.state.fromcountery ?
                                                        `${item.flag}   ${item.name} ` :
                                                        `${item.flag}   ${item.dial_code} ${item.name} `}</Text>
                                        </TouchableOpacity>
                                    </View>
                                }
                            />
                        </View>
                    </View>
                </Modal>
            )
        }
        else
            return null
    }






    renderCounteryListAndroid = () => {
        if (this.state.iscodemodelAndroid) {
            return (
                <Modal
                    visible={this.state.iscodemodelAndroid}
                    transparent={false}
                >
                    <View style={{
                        backgroundColor: 'rgba(237,228,228,0.6)',
                        flex: 1,
                        flexDirection: 'column',
                        //justifyContent: 'flex-end',
                        alignItems: 'center'

                    }} >

                        <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, paddingHorizontal: padding.md }}>

                            <AppInput
                                placeholder={this.props.userlang.search_by_code}
                                lang={this.props.language}
                                label={this.props.userlang.search}
                                containerStyle={{ width: '90%' }}
                                onChangeText={(text) => this.searchCode(text)}
                            />
                            <Icon name='close' iconStyle={{ backgroundColor: 'rgba(0,0,0,0.3)', padding: 8, color: 'white', }} onPress={() => this.setState({ iscodemodelAndroid: false, fromcountery: false })} />

                        </View>

                        <View style={{
                            width: '100%',
                            height: 600,
                            alignSelf: 'flex-end',
                            borderRadius: radius.xl,
                            paddingHorizontal: padding.lg,
                            backgroundColor: colors.white
                        }}>
                            <FlatList
                                data={this.state.filterCounterys}
                                renderItem={({ item }) =>
                                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', marginBottom: margin.md }}>
                                        <TouchableOpacity style={{
                                            borderWidth: 1,
                                            padding: padding.lg,
                                            borderRadius: radius.lg,
                                            width: '100%',
                                            backgroundColor: colors.offBlack
                                        }}
                                            onPress={() => this.setState({
                                                iscodemodelAndroid: false, selectedcode: item.dial_code,
                                                countery_name: item.name
                                            })}
                                        >
                                            <Text
                                                style={{
                                                    color: colors.textColor
                                                }} >
                                                {
                                                    this.state.fromcountery ?
                                                        `${item.flag} ${item.name} ` :

                                                        `${item.flag} ${item.dial_code} ${item.name} `}</Text>
                                        </TouchableOpacity>
                                    </View>
                                }
                            />
                        </View>


                    </View>
                </Modal>
            )
        }
        else
            return null
    }





    render() {

        return (

            <View style={baseStyle.appContainer} >
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'android' ? 'none' : 'position'}
                    enabled
                    style={{
                        flex: Platform.OS === 'android' ? 0 : 1,
                    }}>
                    <this.renderCounteryListAndroid />
                    <this.renderCounteryListIOS />
                    <ScrollView>

                        <View style={styles.avatarStyle}>
                            <View style={styles.container}>
                                {this.renderImageUser()}
                            </View>
                            <Text style={styles.name}>{this.props.username}</Text>
                            <Text style={styles.phone}>{this.props.mobile}</Text>
                        </View>

                        <View style={styles.listWrapper}>

                            <AppInput
                                label={this.props.userlang.first_name}
                                value={this.state.f_name}
                                onChangeText={(f_name) => this.setState({ f_name })}
                                lang={this.props.language}
                                placeholder={`${this.props.userlang.placeholder} ${this.props.userlang.first_name}`} />

                            <AppInput
                                label={this.props.userlang.last_name}
                                value={this.state.l_name}
                                lang={this.props.language}
                                onChangeText={(l_name) => this.setState({ l_name })}
                                placeholder={`${this.props.userlang.placeholder} ${this.props.userlang.last_name}`} />
                            {
                                Platform.OS === 'android'

                                    ?
                                    <PhoneInputAndroid
                                        label={this.props.userlang.mobile_no}
                                        code={this.state.selectedcode}
                                        onPress={() => this.setState({ iscodemodelAndroid: true })}
                                        value={this.state.phone}
                                        lang={this.props.language}
                                        onChangeText={(text) => this.setState({ phone: text })}
                                        placeholder={`${this.props.userlang.placeholder} ${this.props.userlang.mobile_no}`}
                                    />
                                    :

                                    <PhoneInputIOS
                                        label={this.props.userlang.mobile_no}
                                        code={this.state.selectedcode}
                                        onPress={() => this.setState({ iscodemodelIOS: true })}
                                        value={this.state.phone}
                                        lang={this.props.language}
                                        onChangeText={(text) => this.setState({ phone: text })}
                                        placeholder={`${this.props.userlang.placeholder} ${this.props.userlang.mobile_no}`}
                                    />
                            }


                            <AppInput
                                label={this.props.userlang.email}
                                value={this.props.email}
                                editable={false}
                                lang={this.props.language}
                                onChangeText={(name) => this.setState({ name })}
                                placeholder={`${this.props.userlang.placeholder} ${this.props.userlang.email}`} />
                            <AppInput
                                label={this.props.userlang.address}
                                value={this.state.address}
                                lang={this.props.language}
                                onChangeText={(address) => this.setState({ address })}
                                placeholder={`${this.props.userlang.placeholder} ${this.props.userlang.address}`} />

                            <AppInput
                                label={this.props.userlang.city}
                                value={this.state.city}
                                lang={this.props.language}
                                onChangeText={(city) => this.setState({ city })}
                                placeholder={`${this.props.userlang.placeholder} ${this.props.userlang.city}`} />

                            <AppInput
                                label={this.props.userlang.state}
                                value={this.state.state}
                                lang={this.props.language}
                                onChangeText={(state) => this.setState({ state })}
                                placeholder={`${this.props.userlang.placeholder} ${this.props.userlang.state}`} />


                            <Text style={{ fontSize: 16, textAlign: this.props.language == 'ar' ? 'right' : 'left' }}>{this.props.userlang.country}
                            </Text>
                            {
                                Platform.OS === 'android'
                                    ?

                                    <TouchableOpacity
                                        onPress={() => this.setState({ iscodemodelAndroid: true, fromcountery: true })}
                                        style={{
                                            paddingVertical: 13,
                                            paddingHorizontal: padding.lg,
                                            backgroundColor: colors.inputColor,
                                            alignItems: this.props.language == 'ar' ? 'flex-end' : 'flex-start'
                                        }}>
                                        <Text style={{
                                            color: colors.black, fontSize: 16,
                                            textAlign: this.props.language == 'ar' ? 'right' : 'left'
                                        }} >{this.state.countery_name}</Text>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity
                                        onPress={() => this.setState({ iscodemodelIOS: true, fromcountery: true })}
                                        style={{
                                            paddingVertical: 13,
                                            paddingHorizontal: padding.lg,
                                            backgroundColor: colors.inputColor,
                                            alignItems: this.props.language == 'ar' ? 'flex-end' : 'flex-start'
                                        }}>
                                        <Text style={{
                                            color: colors.black, fontSize: 16,
                                            textAlign: this.props.language == 'ar' ? 'right' : 'left'
                                        }} >{this.state.countery_name}</Text>
                                    </TouchableOpacity>
                            }

                            <View style={{ marginVertical: 10}}>
                                <Text style={{
                                    color: 'red', fontSize: 15,
                                    textAlign: this.props.language == 'ar' ? 'right' : 'left'
                                }} >{this.state.error}</Text>
                            </View>
                            <this.renderActivity />

                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>


        );
    }



}

const styles = StyleSheet.create({
    container: {
        width: 150,
        height: 150,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    firstProgressLayer: {
        width: 140,
        height: 140,
        borderWidth: 6,
        borderRadius: 100,
        position: 'absolute',
        borderLeftColor: 'transparent',
        borderBottomColor: 'transparent',
        borderRightColor: colors.white,
        borderTopColor: colors.white,
        transform: [{ rotateZ: '-135deg' }]
    },
    secondProgressLayer: {
        width: 140,
        height: 140,
        position: 'absolute',
        borderWidth: 6,
        borderRadius: 100,
        borderLeftColor: 'transparent',
        borderBottomColor: 'transparent',
        borderRightColor: colors.white,
        borderTopColor: colors.white,
        transform: [{ rotateZ: '45deg' }]
    },
    offsetLayer: {
        width: 140,
        height: 140,
        position: 'absolute',
        borderWidth: 6,
        borderRadius: 100,
        borderLeftColor: 'transparent',
        borderBottomColor: 'transparent',
        borderRightColor: colors.green,
        borderTopColor: colors.green,
        transform: [{ rotateZ: '-135deg' }]
    },
    avatarStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.white,
        height: 300
    },
    name: {
        fontSize: 26,
        fontWeight: 'bold',
        color: colors.labelColor,
        textTransform: 'uppercase'
    },
    phone: {
        color: colors.labelColor,
        fontSize: fonts.xl
    },
    listtext: {
        fontSize: fonts.xl
    },
    listWrapper: {
        padding: padding.lg
    }
});

const mapStateToProp = state => {
    let username = '';
    if (state.userDetail.f_name == null)
        username = state.userDetail.username
    else
        username = state.userDetail.f_name + " " + state.userDetail.l_name;
    const user = state.userDetail
    return {
        fname: user.f_name,
        city: user.city,
        state: user.state,
        mobile: user.mobile,
        lname: user.l_name,
        address: user.address,
        email: user.email,
        username: username,
        userlang: state.rootReducer.userLanguage,
        imageuri: user.image,
        language: state.userDetail.userlanguage,

    }
}


export default connect(mapStateToProp, { setUser })(EditProfile);