
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet, Text, FlatList, ScrollView, Image } from 'react-native';
import { colors, margin, fonts, baseStyle, radius, padding } from '../../style'
import AppListItem from '../../utitlity/AppListRevers';

class MyDonation extends Component {

    render() {
        return (
            <View style={baseStyle.appContainer}>
                <ScrollView>
                    <View style={styles.avatarStyle}>
                        <Text style={styles.amount}>{this.props.userlang.sar}-{this.props.my_donation}</Text>
                        <Text style={styles.subTitle}>{this.props.userlang.mydonation}</Text>
                    </View>
                    <View style={styles.listWrapper}>
                        <FlatList
                            data={this.props.mydonation.campaing_list}
                            keyExtractor={item => item.id}
                            style={{ transform: [{ scaleY: -1 }] }}
                            renderItem={({ item }) =>
                                <AppListItem
                                    title={item.lang_campaing ? item.lang_campaing.campaing_name : "can't found name in selected language"}
                                    titleStyle={styles.listTitle}
                                    subtitle={<Text style={styles.subAmount}> Donate - SAR-{item.donate_amount}</Text>}
                                    containerStyle={{ backgroundColor: 'transparent' }}
                                    leftAvatar={
                                        <Image source={item.lang_campaing ? { uri: item.lang_campaing.patient_image } : require('../../assets/no-image.jpg')} style={styles.listImg} />
                                    }
                                />

                            }
                        />
                    </View>

                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    avatarStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.green,
        height: 250
    },
    amount: {
        fontSize: 36,
        fontWeight: 'bold',
        color: colors.white,
        textTransform: 'uppercase'
    },
    subTitle: {
        color: colors.white,
        fontSize: fonts.xl,
        fontWeight: 'bold'
    },
    subAmount: {
        color: colors.green,
        fontSize: fonts.xl
    },

    listTitle: {
        fontSize: fonts.xxl,
        fontWeight: 'bold',
        marginBottom: margin.lg
    },
    listImg: {
        height: 120,
        width: 140,
        borderRadius: radius.lg
    },
    listWrapper: {
        backgroundColor: colors.white,
        marginTop: -20,
        paddingTop: padding.lg,
        borderTopLeftRadius: radius.lg,
        borderTopRightRadius: radius.lg,
        backgroundColor: colors.white
    },
    titleStyle: {
        color: colors.black,
        fontSize: fonts.xl
    }

});

const mapStateToProps = State => {
    let donate= parseInt(State.rootReducer.dashboardresponse.total_donate) + parseInt(State.rootReducer.myprogram.total_donate);
    return {
        my_donation: donate,
        mydonation: State.rootReducer.mydonation,
        userlang: State.rootReducer.userLanguage,
        total_donate: State.rootReducer.dashboardresponse.total_donate,
    }
}


export default connect(mapStateToProps)(MyDonation);