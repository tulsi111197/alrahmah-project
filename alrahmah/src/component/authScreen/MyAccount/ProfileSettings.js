import React, { Component } from 'react';
import { View } from 'react-native';
import AppListItem from '../../utitlity/AppListItem';
import { colors, borderWidth, margin, baseStyle } from '../../style';
import AppSwitch from '../../utitlity/AppSwitch';
import { Connect, connect } from 'react-redux';

class ProfileSettings extends Component {

  render() {
    const lang = this.props.language;
    return (
      <View style={[baseStyle.appContainer, baseStyle.paddingLg]}>
        <AppListItem
          onPress={() => this.props.navigation.navigate('profileLanguage')}
          title={this.props.userlang.language_setting}
          titleStyle={{ textAlign: lang == 'ar' ? 'right' : 'left' }}
          containerStyle={styles.containerStyle}
        />

        {/* <AppListItem
          title="Notification Setting"
          containerStyle={[styles.containerStyle,styles.marginZero]}
        /> */}
        {/* <AppListItem
          title={this.props.userlang.notification}
          rightElement={<AppSwitch/>}
          containerStyle={styles.containerStyle}
        /> */}
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    backgroundColor: colors.inputColor,
    borderLeftWidth: borderWidth.xl,
    borderColor: colors.green,
    marginBottom: margin.lg
  },
  marginZero: {
    marginBottom: 0
  }
}

const mapStateToProp = state => {

  return {

    userlang: state.rootReducer.userLanguage,
    language: state.userDetail.userlanguage
  }
}


export default connect(mapStateToProp)(ProfileSettings);