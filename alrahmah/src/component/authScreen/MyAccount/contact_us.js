import React, { Component } from 'react';
import { View, FlatList, Text, Modal, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import WebView from 'react-native-webview';
import { OtherContent } from '../../Constent/AppConstent';

class ContactUs extends Component {

    state = {
        activity: true
    }

    renderActivity = () => {
        if (this.state.activity)
            return (

                <View style={{
                    position: 'absolute',
                    backgroundColor: 'rgba(0,0,0,0)',
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                    alignItems: 'center',
                    justifyContent: 'center',
                    zIndex: 1
                }} >
                    <ActivityIndicator size="large" color="#4ABEA1" />
                </View>
            )
        else
            return null
    }

    render() {
        const how_we_work = OtherContent.getContactUSLink();

        return (
            <>
             <this.renderActivity/>
                <WebView source={{
                    uri: this.props.selected_language == 'en' ?
                        how_we_work.en
                        :
                        how_we_work.ar
                }}
                    onLoadEnd={() => this.setState({ activity: false })} />
            </>
        )
    }
}

const mapStateToProp = state => {
    return {
        myprograms: state.rootReducer.myprogram,
        selected_language: state.userDetail.userlanguage,
    }
}

export default connect(mapStateToProp)(ContactUs);