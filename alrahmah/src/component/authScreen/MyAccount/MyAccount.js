import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Text,
    Image,
    TouchableOpacity,
    Linking,
    Alert
} from 'react-native';
import { colors, margin, fonts, baseStyle, padding, radius } from '../../style';
import { connect } from 'react-redux';
import { ScrollView } from 'react-native-gesture-handler';
import AppButton from '../../utitlity/AppButton';
import AppCircularIMg from '../../utitlity/AppCircularIMg';
import AsyncStorage from '@react-native-community/async-storage';
import HowWeWork from './History';
import FastImage from 'react-native-fast-image';
import { OtherContent } from '../../Constent/AppConstent';
import {
    getFlexDirection, getAlignItems, isLangArabic,
    getTextAlign
} from '../../Constent/helperMethods/langMethods';

class MyAccount extends Component {
    state = {
        isimageLoading: false,
    }
    componentDidMount() {

    }
    logoutFromApp = async () => {
        Alert.alert(
            '',
            this.props.userlang.do_you_want_to_logout,
            [
                { text: this.props.userlang.cancel, onPress: () => console.log('Cancel Pressed') },
                { text: this.props.userlang.ok, onPress: () => this.callLogout() },
            ],
            { cancelable: false }
        )
    }
    callLogout = async () => {

        await AsyncStorage.clear();
        OtherContent.setToken(null);
        await this.props.navigation.navigate('welcome');
    }
    aboutUsPage = () => {
        Linking.openURL('http://iwebwiser.com/alrehmah/public/alrahamah')
    }

    dialCall = () => {
        // Linking.openURL('http://iwebwiser.com/alrehmah/public/alrahamah/contact');
        this.props.navigation.navigate('contactus');
    };

    howWeWork = () => {
        Linking.openURL('http://iwebwiser.com/alrehmah/public/alrahamah/work');
    }
    renderAnimatedScreen = () => {
        if (this.state.isimageLoading)
            return (
                <FastImage
                    source={require('../../assets/loading-1.gif')}
                    style={{
                        width: 30,
                        height: 30,
                        position: 'absolute',
                        flex: 1,
                        top: 45,
                        left: 45,
                        justifyContent: 'center'
                    }}
                />
            )
        else
            return null

    }

    renderImageUser = () => {
        if (this.props.image == 'false') {
            return (
                <AppCircularIMg
                    onPress={() => this.navigateToEditProfile()}
                    profileCircle={{ borderColor: colors.white }}
                    source={require('../../assets/user-account.png')} />
            )
        }
        return (
            <View>
                <AppCircularIMg
                    onLoadStart={() => this.setState({ isimageLoading: true })}
                    onLoadEnd={() => this.setState({ isimageLoading: false })}
                    onPress={() => this.navigateToEditProfile()}
                    source={{ uri: this.props.image ? this.props.image.replace(/"/g, '') : this.props.image }} />
                {this.renderAnimatedScreen()}
            </View>
        )
    }

    navigateToEditProfile = () => {
        if (OtherContent.getToken() == null) {
            Alert.alert(
                '',
                this.props.userlang.you_need_to_login_first,
                [
                    { text: this.props.userlang.cancel, onPress: () => console.log('Cancel Pressed') },
                    { text: this.props.userlang.ok, onPress: () => this.callLogout() },
                ],
                { cancelable: false }
            )
        }
        else {
            this.props.navigation.navigate('editProfile')
        }
    }


    navigateToMyProgram = () => {
        if (OtherContent.getToken() == null) {
            Alert.alert(
                '',
                this.props.userlang.you_need_to_login_first,
                [
                    { text: this.props.userlang.cancel, onPress: () => console.log('Cancel Pressed') },
                    { text: this.props.userlang.ok, onPress: () => this.callLogout() },
                ],
                { cancelable: false }
            )
        }
        else {
            this.props.navigation.navigate('myProgram')
        }
    }

    render() {
        return (
            <View style={baseStyle.appContainer} >
                <ScrollView>
                    <View style={styles.avatarStyle}>
                        <View style={styles.container}>
                            {this.renderImageUser()}
                            <TouchableOpacity onPress={() => this.navigateToEditProfile()}>

                            </TouchableOpacity>
                        </View>
                        <Text style={styles.name}>{this.props.username}</Text>
                        <Text style={styles.phone}>{this.props.phone}</Text>
                    </View>

                    <View style={{
                        ...styles.listWrapper,
                        //flexDirection: getFlexDirection(this.props.language)
                    }}>
                        <TouchableOpacity style={{
                            ...styles.listStyle,
                            flexDirection: getFlexDirection(this.props.language)
                        }} onPress={() => this.navigateToEditProfile()}>
                            <Image source={require('../../assets/profile.png')} style={styles.iconStyle} />
                            <Text style={{
                                ...styles.listtext,
                                paddingRight: isLangArabic(this.props.language) ? 10 : 0,
                                textAlign: getTextAlign(this.props.language)
                            }}>{this.props.userlang.my_profile}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{
                            ...styles.listStyle,
                            flexDirection: getFlexDirection(this.props.language),
                            
                        }} onPress={() => this.props.navigation.navigate('donate')}>
                            <Image source={require('../../assets/donation.png')} style={styles.iconStyle} />
                            <Text style={{
                                ...styles.listtext,
                                paddingRight: isLangArabic(this.props.language) ? 10 : 0,
                                textAlign: getTextAlign(this.props.language)
                            }}>{this.props.userlang.donate_now}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{
                            ...styles.listStyle,
                            flexDirection: getFlexDirection(this.props.language),
                        }} onPress={() => this.navigateToMyProgram()}>
                            <Image source={require('../../assets/program.png')} style={styles.iconStyle} />
                            <Text style={{
                                ...styles.listtext,
                                paddingRight: isLangArabic(this.props.language) ? 10 : 0,
                                textAlign: getTextAlign(this.props.language)
                            }}>{this.props.userlang.my_program}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{
                             ...styles.listStyle,
                             flexDirection: getFlexDirection(this.props.language),
                        }} onPress={() => this.props.navigation.navigate('profileSetting')}>
                            <Image source={require('../../assets/setting.png')} style={styles.iconStyle} />
                            <Text style={{
                                ...styles.listtext,
                                paddingRight: isLangArabic(this.props.language) ? 10 : 0,
                                textAlign: getTextAlign(this.props.language)
                            }}>{this.props.userlang.setting}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{
                            ...styles.listStyle,
                            flexDirection: getFlexDirection(this.props.language)
                        }} onPress={() => this.props.navigation.navigate('howwework')} >
                            <Image source={require('../../assets/work.png')} style={styles.iconStyle} />
                            <Text style={{
                                ...styles.listtext,
                                paddingRight: isLangArabic(this.props.language) ? 10 : 0,
                                textAlign: getTextAlign(this.props.language)
                            }}>{this.props.userlang.how_we_work}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{
                            ...styles.listStyle,
                            flexDirection: getFlexDirection(this.props.language)
                        }} onPress={() => this.props.navigation.navigate('aboutus')} >
                            <Image source={require('../../assets/about.png')} style={styles.iconStyle} />
                            <Text style={{
                                ...styles.listtext,
                                paddingRight: isLangArabic(this.props.language) ? 10 : 0,
                                textAlign: getTextAlign(this.props.language)
                            }}>{this.props.userlang.about_us}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{
                            ...styles.listStyle,
                            flexDirection: getFlexDirection(this.props.language)
                        }} onPress={() => this.dialCall()} >
                            <Image source={require('../../assets/contact.png')} style={styles.iconStyle} />
                            <Text style={{
                                ...styles.listtext,
                                paddingRight: isLangArabic(this.props.language) ? 10 : 0,
                                textAlign: getTextAlign(this.props.language)
                            }}>{this.props.userlang.contact_us}</Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>
                <View style={baseStyle.paddingLg}>
                    <AppButton
                        title={this.props.userlang.log_out}
                        buttonStyle={[baseStyle.bgRedColor,baseStyle.alignSelfEnd]} onPress={() => this.logoutFromApp()} />
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        width: 150,
        height: 150,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    firstProgressLayer: {
        width: 140,
        height: 140,
        borderWidth: 6,
        borderRadius: 100,
        position: 'absolute',
        borderLeftColor: 'transparent',
        borderBottomColor: 'transparent',
        borderRightColor: colors.white,
        borderTopColor: colors.white,
        transform: [{ rotateZ: '-135deg' }]
    },
    secondProgressLayer: {
        width: 140,
        height: 140,
        position: 'absolute',
        borderWidth: 6,
        borderRadius: 100,
        borderLeftColor: 'transparent',
        borderBottomColor: 'transparent',
        borderRightColor: colors.white,
        borderTopColor: colors.white,
        transform: [{ rotateZ: '45deg' }]
    },
    offsetLayer: {
        width: 140,
        height: 140,
        position: 'absolute',
        borderWidth: 6,
        borderRadius: 100,
        borderLeftColor: 'transparent',
        borderBottomColor: 'transparent',
        borderRightColor: colors.green,
        borderTopColor: colors.green,
        transform: [{ rotateZ: '-135deg' }]
    },
    avatarStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.green,
        height: 300
    },
    name: {
        fontSize: 24,
        fontWeight: 'bold',
        color: colors.white,
        textTransform: 'uppercase'
    },
    phone: {
        color: colors.white,
        fontSize: fonts.xl
    },
    listStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: padding.lg
    },
    iconStyle: {
        height: 30,
        width: 25,
        resizeMode: 'contain',
        marginRight: margin.lg
    },
    listWrapper: {
        backgroundColor: colors.white,
        marginTop: -20,
        borderTopLeftRadius: radius.lg,
        borderTopRightRadius: radius.lg,
        backgroundColor: colors.white,
        padding:padding.lg
    },
    listtext: {
        fontSize: fonts.xl
    }
});

const mapStateToProp = state => {

    let username = '';
    if (state.userDetail.f_name == null)
        username = state.userDetail.username
    else
        username = state.userDetail.f_name + " " + state.userDetail.l_name

    return {
        username: username,
        phone: state.userDetail.mobile,
        image: state.userDetail.image,
        userlang: state.rootReducer.userLanguage,
        language: state.userDetail.userlanguage,
    }
}


export default connect(mapStateToProp, null)(MyAccount);