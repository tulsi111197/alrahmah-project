import React from 'react';
import { View, FlatList } from 'react-native';
import { Tile, Text } from 'react-native-elements';
//import program from '../../data/data';
import { baseStyle } from '../../style';


 class UpComingProgram extends React.Component {
     componentDidMount(){
         alert();
     }
    render(){
        return (
            <View style={baseStyle.appContainer}>
                <FlatList
                    data={program}
                    renderItem={({ item }) =>
                        <View style={baseStyle.paddingLg}>
                            <Tile
                                imageSrc={item.image}
                                title={item.title}
                                imageContainerStyle={{ boederRadius: 50 }}
                                containerStyle={{ width: '100%', alignSelf: 'center', borderRadius: 50 }}
                            >
    
                                <Text>{item.date}</Text>
                                <Text>{item.description}</Text>
                            </Tile>
                        </View>}
                />
            </View>
        );
    }
    
}

export default UpComingProgram;