import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ActivityIndicator, Modal } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { setUser } from '../../store/action/userAction';
import AppListItem from '../../utitlity/AppListItem';
import { colors, borderWidth, margin, baseStyle } from '../../style';
import { mydonationuri, dashboard, programlist, bloglist } from '../../api utility/apiutility';
import fechData from '../../api unitlity function/fetchuserdata';
import { setLanguageArabic, setLanguageEnglish } from '../../store/reducer/action/userAction';
import { setStaticLangArabic, setStaticLangEnglish, setMyDonation, setRootReduser, setProgramList, setBlogList } from '../../store/reducer/action/RootReducerAction';
import AppButton from '../../utitlity/AppButton';
import { OtherContent } from '../../Constent/AppConstent';
class ProfileLanguage extends Component {
    state = {
        isLangAreEnglish: true,
        apiToken: '',
        isloading: false
    }
    componentDidMount = async () => {
        let token = await AsyncStorage.getItem('APITOKEN');
        this.setState({ apiToken: token })
        if (this.props.language == 'en') {
            this.setState({ isLangAreEnglish: true })
        }
        else {
            this.setState({ isLangAreEnglish: false })
        }
    }
    changeLanguageEnglish = async () => {
        this.setState({ isLangAreEnglish: true })
    }

    changeLanguageArabic = async () => {
        this.setState({ isLangAreEnglish: false })
    }

    storeUserLanguageByAsyncStorage = async () => {
        this.setState({ isloading: true });
        if (this.state.isLangAreEnglish) {
            this.props.setLanguageEnglish();
            await AsyncStorage.setItem('LANGUAGE','en');
        }
        else {
            this.props.setLanguageArabic();
            await AsyncStorage.setItem('LANGUAGE','ar');

        }
        
        await this.navigationUserAndCollectApiData();
        this.props.navigation.navigate('account')
    }

    navigationUserAndCollectApiData = async () => {
        let dashboardresponse = await fechData(
            dashboard, '', this.state.isLangAreEnglish ? 'en' : 'ar');
        let program = await fechData(
            programlist, '', this.state.isLangAreEnglish ? 'en' : 'ar');

        this.state.isLangAreEnglish ? this.props.setStaticLangEnglish() : this.props.setStaticLangArabic();
        this.props.setRootReduser(dashboardresponse.data);
        this.props.setProgramList(program.data.reverse());
        
        if (OtherContent.getToken() == null) {
            this.props.setUser('USERNAME',
                this.state.isLangAreEnglish ? 'Guest' : 'زائر');
            this.props.setUser('F_NAME',
                this.state.isLangAreEnglish ? 'Guest' : 'زائر');
            this.props.setUser('L_NAME',
                this.state.isLangAreEnglish ? 'User' : 'المستعمل');

        }


        this.setState({ isloading: false });
    }

    renderActivity = () => {
        return (
            <AppButton
                title={this.props.userlang.done}
                onPress={() => this.storeUserLanguageByAsyncStorage()}
            />
        )
    }

    renderActivityModal = () => {
        if (this.state.isloading)
            return (
                <Modal
                    visible={true}
                    transparent={true}
                >
                    <View style={{
                        position: 'absolute',
                        backgroundColor: 'rgba(0,0,0,0.3)',
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }} >
                        <ActivityIndicator size="large" color="#4ABEA1" />
                    </View>
                </Modal>

            )
        else
            return null
    }

    render() {
        return (
            <View style={[baseStyle.appContainer, baseStyle.paddingLg]}>
                <this.renderActivityModal />
                <AppListItem
                    title="English"
                    containerStyle={this.state.isLangAreEnglish ? styles.containerStyle : null}
                    onPress={this.changeLanguageEnglish}
                />
                <AppListItem
                    title="عربى"
                    containerStyle={this.state.isLangAreEnglish ? null : styles.containerStyle}
                    onPress={this.changeLanguageArabic}
                    titleStyle={{ textAlign: 'right' }}
                />
                <this.renderActivity />

            </View>
        );
    }
}

const styles = {
    containerStyle: {
        backgroundColor: colors.inputColor,
        borderLeftWidth: borderWidth.xl,
        borderColor: colors.green,
        marginBottom: margin.lg
    }
}

const mapStateToProp = state => {
    return {
        userlang: state.rootReducer.userLanguage,
        language: state.userDetail.userlanguage
    }
}

export default connect(mapStateToProp, { setStaticLangArabic, setStaticLangEnglish, setLanguageArabic, setLanguageEnglish, setMyDonation, setRootReduser, setBlogList, setProgramList, setUser })(ProfileLanguage);