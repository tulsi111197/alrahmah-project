import React, { Component } from 'react';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { View, Image, FlatList, TouchableOpacity, ScrollView, StyleSheet } from 'react-native';
import { Text, Card, Avatar } from 'react-native-elements'
import { setUser } from '../../store/action/userAction';
import { baseStyle, fonts, colors, padding, radius, margin } from '../../style';
import AppListItem from '../../utitlity/AppListItem';
import { setRootReduser } from '../../store/reducer/action/RootReducerAction';
import { set_Arabic, set_English } from '../../store/reducer/action/userAction';
import notificationService from '../../api unitlity function/notification';
import { OtherContent } from '../../Constent/AppConstent';
import {
    getFlexDirection, getAlignItems, isLangArabic,
    getTextAlign
} from '../../Constent/helperMethods/langMethods';
import { NavigationEvents } from 'react-navigation';
import { color } from 'react-native-reanimated';


class Dashboard extends Component {
    state = {
        language: '',
        lang: this.props.language,
    }

    isLanguageEnglish = () => {
        if (this.props.language == 'en')
            return true
        return false
    }

    getUserDataByLocalStore = async () => {
        if (OtherContent.getToken() == null) {
            this.props.setUser('USERNAME',
                this.isLanguageEnglish() ? 'Guest' : 'زائر');
            this.props.setUser('EMAIL', await AsyncStorage.getItem('EMAIL'));
            let pic_response = await AsyncStorage.getItem('IMAGE_URI');
            await this.props.setUser('IMAGE_URI', pic_response);
            this.props.setUser('F_NAME', this.isLanguageEnglish() ? 'Guest' : 'زائر');
            this.props.setUser('L_NAME', this.isLanguageEnglish() ? 'User' : 'المستعمل');
            this.props.setUser('MOBILE', await AsyncStorage.getItem('MOBILE'));
            this.props.setUser('ADDRESS', await AsyncStorage.getItem('ADDRESS'));
            this.props.setUser('IMAGE_URI', await AsyncStorage.getItem('IMAGE_URI'));
            this.props.setUser('CITY', await AsyncStorage.getItem('CITY'));
            this.props.setUser('STATE', await AsyncStorage.getItem('STATE'));

        }
        else {
            this.props.setUser('USERNAME', await AsyncStorage.getItem('USERNAME'));
            this.props.setUser('EMAIL', await AsyncStorage.getItem('EMAIL'));
            let pic_response = await AsyncStorage.getItem('IMAGE_URI');
            await this.props.setUser('IMAGE_URI', pic_response);
            this.props.setUser('F_NAME', await AsyncStorage.getItem('F_NAME'));
            this.props.setUser('L_NAME', await AsyncStorage.getItem('L_NAME'));
            this.props.setUser('MOBILE', await AsyncStorage.getItem('MOBILE'));
            this.props.setUser('ADDRESS', await AsyncStorage.getItem('ADDRESS'));
            this.props.setUser('IMAGE_URI', await AsyncStorage.getItem('IMAGE_URI'));
            this.props.setUser('CITY', await AsyncStorage.getItem('CITY'));
            this.props.setUser('STATE', await AsyncStorage.getItem('STATE'));
        }
    }

    async componentDidMount() {
        this.getUserDataByLocalStore();
        this.setState({ lang: this.props.language })
    }


    getFlexDirection(lang) {
        if (lang == 'ar') {
            return 'row-reverse'
        }
        return 'row'
    }

    render() {
        const lang = this.props.language;
        return (
            <View style={[baseStyle.appContainer, baseStyle.paddingLg]}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text style={{ ...styles.heading, textAlign: lang == 'ar' ? 'right' : 'left' }}>
                        {this.props.userlang.welcome},
                        {` ${this.props.username}`}</Text>
                    <View style={styles.cardStyle}>
                        <View style={{
                            ...styles.cardInner,
                            flexDirection: getFlexDirection(this.props.language)
                            //backgroundColor: 'red'
                        }}>
                            <Image source={require('../../assets/icon_care.png')} style={styles.iconStyle} />
                            <View style={{ paddingRight: isLangArabic(this.props.language) ? 10 : 0 }}>
                                <Text style={{
                                    ...styles.textStyle,
                                    textAlign: getTextAlign(this.props.language)
                                }}>{this.props.userlang.contribution}</Text>
                                <Text style={{
                                    ...styles.heading2,
                                    textAlign: getTextAlign(this.props.language)
                                }}>{this.props.userlang.society}</Text>
                                <Text style={{
                                    ...styles.heading2,
                                    textAlign: getTextAlign(this.props.language)
                                }}>{this.props.userlang.sar}, {this.props.total_donate}</Text>
                            </View>

                        </View>
                        <View style={{
                            width: '100%',
                            alignItems: getAlignItems(this.props.language),
                        }}>
                            <TouchableOpacity
                                style={{ transform: [{ rotate: isLangArabic(this.props.language) ? '180deg' : '0deg' }] }}
                                onPress={() => this.props.navigation.navigate('donate')}>
                                <Image source={require('../../assets/right-arrow.png')} style={styles.rightArrow} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{
                        ...styles.row,
                        flexDirection: getFlexDirection(this.props.language)
                    }}>
                        <Text style={styles.contentHeading}>{this.props.userlang.campaigns}</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('treatmentNeedy', { categoriesid: 'All' })}>
                            <Text>{this.props.userlang.view_all}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.row}>
                        {/* campaign detail goes here */}
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('treatmentNeedy', { categoriesid: 1 })}>
                            <Card containerStyle={styles.cardContainer}>
                                <Image source={require('../../assets/watch.png')} style={styles.iconImg} />
                                <Text style={{ color: colors.green, alignSelf: 'center', fontSize: 12,fontWeight:'400' }}>{this.props.userlang.device}</Text>
                            </Card>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('treatmentNeedy', { categoriesid: 2 })}>
                            <Card containerStyle={styles.cardContainer}>
                                <Image source={require('../../assets/heartbeat.png')} style={styles.iconImg} />
                                <Text style={{ color: colors.green, alignSelf: 'center', fontSize: 12, textAlign: 'center',fontWeight:'400' }}>{this.props.userlang.treatment}</Text>
                            </Card>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('treatmentNeedy', { categoriesid: 3 })}>
                            <Card containerStyle={styles.cardContainer}>
                                <Image source={require('../../assets/meds.png')} style={styles.iconImg} />
                                <Text style={{ color: colors.green, alignSelf: 'center', fontSize: 12,fontWeight:'400', }}>
                                    {this.props.userlang.medicine}</Text>
                            </Card>
                        </TouchableOpacity>
                    </View>

                    <View style={{marginBottom:margin.lg}}>
                        <View style={{
                            ...styles.row,
                            flexDirection: getFlexDirection(this.props.language)
                        }}>
                            <Text style={styles.contentHeading}>{this.props.userlang.upcomming_programmes}</Text>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('program')}>
                                <Text>{this.props.userlang.view_all}</Text>
                            </TouchableOpacity>
                        </View>

                        {/* UPCOMING PROGRAM DATA FROM STATIC JSON  */}
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('program')}
                            style={{ flex: 1 }}>
                            <Image source={{ uri: this.props.up_coming_program.image }}
                                style={{ height: 250, width: '100%', borderRadius: radius.lg }}
                                resizeMode='cover'>
                            </Image>
                            <View style={{
                                backgroundColor: 'rgba(0,0,0,0.5)',
                                ...StyleSheet.absoluteFill,
                                borderRadius: radius.lg,
                                padding: 10,
                                justifyContent: 'flex-end'
                            }}>
                                <>
                                    <Text style={{ ...styles.titleStyle, textAlign: lang == 'ar' ? 'right' : 'left' }}>{
                                        this.props.up_coming_program.lang_programs ? this.props.up_coming_program.lang_programs.heading
                                            :
                                            ''
                                    }</Text>
                                    <Text style={{ ...styles.text, textAlign: lang == 'ar' ? 'right' : 'left' }}>{
                                        this.props.up_coming_program.lang_programs ? this.props.up_coming_program.lang_programs.program_short_desc
                                            :
                                            ''
                                    }</Text>
                                </>
                            </View>
                        </TouchableOpacity>

                    </View>
{/* STORIES  */}

                    <View>
                        <View style={{
                            ...styles.row,
                            flexDirection: getFlexDirection(this.props.language),
                            paddingHorizontal:padding.lg,
                            backgroundColor:colors.green,
                            borderTopLeftRadius:30,
                            borderTopRightRadius:30,
                            marginVertical:margin.xl
                        }}>
                            <Text style={styles.contentHeading}>{this.props.userlang.stories}</Text>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('blog')}>
                                <Text>{this.props.userlang.view_all}</Text>
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            data={this.props.stories}
                            renderItem={({ item }) =>
                                <View>
                                    <AppListItem
                                        title={this.isLanguageEnglish() ? item.title : item.a_title}
                                        subtitle={
                                            <View style={styles.subtitleView}>
                                                <Text style={styles.ratingText}>{item.created_at}</Text>
                                                <Text style={styles.ratingText}>{item.time}</Text>
                                            </View>}
                                        leftAvatar={<Image source={{ uri: item.image }} 
                                        style={{borderRadius:10,height:100,width:100}} />}
                                        onPress={() => this.props.navigation.navigate('blog')}
                                        titleStyle={{ textAlign: getTextAlign(this.props.language) }}
                                        containerStyle={{ flexDirection: getFlexDirection(this.props.language),
                                         }}
                                    />
                                </View>
                            }
                        />
                    </View>



{/* BLOGS  */}

                    <View>
                        <View style={{
                            ...styles.row,
                            flexDirection: getFlexDirection(this.props.language),
                            paddingHorizontal:padding.lg,
                            backgroundColor:colors.green,
                            borderTopLeftRadius:30,
                            borderTopRightRadius:30,
                            marginVertical:margin.xl
                        }}>
                            <Text style={styles.contentHeading}>{this.props.userlang.blog}</Text>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('blog')}>
                                <Text>{this.props.userlang.view_all}</Text>
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            data={this.props.blogs}
                            renderItem={({ item }) =>
                                <View>
                                    <AppListItem
                                        title={this.isLanguageEnglish() ? item.heading : item.a_heading}
                                        subtitle={
                                            <View style={styles.subtitleView}>
                                                <Text style={styles.ratingText}>{item.created_at}</Text>
                                                <Text style={styles.ratingText}>{item.time}</Text>
                                            </View>}
                                        leftAvatar={<Image source={{ uri: item.image }} 
                                        style={{borderRadius:10,height:100,width:100}} />}
                                        onPress={() => this.props.navigation.navigate('blog')}
                                        titleStyle={{ textAlign: getTextAlign(this.props.language) }}
                                        containerStyle={{ flexDirection: getFlexDirection(this.props.language) }}
                                    />
                                </View>
                            }
                        />
                    </View>
                </ScrollView>
            </View>


        );
    }
}

const styles = {
    heading: {
        fontSize: 24,
        fontWeight:'600',
      
    },
    heading2: {
        color: colors.white,
        fontSize: 16,
        fontWeight: 'bold'
    },
    contentHeading: {
        fontSize: fonts.xxl
    },
    iconStyle: {
        height: 70,
        width: 70,
        resizeMode: 'contain',
        marginRight: margin.lg
    },
    cardStyle: {
        backgroundColor: colors.green,
        padding: padding.lg,
        borderRadius: radius.lg,
        marginVertical: margin.lg,
    },
    cardInner: {
        flexDirection: 'row',
        // justifyContent: 'space-between',
        alignItems: 'center'
    },
    textStyle: {
        color: colors.white,
        fontSize: fonts.xl,
    },
    marginAuto: {
        marginLeft: 'auto'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: padding.lg,
        flex: 1
    },
    iconImg: {
        height: 40,
        width: 40,
        resizeMode: 'contain',
        marginBottom: margin.sm,
        alignSelf: 'center',
    },
    cardContainer: {
        margin: 0,
        borderWidth: 0,
        padding: padding.md,
        borderRadius: radius.md,
        backgroundColor:colors.white,
        borderWidth:1,
        borderColor:colors.green,
        flex: 1,
        width: 120,
        height: '100%',
    },
    subtitleView: {
        flexDirection: 'row',
        paddingTop: 5,
        justifyContent: 'flex-start'
    },
    ratingText: {
        color: 'grey',
        marginRight: margin.lg
    },
    rightArrow: {
        marginLeft: 'auto',
        height: 15,
        width: 30,
        resizeMode: 'contain'
    },
    titleStyle: {
        fontSize: fonts.xl,
        fontWeight: 'bold',
        textAlign: 'left',
        color: colors.white,
        marginBottom: margin.md

    },
    text: {
        color: colors.white,
        marginBottom: margin.xl
    },
    mrLeft: {
        marginLeft: 'auto',
    },
    overlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        width: '100%',
        height: 450,
        flex: 1,
        padding: padding.md,
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        backgroundColor: 'rgba(0,0,0,0.5)',
        borderRadius: radius.md
    },
}

const mapStateToProp = state => {
    //console.log(state.userDetail.userlanguage);
    //console.log(state.rootReducer.userLanguage);
    return {
        language: state.userDetail.userlanguage,
        total_donate: state.rootReducer.dashboardresponse.total_donate,
        categories: state.rootReducer.dashboardresponse.categories,
        up_coming_program: state.rootReducer.programlist[0],
        blogs: state.rootReducer.dashboardresponse.blogs,
        username: state.userDetail.username,
        userlang: state.rootReducer.userLanguage,
        program_doate: state.rootReducer.myprogram.total_donate,
        stories: state.rootReducer.storylist,

    }
}

export default connect(mapStateToProp, { setUser, setRootReduser, set_Arabic, set_English })(Dashboard);