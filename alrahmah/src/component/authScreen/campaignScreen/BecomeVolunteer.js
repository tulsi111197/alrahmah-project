import React, { Component } from 'react';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-picker';
import { View, ScrollView, Text, Image, TouchableOpacity, FlatList, TextInput, ActivityIndicator } from 'react-native';
import { baseStyle, margin, fonts, colors, padding, radius } from '../../style';
import { Icon } from 'react-native-elements';
import AppInput from '../../utitlity/AppInput';
import AppButton from '../../utitlity/AppButton';
import { volunteer } from '../../api utility/apiutility';
import { Alert, Modal } from 'react-native';
import DatePicker from '../../utitlity/datePicker';
import { NavigationEvents } from 'react-navigation';
import Counterys from '../../api utility/Countries';
import { isLangArabic } from '../../Constent/helperMethods/langMethods';

class BecomeVolunteer extends Component {
    state = {
        program_id: this.props.programid,
        name: '',
        name_status: '',
        date: true,
        dob: '',
        dob_status: '',

        nationality: '',
        nationality_status: '',

        nationality_id_number: '',
        nationality_id_number_status: '',

        occupation: '',
        occupation_status: '',

        speciality_class: '',
        speciality_class_status: '',

        employer_institute: '',
        employer_institute_status: '',

        highest_qualification: '',
        highest_qualification_status: '',

        mobile: '',
        mobile_status: '',

        city: '',
        city_status: '',

        image: '',
        image_status: '',
        isimageselected: false,
        token: '',
        error: '',

        countery_name: 'India',
        iscodemodel: false,
        filterCounterys: Counterys,
        phone: '',
        country: '',
        iscodemodelIOS: false,
        iscodemodelAndroid: false,

        isloading: false
    }

    componentDidMount = async () => {
        this.checkUserDetail();
        const value = await AsyncStorage.getItem('APITOKEN')
        this.setState({ token: value });
        let nid = await AsyncStorage.getItem('NID');

        if (nid) {
            let dob = await AsyncStorage.getItem('DOB');
            let occ = await AsyncStorage.getItem('OCCUPACTION');
            let spc = await AsyncStorage.getItem('SPCCLAS');
            let emp = await AsyncStorage.getItem('EMPINS');
            let high = await AsyncStorage.getItem('HIGQUI');
            this.setState({
                nationality_id_number: nid,
                occupation: occ,
                speciality_class: spc,
                employer_institute: emp,
                highest_qualification: high,
                dob: dob
            })
        }

    }

    options = {
        title: 'Select Avatar',
        storageOptions: {
            skipBackup: true,
            path: 'images',
        },
    };

    imageBox = () => {
        ImagePicker.showImagePicker(this.options, response => {
            if (response.error) {
            }
            else {
                this.setState({ image: response });
                this.setState({ isimageselected: true });
            }
        })
    }

    validateUserInput = () => {
        if (this.state.name &&
            this.state.email &&
            this.state.mobile &&
            this.state.dob &&
            this.state.employer_institute &&
            this.state.highest_qualification) {
            return true
        }
        return false
    }

    uploadDetail = async () => {

        let response = await fetch(volunteer, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.state.token}`
            },
            body: JSON.stringify({
                program_id: this.props.programid,
                full_name: this.state.name,
                dob: this.state.dob,
                nationality: this.state.countery_name,
                nationality_id_number: this.state.nationality_id_number,
                occupation: this.state.occupation,
                speciality_class: this.state.speciality_class,
                employer_institute: this.state.employer_institute,
                highest_qualification: this.state.highest_qualification,
                mobile: this.state.mobile,
                email: this.props.email,
                city: this.state.city,
            })
        })
        let result = await response.json();
        this.setState({ isloading: false });


        if (result.success) {
            await AsyncStorage.setItem('DOB', this.state.dob);
            await AsyncStorage.setItem('NID', this.state.nationality_id_number);
            await AsyncStorage.setItem('OCCUPACTION', this.state.occupation);
            await AsyncStorage.setItem('SPCCLAS', this.state.speciality_class);
            await AsyncStorage.setItem('EMPINS', this.state.employer_institute);
            await AsyncStorage.setItem('HIGQUI', this.state.highest_qualification);
            this.props.navigation.navigate('dashboard');
        }
        else {

            this.props.navigation.goBack();
            this.showAlert();
        }
    }

    showAlert = () => {
        Alert.alert(
            'Information',
            'You Are Already Volunteer '
        )
    }

    validate() {
        if (this.state.name === '') {
            this.setState({ name_status: false })
        }
        if (this.state.dob === '') {
            this.setState({ dob_status: false })
        }
        if (this.state.countery_name === '') {
            this.setState({ nationality_status: false })
        }
        if (this.state.nationality_id_number === '') {
            this.setState({ nationality_id_number_status: false })
        }
        if (this.state.highest_qualification === '') {
            this.setState({ highest_qualification_status: false })
        }
        if (this.state.mobile === '') {
            this.setState({ mobile_status: false })
        }
        if (this.state.city === '') {
            this.setState({ city_status: false })
        }
        if (this.state.occupation === '') {
            this.setState({ occupation_status: false })
        }
        if (this.state.employer_institute === '') {
            this.setState({ employer_institute_status: false })
        }
        if (this.state.image === '') {
            this.setState({ image_status: false })
        }
        if (this.state.city === '') {
            this.setState({ city_status: false })
        }
        if (this.state.speciality_class === '') {
            this.setState({ speciality_class_status: false })
        }

        else {
            this.setState({ isloading: true });
            this.uploadDetail();
        }
    }

    datePicker() {
        if (this.state.date === true) {
            return (
                <DatePicker
                    style={{ width: 200 }}
                    date={this.state.dob}
                    mode="date"
                    placeholder="Select Date Of Birth"
                    format="DD-MM-YYYY"
                    minDate="31-12-1980"
                    maxDate="31-12-2020"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    hideText={true}
                    showIcon={false}
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 4,
                            marginLeft: 0
                        },
                        dateInput: {
                            marginLeft: 36
                        }
                        // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => { this.setState({ dob: date }) }}
                />
            )
        }
    }


    findWordForEach = (toSearch, item) => {
        if (toSearch.trim().toLowerCase() >= 'a' && toSearch.trim().toLowerCase() <= 'z') {
            let response = item.name.toLowerCase().search(toSearch.toLowerCase());
            if (response >= 0)
                return item
        }
        else {
            let response = item.dial_code.replace(/[^0-9]/g, '').search(toSearch.replace(/[^0-9]/g, ''));
            if (response >= 0)
                return item
        }


    }
    searchCode = (text) => {
        if (text == '') {
            this.setState({ filterCounterys: Counterys })
        }
        else {
            let result = Counterys.filter(item => this.findWordForEach(text, item));
            this.setState({ filterCounterys: result });
        }

    }
    renderCounteryListIOS = () => {
        if (this.state.iscodemodelIOS) {
            return (
                <Modal
                    visible={this.state.iscodemodelIOS}
                    transparent={true}
                >
                    <View style={{
                        backgroundColor: 'rgba(237,228,228,0)',
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'flex-end',
                        alignItems: 'center'

                    }} >

                        <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, paddingHorizontal: padding.lg }}>

                            <AppInput
                                placeholder={this.props.userlang.search_by_code}
                                label={this.props.userlang.search}
                                containerStyle={{ width: '90%' }}
                                onChangeText={(text) => this.searchCode(text)}
                                lang={this.props.language}
                            />
                            <Icon name='close' iconStyle={{ backgroundColor: 'rgba(0,0,0,0.3)', padding: 8, color: 'white', }} onPress={() => this.setState({ iscodemodelIOS: false, fromcountery: false })} />

                        </View>

                        <View style={{
                            width: '100%',
                            height: 600,
                            alignSelf: 'flex-end',
                            borderRadius: radius.xl,
                            paddingHorizontal: padding.lg,
                            backgroundColor: colors.white
                        }}>
                            <FlatList
                                data={this.state.filterCounterys}
                                renderItem={({ item }) =>
                                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', marginBottom: margin.md }}>
                                        <TouchableOpacity style={{
                                            borderWidth: 1,
                                            padding: padding.lg,
                                            borderRadius: radius.lg,
                                            width: '100%',
                                            backgroundColor: colors.offBlack
                                        }}
                                            onPress={() => this.setState({
                                                iscodemodelIOS: false, selectedcode: item.dial_code,
                                                countery_name: item.name
                                            })}
                                        >
                                            <Text
                                                style={{
                                                    color: colors.textColor
                                                }} >
                                                {
                                                    this.state.fromcountery ?
                                                        `${item.flag}   ${item.name} ` :

                                                        `${item.flag}   ${item.dial_code} ${item.name} `}</Text>
                                        </TouchableOpacity>
                                    </View>
                                }
                            />
                        </View>


                    </View>
                </Modal>
            )
        }
        else
            return null
    }
    renderCounteryListAndroid = () => {
        if (this.state.iscodemodelAndroid) {
            return (
                <Modal
                    visible={this.state.iscodemodelAndroid}
                    transparent={true}
                >
                    <View style={{
                        backgroundColor: 'rgba(237,228,228,0)',
                        flex: 1,
                        flexDirection: 'column',
                        //justifyContent: 'flex-end',
                        alignItems: 'center'

                    }} >

                        <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, paddingHorizontal: padding.lg }}>

                            <AppInput
                                placeholder={this.props.userlang.search_by_code}
                                label={this.props.userlang.search}
                                containerStyle={{ width: '90%' }}
                                onChangeText={(text) => this.searchCode(text)}
                                lang={this.props.language}
                            />
                            <Icon name='close' iconStyle={{ backgroundColor: 'rgba(0,0,0,0.3)', padding: 8, color: 'white', }} onPress={() => this.setState({ iscodemodelAndroid: false, fromcountery: false })} />

                        </View>

                        <View style={{
                            width: '100%',
                            height: 600,
                            alignSelf: 'flex-end',
                            borderRadius: radius.xl,
                            paddingHorizontal: padding.lg,
                            backgroundColor: colors.white
                        }}>
                            <FlatList
                                data={this.state.filterCounterys}
                                renderItem={({ item }) =>
                                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', marginBottom: margin.md }}>
                                        <TouchableOpacity style={{
                                            borderWidth: 1,
                                            padding: padding.lg,
                                            borderRadius: radius.lg,
                                            width: '100%',
                                            backgroundColor: colors.offBlack
                                        }}
                                            onPress={() => this.setState({
                                                iscodemodelAndroid: false, selectedcode: item.dial_code,
                                                countery_name: item.name
                                            })}
                                        >
                                            <Text
                                                style={{
                                                    color: colors.textColor
                                                }} >
                                                {
                                                    this.state.fromcountery ?
                                                        `${item.flag} ${item.name} ` :

                                                        `${item.flag} ${item.dial_code} ${item.name} `}</Text>
                                        </TouchableOpacity>
                                    </View>
                                }
                            />
                        </View>


                    </View>
                </Modal>
            )
        }
        else
            return null
    }


    checkUserDetail = () => {
        let fullname = this.props.fullname;
        if (fullname.indexOf('null') != -1) {
            Alert.alert(
                'Account Detail',
                'Please Complete Your Account Details',
                [
                    {
                        text: 'OK', onPress: () =>
                            this.props.navigation.navigate('editProfile', { from: 'program' })
                    },
                ],
                { cancelable: false }
            )
        }
        else {
            this.setState({
                name: this.props.fullname,
                city: this.props.city,
                mobile: this.props.mobile,
            });
        }


    }

    render() {
        const lang = this.props.language;
        return (
            <View style={[
                baseStyle.appContainer,
                //baseStyle.paddingLg
            ]}>
                <NavigationEvents onDidFocus={() => this.checkUserDetail()} />
                <this.renderCounteryListAndroid />
                <this.renderCounteryListIOS />
                <ScrollView>
                    <View style={{ flex: 1, ...baseStyle.paddingLg }}>
                        <Text style={{ ...styles.heading, textAlign: lang == 'ar' ? 'right' : 'left' }}>{this.props.userlang.case_detail}</Text>

                        <AppInput
                            label={this.props.userlang.full_name}
                            editable={false}
                            value={this.state.name}
                            inputContainerStyle={{ marginBottom: 0 }}
                            onChangeText={(name) => {
                                this.setState({ name: name, name_status: true })
                            }}
                            placeholder={this.props.userlang.full_name}
                            errorMessage={this.state.name_status === false ? 'Please Enter Full Name' : null}
                            lang={this.props.language}
                        />

                        <AppInput
                            label={this.props.userlang.email}
                            value={this.props.email}
                            inputContainerStyle={{ marginBottom: 0 }}
                            editable={false}
                            placeholder={this.props.userlang.email}
                            lang={this.props.language}
                        />

                        <AppInput
                            label={this.props.userlang.mobile_no}
                            keyboardType='numeric'
                            inputContainerStyle={{ marginBottom: 0 }}
                            value={this.state.mobile}
                            onChangeText={(mobile) => this.setState({ mobile })}
                            editable={false}
                            placeholder={this.props.userlang.mobile_no}
                            errorMessage={this.state.mobile_status === false ? 'Please Enter Mobile Number' : null}
                            lang={this.props.language}
                        />

                        <AppInput
                            label={this.props.userlang.city}
                            value={this.state.city}
                            inputContainerStyle={{ marginBottom: 0 }}
                            onChangeText={(city) => this.setState({ city: city, city_status: true })}
                            placeholder={this.props.userlang.city}
                            errorMessage={this.state.city_status === false ? 'Please Enter City' : null}
                            lang={this.props.language}
                        />

                        <Text style={{
                            fontSize: 16,
                            margin: 5,
                            textAlign: lang == 'ar' ? 'right' : 'left'
                        }}>{this.props.userlang.nationality}
                        </Text>
                        {
                            Platform.OS === 'android'
                                ?
                                <TouchableOpacity onPress={() => {
                                    this.setState({
                                        iscodemodelAndroid: true,
                                        fromcountery: true
                                    })
                                }}
                                    style={{
                                        paddingVertical: 13,
                                        paddingHorizontal: padding.lg,
                                        backgroundColor: colors.inputColor,
                                        alignItems: this.props.language == 'ar' ? 'flex-end' : 'flex-start'
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: colors.black,
                                            fontSize: 16,
                                            textAlign: this.props.language == 'ar' ? 'right' : 'left'
                                        }}
                                    >
                                        {
                                            this.state.countery_name
                                        }
                                    </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => {
                                    this.setState({
                                        iscodemodelIOS: true,
                                        fromcountery: true
                                    })
                                }}
                                    style={{
                                        paddingVertical: 13,
                                        paddingHorizontal: padding.lg,
                                        backgroundColor: colors.inputColor,
                                        alignItems: this.props.language == 'ar' ? 'flex-end' : 'flex-start'
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: colors.black,
                                            fontSize: 16,
                                            textAlign: this.props.language == 'ar' ? 'right' : 'left'
                                        }}
                                    >
                                        {
                                            this.state.countery_name
                                        }
                                    </Text>
                                </TouchableOpacity>
                        }


                        <View style={{ marginTop: 10 }}>
                            <DatePicker
                                label={this.props.userlang.dob}
                                placeholder={this.props.userlang.select_date_of_birth}
                                date={this.state.dob}
                                mindate="31-12-1980"
                                maxdate="31-12-2020"
                                onDateChange={(date) => this.setState({ dob: date })}
                                lang={this.props.language}
                            />
                            <Text
                                style={{
                                    fontSize: 12,
                                    color: 'red',
                                    textAlign: this.props.language == 'ar' ? "right" : 'left'
                                }}>
                                {this.state.dob_status === false ? this.props.language == 'en' ? 'Please Enter Date' : 'الرجاء إدخال التاريخ' : null}
                            </Text>
                        </View>
                        <AppInput
                            label={this.props.userlang.nationality_id}
                            value={this.state.nationality_id_number}
                            inputContainerStyle={{ marginBottom: 0 }}
                            keyboardType='numeric'
                            onChangeText={(nationality_id_number) => {
                                this.setState({ nationality_id_number: nationality_id_number, nationality_id_number_status: true })
                            }}
                            placeholder={this.props.userlang.nationality_id}
                            lang={this.props.language}
                            errorMessage={this.state.nationality_id_number_status === false ? this.props.language == 'en' ? 'Please Enter Nationality Id Number' : 'الرجاء إدخال رقم الهوية الوطنية' : null}
                        />
                        <AppInput
                            label={this.props.userlang.occupation}
                            value={this.state.occupation}
                            inputContainerStyle={{ marginBottom: 0 }}
                            onChangeText={(occupation) => this.setState({ occupation: occupation, occupation_status: true })}
                            placeholder={this.props.userlang.occupation}
                            lang={this.props.language}
                            errorMessage={this.state.occupation_status === false ? this.props.language == 'en' ? 'Please Enter Occupation' : 'الرجاء إدخال المهنة' : null}
                        />
                        <AppInput
                            label={this.props.userlang.case_of_study}
                            value={this.state.speciality_class}
                            inputContainerStyle={{ marginBottom: 0 }}
                            onChangeText={(speciality_class) => this.setState({ speciality_class: speciality_class, speciality_class_status: true })}
                            placeholder={this.props.userlang.case_of_study}
                            lang={this.props.language}
                            errorMessage={this.state.speciality_class_status === false ? this.props.language == 'en' ? 'Please Enter Speciality Class of Study' : 'الرجاء إدخال فئة الدراسة التخصصية' : null}
                        />
                        <AppInput
                            label={this.props.userlang.emp_ins}
                            value={this.state.employer_institute}
                            inputContainerStyle={{ marginBottom: 0 }}
                            onChangeText={(employer_institute) => this.setState({
                                employer_institute: employer_institute,
                                employer_institute_status: true
                            })}
                            placeholder={this.props.userlang.emp_ins}
                            lang={this.props.language}
                            errorMessage={this.state.employer_institute_status === false ? this.props.language == 'en' ? 'Please Enter Employer Institute' : 'الرجاء إدخال معهد صاحب العمل' : null}
                        />
                        <AppInput
                            label={this.props.userlang.high_que}
                            value={this.state.highest_qualification}
                            inputContainerStyle={{ marginBottom: 0 }}
                            onChangeText={(highest_qualification) => this.setState({
                                highest_qualification: highest_qualification,
                                highest_qualification_status: true
                            })}
                            placeholder={this.props.userlang.high_que}
                            lang={this.props.language}
                            errorMessage={this.state.highest_qualification_status === false ? this.props.language == 'en' ? 'Please Enter Highest Qualification' : 'الرجاء إدخال أعلى مؤهل' : null}
                        />



                        <Text style={{ ...styles.heading, textAlign: lang == 'ar' ? 'right' : 'left' }} >{this.props.userlang.upload_image}</Text>
                        <View style={{
                            alignItems: isLangArabic(this.props.language) ? 'flex-end' : 'flex-start'
                        }}>
                            <TouchableOpacity style={styles.uploadWrapper} onPress={() => this.imageBox()} >
                                <Image source={this.state.isimageselected ? { uri: this.state.image.uri } : require('../../assets/camera.png')} style={styles.imgStyle} />
                                <Text>{this.props.userlang.upload_doc}</Text>
                            </TouchableOpacity>
                            <Text>{this.state.error}</Text>
                        </View>
                        {
                            this.state.isloading ?
                                <ActivityIndicator size="large" color="#4ABEA1" />
                                :
                                <AppButton
                                    title={this.props.userlang.send}
                                    onPress={() => {
                                        this.validate();
                                    }}
                                />
                        }
                    </View>
                </ScrollView>
            </View >
        );
    }
}


const styles = {
    heading: {
        fontSize: fonts.xl,
        fontWeight: 'bold',
        marginVertical: margin.xl
    },
    imgStyle: {
        height: 50,
        width: 50,
        resizeMode: 'contain'
    },
    uploadWrapper: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        width: '50%',
        borderWidth: 1,
        borderColor: colors.green,
        minHeight: 180,
        maxHeight: 200,
        marginBottom: margin.xl
    }
}

const mapStatToProp = state => {
    let user = state.userDetail;
    console.log(state.userDetail.userlanguage)
    return {
        programid: state.programDetails.programDetail.id,
        email: user.email,
        fullname: user.f_name + ' ' + user.l_name,
        mobile: user.mobile,
        city: user.city,
        userlang: state.rootReducer.userLanguage,
        language: state.userDetail.userlanguage,
    }
}

export default connect(mapStatToProp, null)(BecomeVolunteer);