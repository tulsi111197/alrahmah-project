import React, { Component } from 'react';
import { View, ScrollView, FlatList, Image, TextInput, Modal, TouchableWithoutFeedback, Button } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Text, Card, Avatar } from 'react-native-elements';
import { baseStyle, padding, colors, fonts, margin, radius } from '../../style';
import AppTile from '../../utitlity/AppTile';
import AppButton from '../../utitlity/AppButton';
import AppSlider from '../../utitlity/AppSlider';
import AppListItem from '../../utitlity/AppListItem';

class CampaignDetail extends Component {
    state = {
        selectedAmount: 10,
        imageuri: '',
        isopen: false,
        donateamount: '',
        topdoerslist: [],
    }
    componentDidMount() {
        console.log(this.props.campaignDetail);
    }
    calculatePercentages = (total_donate, campaing_amount) => {
        let percentages = (total_donate / campaing_amount) * 100
        return (parseInt(percentages));
    }

    calculateDay = (start, end) => {
        if (end) {
            let d = new Date();
            const year = end.slice(0, 4) - d.getFullYear();
            const month = end.slice(6, 7) - d.getMonth();
            const day = end.slice(9, 10) - d.getDate();
            let rem = (day + (month * 30) + (year * 365));
            if (rem < 0)
                return 0
            return rem
        }
        return 0

    }
    renderTextInputBox = () => {
        if (this.state.selectedAmount == 'other') {
            return (
                <View style={{ marginBottom: 8 }}>
                    <TextInput
                        placeholder={this.props.userlang.amount}
                        value={this.state.donateamount}
                        onChangeText={(text) => this.setState({ donateamount: text })}
                        keyboardType="numeric"
                        maxLength={7}
                        style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                    />
                </View>
            )
        }
        return null
    }

    quick_Sort = (origArray) => {
        if (origArray.length <= 1) {
            return origArray;
        } else {

            var left = [];
            var right = [];
            var newArray = [];
            var pivot = origArray.pop();
            var length = origArray.length;

            for (var i = 0; i < length; i++) {
                this.count = this.count + 1
                if (origArray[i].donate_amount >= pivot.donate_amount) {
                    left.push(origArray[i]);
                } else {
                    right.push(origArray[i]);
                }
            }

            return newArray.concat(this.quick_Sort(left), pivot, this.quick_Sort(right));
        }
    }

    sortTopDonors = (resp) => {
        this.count = 0
        let res = this.quick_Sort(resp);

        if (res.length > 20) {
            return (res.splice(0, 20))
        }
        return res
    }

    renderModel = () => {
        if (this.state.isopen) {
            return (
                <View>
                    <Modal
                        animationType={"fade"}
                        transparent={false}
                        visible={this.state.isopen}>
                        <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='close' onPress={() => this.setState({ isopen: false })} style={{ color: 'white' }} size={30} />
                            <Image source={{ uri: this.state.imageuri }} style={{ width: 400, height: 400, resizeMode: 'contain', marginBottom: 50, marginTop: 20 }} />
                        </View>

                    </Modal>
                </View>
            );
        }
        return null
    }

    collectimage = (item) => {
        if (item.user_image)
            return item.user_image.indexOf('http') == -1 ?
                'hello'
                :
                item.user_image
        return 'gello'
    }

    render() {
        if (this.props.campaignDetail.lang_campaing == null) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                    <Text>{this.props.userlang.no_data}</Text>
                </View>
            )
        }
        return (
            <View style={baseStyle.appContainer} >
                <ScrollView>
                    <View style={[baseStyle.marginBottomXl, styles.position]}>
                        {/* Campaing Detail and Image goes here */}
                        <AppTile
                            source={{ uri: this.props.campaignDetail.campaing_cover_image }}
                            height={350}
                            containerStyle={{ borderRadius: 0, marginBottom: 30 }}
                            overlayStyle={{ borderRadius: 0 }}
                        />
                    </View>


                    <Card containerStyle={{ borderColor: 'transparent', borderWidth: 0, shadowOpacity: 0 }}>
                        <Text style={styles.heading}>
                            {
                                this.props.campaignDetail.lang_campaing.campaing_name
                            }
                        </Text>
                        <Text style={styles.textStyle}>
                            {
                                this.props.campaignDetail.lang_campaing.campaing_short_desc
                            }
                        </Text>
                    </Card>

                    {/* Fund Detail Goes here */}
                    <Card containerStyle={styles.boxShadow}>
                        <Text style={styles.heading}>{this.props.userlang.found_need}</Text>
                        <View style={baseStyle.marginBottomLg}>
                            <View style={[baseStyle.flexRow, baseStyle.justifyContentBetween, baseStyle.alignItemsCenter]}>
                                <Text style={styles.heading}>{this.props.userlang.found}</Text>
                                <Text style={styles.heading}>
                                    {
                                        `${this.props.userlang.sar} ${
                                        this.props.campaignDetail.total_donate ?
                                            this.props.campaignDetail.total_donate :
                                            0
                                        }`
                                    }
                                </Text>
                            </View>

                            <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center' }}>
                                <AppSlider
                                    value={this.props.campaignDetail.total_donate}
                                    maximumValue={this.props.campaignDetail.campaing_amount}
                                    minimumValue={0}
                                />
                                <Text style={styles.textStyle}>
                                    {
                                        `${this.props.userlang.sar} ${
                                        this.props.campaignDetail.total_donate
                                        ?
                                        this.props.campaignDetail.total_donate
                                        :
                                        0
                                        }`
                                    }{this.props.userlang.rase_of}
                                    {
                                        ` ${this.props.userlang.sar} ${this.props.campaignDetail.campaing_amount}`
                                    }
                                </Text>
                            </View>
                        </View>

                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around' }}>
                            {/* <View style={{ flex: 1, alignItems: 'center' }}>
                                <Text style={styles.subHeading}>{this.props.userlang.top_donor}</Text>
                                <Text style={{ fontSize: 30 }} >{this.props.campaignDetail.donate_user.length}</Text>
                            </View> */}

                            <View style={{ flex: 1, alignItems: 'center' }}>

                                <Text style={styles.subHeading}>{this.props.userlang.day_remaning}</Text>
                                <Text style={{ fontSize: 30 }} >{this.calculateDay(this.props.campaignDetail.campaign_start_date, this.props.campaignDetail.campaign_end_date)}</Text>
                            </View>
                        </View>
                    </Card>


                    {/* detail goes here */}

                    <View style={styles.cardBorder}>
                        <Text style={styles.heading}>{this.props.userlang.hospital_info}</Text>
                        <Text style={styles.textStyle}>
                            {
                                this.props.campaignDetail.lang_campaing.hospital_name
                            }
                        </Text>
                        <Text style={styles.textStyle}>
                            {
                                this.props.campaignDetail.lang_campaing.hospital_desc
                            }
                        </Text>
                    </View>

                    {/* case detail goes here */}
                    <View style={styles.cardBorder}>
                        <Text style={styles.heading}>{this.props.userlang.case_detail}</Text>
                        <Text style={styles.textStyle}>
                            {
                                this.props.campaignDetail.lang_campaing.campaing_case_desc
                            }
                        </Text>
                    </View>
                    {/* document Details */}

                    <View style={styles.cardBorder}>
                        <Text style={styles.heading}>{this.props.userlang.documents}</Text>
                        <FlatList
                            data={this.props.campaignDetail.campaing_doc}
                            horizontal={true}
                            renderItem={({ item, index }) => (
                                <View style={{
                                    borderColor: colors.green,
                                    borderWidth: 1,
                                    borderRadius: radius.sm,
                                    margin: margin.sm,
                                    height: 180,
                                    width: 140,

                                }}>
                                    <TouchableWithoutFeedback
                                        onPress={() => this.setState({ isopen: true, imageuri: item.campaign_doc_url })} >
                                        < Image source={{ uri: item.campaign_doc_url }} style={styles.docStyle} />
                                    </TouchableWithoutFeedback>
                                </View>
                            )}
                        />
                        {this.renderModel()}
                    </View>
                    {/* patient detail */}
                    <View style={styles.cardBorder} >
                        <Text style={styles.heading}>{this.props.userlang.patient_detail}</Text>

                        <AppListItem
                            leftAvatar={<Avatar rounded size="large" source={{ uri: this.props.campaignDetail.lang_campaing.patient_image }} />}
                            title={this.props.campaignDetail.lang_campaing.patient_name}
                            titleStyle={styles.heading}
                            subtitle={<View>
                                <Text style={styles.textStyle}>{this.props.userlang.age} - {this.props.campaignDetail.lang_campaing.patient_age}yr</Text>
                                <Text style={styles.textStyle}>{this.props.userlang.guardian_name} - {this.props.campaignDetail.lang_campaing.patient_guardian}</Text>
                            </View>}
                            subTitleStyle={styles.subTitle}
                        />
                        <Text style={styles.textStyle}>
                            {
                                this.props.campaignDetail.lang_campaing.patient_desc
                            }
                        </Text>
                    </View>
                    {/* Top donors */}
                    {/* <View style={styles.cardBorder} >
                        <Text style={styles.heading}>{this.props.userlang.top_donor}</Text>
                        <FlatList
                            data={this.sortTopDonors(this.props.campaignDetail.donate_user)}
                            horizontal={true}
                            renderItem={({ item }) =>
                                <AppListItem
                                    leftAvatar={<Avatar rounded size="large" source={{
                                        uri: this.collectimage(item)
                                    }} />}
                                    title={item.username}
                                    titleStyle={[styles.heading, baseStyle.textAlignCenter]}
                                    subtitle={`${this.props.userlang.sar} ${item.donate_amount}`}
                                    subTitleStyle={styles.subTitle}
                                    containerStyle={{ flexDirection: 'column', alignItems: 'center', justifyContent: 'center', flex: 1 }}
                                />
                            }
                        />
                    </View> */}

                    <View style={styles.cardBorder}>
                        {/* <Text style={styles.heading}>{this.props.userlang.select_amount}</Text> */}
                        <View style={styles.row}>
                            {/* <AppButton
                                title={`${this.props.userlang.sar}10 `}
                                buttonStyle={styles.btnStyle}
                                titleStyle={this.state.selectedAmount == 10 ? { color: colors.green } : { color: colors.black }}
                                containerStyle={this.state.selectedAmount == 10 ? [styles.btnContainer, styles.activeBtn] : styles.btnContainer}
                                onPress={() => this.setState({ selectedAmount: 10 })}
                            />
                            <AppButton
                                title={`${this.props.userlang.sar}50 `}
                                buttonStyle={styles.btnStyle}
                                titleStyle={this.state.selectedAmount == 50 ? { color: colors.green } : { color: colors.black }}
                                containerStyle={this.state.selectedAmount == 50 ? [styles.btnContainer, styles.activeBtn] : styles.btnContainer}
                                onPress={() => this.setState({ selectedAmount: 50 })}
                            />
                            <AppButton
                                title={`${this.props.userlang.sar}80 `}
                                buttonStyle={styles.btnStyle}
                                titleStyle={this.state.selectedAmount == 80 ? { color: colors.green } : { color: colors.black }}
                                containerStyle={this.state.selectedAmount == 80 ? [styles.btnContainer, styles.activeBtn] : styles.btnContainer}
                                onPress={() => this.setState({ selectedAmount: 80 })}
                            />
                            <AppButton
                                title={this.props.userlang.other}
                                buttonStyle={styles.btnStyle}
                                titleStyle={this.state.selectedAmount == 'other' ? { color: colors.green } : { color: colors.black }}
                                containerStyle={styles.btnContainer}
                                onPress={() => this.setState({ selectedAmount: 'other' })}
                            /> */}
                        </View>
                        {/* < this.renderTextInputBox /> */}

                        <AppButton
                            title={this.props.userlang.donate_now}
                            buttonStyle={{alignSelf:'flex-end'}}
                            onPress={() =>
                                this.props.navigation.navigate('donate',
                                    {
                                        from: 'campaign',
                                        campaign_id: this.props.campaignDetail.lang_campaing.campaign_id,
                                        campaign_name: this.props.campaignDetail.lang_campaing.campaing_name,
                                        amount: this.state.selectedAmount,
                                        donateamount: this.state.donateamount

                                    })} />
                                   
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = {
    backImg: {
        width: 20,
        height: 15,
        resizeMode: 'contain',
        marginLeft: padding.lg
    },
    heading: {
        fontSize: fonts.xl,
        fontWeight: 'bold',
        marginVertical: margin.lg
    },
    bulletStyle: {
        colors: colors.green
    },
    textStyle: {
        flex: 1,
        fontSize: fonts.xl,
    },
    text: {
        fontSize: fonts.lg,
        marginBottom: margin.md
    },
    circularWrapper: {
        position: 'absolute',
        bottom: 0,
        right: 20,
        left: 'auto'
    },
    boxShadow: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.1,
        shadowRadius: 2,
        elevation: 2,
        borderColor: 'transparent'
    },
    cardBorder: {
        borderBottomWidth: 1,
        borderBottomColor: colors.inputColor,
        padding: padding.lg
    },
    docStyle: {
        flex: 1,
        resizeMode: 'cover',
        borderRadius: radius.sm
    },
    subTitle: {
        color: colors.textMuted,
        textAlign: 'center',
        alignSelf: 'center',
        flex: 1,
    },
    btnStyle: {
        backgroundColor: colors.white,
        alignSelf: 'center',
    },
    btnContainer: {
        borderColor: colors.btnBorder,
        borderWidth: 1,
        paddingHorizontal: padding.sm,
        borderRadius: radius.sm,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        justifyContent: 'space-between',
        marginVertical: margin.lg
    },
    activeBtn: {
        borderColor: colors.green,
    },
    subHeading: {
        fontSize: fonts.lg,
        fontWeight: "bold",
        textAlign: 'center',
        marginTop: margin.lg
    }
}

const mapStateToProps = state => {
    return {
        campaignDetail: state.campaingDetails.campaignDetail[0],
        userlang: state.rootReducer.userLanguage
    }
}

export default connect(mapStateToProps, null)(CampaignDetail);
