import React, { Component } from 'react';
import { View, ScrollView, Text, Image, TouchableOpacity } from 'react-native';
import { baseStyle, margin, fonts, colors,padding } from '../../style';
import AppInput from '../../utitlity/AppInput';
import AppButton from '../../utitlity/AppButton';

export default class CreateNewCampaign extends Component {
    render() {
        return (
            <View style={[baseStyle.appContainer, baseStyle.paddingLg]}>
                <ScrollView>
                    <Text style={styles.heading}>Case Detail</Text>
                    <AppInput label="Choose Donation Program" placeholder="Program..." />
                    <AppInput label="Case Name" placeholder="Case Name" />
                    <AppInput label="Case Description" placeholder="Description" />
                    <Text style={styles.subHeading}>Upload Document</Text>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TouchableOpacity style={styles.uploadWrapper}>
                            <Image source={require('../../assets/camera.png')} style={styles.imgStyle} />
                            <Text>Upload documents</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.uploadWrapper}>
                            <Image source={require('../../assets/camera.png')} style={styles.imgStyle} />
                            <Text>Upload documents</Text>
                        </TouchableOpacity>
                    </View>



                    <Text style={styles.heading}>Patient Detail</Text>
                    <AppInput label="Patient Name" placeholder="John" />
                    <AppInput label="Guardian Name" placeholder="Abdul" />
                    <AppInput label="City" placeholder="Saudi Arabia" />
                    <AppInput label="Patient Age" placeholder="MM/YYYY" />
                    <AppInput label="Address" placeholder="Your Address" />
                    <AppInput label="Mobile No." placeholder="Your Mobile No." />
                    <AppInput label="Case Description" placeholder="Description" />

                    <Text style={styles.subHeading}>Upload Document</Text>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TouchableOpacity style={styles.uploadWrapper}>
                            <Image source={require('../../assets/camera.png')} style={styles.imgStyle} />
                            <Text>Upload documents</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.uploadWrapper}>
                            <Image source={require('../../assets/camera.png')} style={styles.imgStyle} />
                            <Text>Upload documents</Text>
                        </TouchableOpacity>
                    </View>



                    <Text style={styles.heading}>Hospital Information</Text>
                    <AppInput label="Hospital Name" placeholder="BA Hospital" />
                    <AppInput label="City" placeholder="Saudi Arabia" />
                    <AppInput label="Address" placeholder="Your Address" />
                    <AppInput label="Mobile No." placeholder="Your Mobile No." />
                    <AppInput label="Description" placeholder="Write description" />

                    <Text style={styles.subHeading}>Upload Document</Text>
                        <TouchableOpacity style={styles.uploadWrapper}>
                            <Image source={require('../../assets/camera.png')} style={styles.imgStyle} />
                            <Text style={[baseStyle.textAlignCenter,baseStyle.alignSelfCenter]}>Upload Hospital documents</Text>
                        </TouchableOpacity>

                    <AppButton
                        title="Send"
                        onPress={() => this.props.navigation.navigate('dashboard')}
                    />
                </ScrollView>
            </View>
        );
    }
}

const styles = {
    heading: {
        fontSize: fonts.xl,
        fontWeight: 'bold',
        marginVertical: margin.xl
    },
    imgStyle: {
        height: 50,
        width: 50,
        resizeMode: 'contain'
    },
    uploadWrapper: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        width: '45%',
        borderWidth: 1,
        borderColor: colors.green,
       height:150,
        margin: margin.md,
        padding:padding.sm,
        marginBottom: margin.xl
    },
    subHeading:{
        fontSize:fonts.xl
    }
}