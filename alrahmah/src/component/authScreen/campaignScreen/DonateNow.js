import React, { Component } from 'react';
import { View, ScrollView, Text, TextInput, Alert, Image } from 'react-native';
import { baseStyle, margin, fonts, colors, padding, radius, compHeight } from '../../style';
import AppInput from '../../utitlity/AppInput';
import AppButton from '../../utitlity/AppButton';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { campadonation } from '../../api utility/apiutility';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import { bank_arabic } from '../../bank/bank_arabic';
import { bank_english } from '../../bank/bank_english'
import { setUser } from '../../store/reducer/action/userAction';
import campaignDonation from '../../api unitlity function/campaigndonation';
import programDonation from '../../api unitlity function/programdonate';
import { TouchableOpacity } from 'react-native-gesture-handler';
import WebView from 'react-native-webview';
import { postRequest } from '../../api unitlity function/request/post';
import { ActivityIndicator } from 'react-native';
import { Modal } from 'react-native';
import { SafeAreaView } from 'react-native';

// for Payment
export const port = 'thetoysboutique';
export const success = 'success';
export const error = 'error';

class DonateNow extends Component {

    state = {
        id: '',
        name: this.props.f_name + " " + this.props.l_name,
        amount: '',
        error: '',
        todonate: '',
        amount: '',
        selectedAmount: 10,
        transectionid: 'we23',
        paymentmodalvisible: false,
        isLoading: false,
        activity: false,
        payment_url: ''
    }
    webViewRef = React.createRef();
    canGoBack = false

    createPaymentUrl = async (callBack) => {
        const body = {
            "api-key": "d608a83f-c511-4cd9-973d-04bc9cb2816a",
            "vendor-id": 4562,
            "branch-id": 6433,
            "terminal-id": 7511,
            "notification-mode": 300,
            "success-url": "http://104.248.3.101/toysboutique/public/success/",
            "error-url": "http://104.248.3.101/toysboutique/public/error/",
            "expiryDate": 1440,
            "msisdn": "97333333333",
            "email": 'guest@gmail.com',
            "customer-name": this.state.name ? this.state.name : "Guest User",
            "description": "Alrahmah",
            "remarks": "Good",
            "date": "2017-08-17T08:01:05.87Z",
            "external-reference": "123ABC",
            "amount": this.state.amount
        }
        this.setState({ isLoading: true });
        const payurl = await postRequest({
            url: 'https://eps-net.sadadbh.com/api/v2/web-ven-sdd/epayment/create/',
            body: {
                ...body,
            }
        });

        this.setState({ isLoading: false }, () => callBack && callBack(
            (payurl && payurl["payment-url"]) ? payurl["payment-url"] : false
        ));
    }

    checkStatus = (url) => {
        const arr = url.split('/');
        if (arr.indexOf(success) > 0) {
            return success
        }
        else if (arr.indexOf(error) > 0) {
            return error
        }
        return undefined
    }


    onNavigationStateChange = ({ url, canGoBack }) => {

        this.canGoBack = canGoBack;
        const response = this.checkStatus(url);
        if (this.state.paymentmodalvisible) {
            if (response === success) {
                this.setState({
                    paymentmodalvisible: false,
                    payment_url: ''
                })
                setTimeout(() => {
                    alert('Payment Completed');
                    this.props.navigation.goBack();
                }, 1000);
            }
            else if (response === error) {
                this.setState({
                    paymentmodalvisible: false,
                    payment_url: ''
                })
                setTimeout(() => {
                    alert('Transaction Failed')
                }, 1000);
            }
        }
    }

    onPaymetGoBack = () => {
        if (this.state.paymentmodalvisible) {
            if (this.canGoBack) {
                this.webViewRef.current.goBack();
            }
            else {
                Alert.alert(
                    "Cancel Payment",
                    "Would you like to cancel the payment?",
                    [
                        { text: "Proceed", },
                        {
                            text: "Cancel",
                            onPress: () => {
                                this.setState(({
                                    paymentmodalvisible: false,
                                    payment_url: ''
                                }))
                            }
                        },
                    ],
                    { cancelable: false }
                );
            }
        }
    }


    componentDidMount = async () => {
        // alert(this.props.f_name);
        // let todonate = this.props.navigation.getParam('from');
        // this.setState({ todonate });
        // if (todonate == 'campaign') {
        //     this.setState({ id: this.props.navigation.getParam('campaign_id') });
        //     this.setState({ selectedAmount: this.props.navigation.getParam('amount') })
        //     this.setState({ name: this.props.navigation.getParam('campaign_name') });
        //     this.setState({ amount: this.props.navigation.getParam('donateamount') })
        // }
        // else {
        //     this.setState({ id: this.props.navigation.getParam('program_id') });
        //     this.setState({ name: this.props.navigation.getParam('program_name') });
        // }
    }

    // makePaymentApiRequest = async () => {
    //     let apitoken = await AsyncStorage.getItem('APITOKEN');
    //     console.log('token --->', apitoken);
    //     let response = '';

    //     if (this.state.todonate == 'campaign') {

    //         response = await campaignDonation(apitoken, this.state.id, this.collectAmount(),
    //             this.state.transectionid);
    //     }

    //     else {
    //         response = await programDonation(apitoken, this.state.id, this.collectAmount(), this.state.transectionid);

    //     }


    //     let result = await response.json();

    //     if (result.success) {
    //         this.props.navigation.navigate('payment', {
    //             'amount': this.collectAmount(),
    //             'from': this.state.todonate
    //         });
    //         await notificationService('You have Donated Successfully');

    //     }
    //     else {
    //     }
    // }

    // renderTextInputBox = () => {
    //     if (this.state.selectedAmount == 'other') {
    //         return (
    //             <View style={{ marginBottom: 8 }}>
    //                 <TextInput
    //                     placeholder={this.props.userlang.amount}
    //                     value={this.state.amount}
    //                     onChangeText={(text) => this.setState({ amount: text.replace(/[^0-9]/g, '') })}
    //                     keyboardType="numeric"
    //                     maxLength={7}
    //                     style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
    //                 />
    //                 <Text style={{ color: 'red' }} >
    //                     {this.state.error}
    //                 </Text>
    //             </View>
    //         )
    //     }
    //     return null
    // }

    // collectAmount = () => {
    //     if (this.state.selectedAmount == 'other') {
    //         return this.state.amount
    //     }
    //     return this.state.selectedAmount
    // }

    // paymentCheck = () => {
    //     if (this.collectAmount() == '') {
    //         this.setState({ error: 'Enter Amount' });
    //         return null
    //     }
    //     this.setState({ error: '' });
    //     var options = {
    //         description: 'Credits towards consultation',
    //         image: 'https://i.imgur.com/3g7nmJC.png',
    //         currency: 'INR',
    //         key: 'rzp_test_FU9mAyXWz8cTF1',
    //         amount: '500',
    //         external: {
    //             wallets: ['paytm']
    //         },
    //         name: 'Ravi',
    //         prefill: {
    //             email: 'ravi@razorpay.com',
    //             contact: '932806560',
    //             name: 'Ravi Gahlot',
    //         },
    //         theme: { color: '#F37254' }
    //     }
    //     RazorpayCheckout.open(options).then((data) => {
    //         // handle success
    //         this.setState({ transectionid: data.razorpay_payment_id })
    //         //alert(`Success: ${data.razorpay_payment_id}`);
    //         this.makePaymentApiRequest()
    //         // notificationService('You are Donated Successfully');
    //     }).catch((error) => {
    //         // handle failure
    //         alert(`Error: ${error.code} | ${error.description}`);
    //     });
    //     RazorpayCheckout.onExternalWalletSelection(data => {
    //         alert(`External Wallet Selected: ${data.external_wallet} `);
    //     });
    // }

    onPayFromSaddad = () => {
        if (!this.state.amount) {
            alert('Enter valid donation price');
            return
        }
        if (parseFloat(this.state.amount).toString() == 'NaN') {
            alert('Invalid valid donation price');
            return
        }

        this.createPaymentUrl((url) => {
            if (url) {
                this.setState({
                    payment_url: url,
                    paymentmodalvisible: true
                })
            }
        })
    }


    createPDF = async () => {
        let lang = await AsyncStorage.getItem('LANGUAGE');

        // let options_english = {
        //     html: bank_english,
        //     fileName: 'AlrahmahBankDetail',
        //     directory: 'Alrahmah App',
        // }

        // let options_arabic = {
        //     html: bank_arabic,
        //     fileName: 'الرحمهبنكدتل',
        //     directory: 'الرحمه أب',
        // }
        if (this.props.selected_language === 'en') {
            let options_english = {
                html: bank_english,
                fileName: 'AlrahmahBankDetail',
                directory: 'Alrahmah App',
            };

            let file = await RNHTMLtoPDF.convert(options_english);
            Alert.alert(
                'PDF Save',
                'Path Alrahmah App/AlrahmahBankDetail.pdf'
            );
        }
        else {
            let options_arabic = {
                html: bank_arabic,
                fileName: 'AlrahmahBankDetail',
                directory: 'Alrahmah App',
            };

            let file = await RNHTMLtoPDF.convert(options_arabic);
            Alert.alert(
                'حفظ PDF',
                'بث الرحمه أب/الرحمهبنكدتل.بدف'
            );
        }
    }

    render() {
        return (
            <View style={[baseStyle.appContainer, baseStyle.paddingLg]}>
                <this._paymentView />
                <ScrollView>

                    {
                        this.props.selected_language == 'en'
                            ?

                            <View style={{ marginTop: 30 }}>
                                <Text style={{ fontSize: 18 }}>
                                    Dear visitor,
                                </Text>
                                <Text style={{ fontSize: 14, marginTop: 10 }}>
                                    Due to many restrictions we are facing related to COVID-19 preventive measures, the launch of our online donation service will be postponed until a later time.
                    </Text>
                                <Text style={{ marginTop: 10 }}>
                                    In the meanwhile, you can still donate through direct bank transfers to our bank accounts:
                    </Text>

                                <View style={{ flexDirection: 'row', marginTop: 30 }}>
                                    <Image source={require('../../assets/bank_image_1.png')}
                                        style={{
                                            height: 50,
                                            width: 50,
                                            borderRadius: 20
                                        }}
                                    />
                                    <Text style={{ fontSize: 16, margin: 10 }}>
                                        Al Rajhi Bank IBAN: SA1280000-227608010000807
                            </Text>
                                </View>

                                <View style={{ flexDirection: 'row' }}>
                                    <Image source={require('../../assets/bank_image_1.png')}
                                        style={{
                                            height: 50,
                                            width: 50,
                                            borderRadius: 20
                                        }}
                                    />
                                    <Text style={{ fontSize: 16, margin: 10 }}>
                                        {`Al Rajhi Bank IBAN: SA8780000-227608010000815 \n(for Zakat)`}

                                    </Text>
                                </View>

                                <View style={{ flexDirection: 'row' }}>
                                    <Image source={require('../../assets/bank_image_2.png')}
                                        style={{
                                            height: 50,
                                            width: 50,
                                            borderRadius: 20
                                        }}
                                    />
                                    <Text style={{ fontSize: 16, margin: 10 }}>
                                        Alinma Bank IBAN : SA71050000-68204444404000
                            </Text>
                                </View>
                            </View>

                            :

                            <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                <Text style={{ fontSize: 18 }}>
                                    الزائر الكريم..
                    </Text>
                                <Text style={{ fontSize: 14, marginTop: 10 }}>
                                    نظراً للعديد من القيود التي نواجهها والمتعلقة بالإجراءات الوقائية ضد تفشي جائحة فيروس كورونا الجديد، فسيتم تأجيل إطلاق خدمة التبرع الإلكتروني عبر الموقع إلى وقت لاحق.

                    </Text>
                                <Text style={{ marginTop: 10 }}>
                                    وفي هذه الأثناء، سيكون بإمكانك التبرع عن طريق التحويل البنكي المباشر لأحد حساباتنا البنكية التالية:
                    </Text>

                                <View style={{
                                    marginTop: 20,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center'
                                }}>
                                    <Text style={{ fontSize: 16, margin: 10 }}>
                                        صرف الراجحي (حساب الصدقات): SA1280000-227608010000807
                            </Text>
                                    <Image source={require('../../assets/bank_image_1.png')}
                                        style={{
                                            height: 50,
                                            width: 50,
                                            borderRadius: 20
                                        }}
                                    />
                                </View>

                                <View style={{
                                    marginTop: 20,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center'
                                }}>
                                    <Text style={{ fontSize: 16, margin: 10 }}>
                                        مصرف الراجحي (حســـــاب الــزكــــــــــاة): SA8780000-227608010000815
                            </Text>
                                    <Image source={require('../../assets/bank_image_1.png')}
                                        style={{
                                            height: 50,
                                            width: 50,
                                            borderRadius: 20
                                        }}
                                    />
                                </View>

                                <View style={{
                                    marginTop: 20,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center'
                                }}>
                                    <Text style={{ fontSize: 16, margin: 10 }}>
                                        مصرف الإنــمــــــاء (حساب الصدقات): SA71050000-68204444404000
                            </Text>

                                    <Image source={require('../../assets/bank_image_2.png')}
                                        style={{
                                            height: 50,
                                            width: 50,
                                            borderRadius: 20
                                        }}
                                    />
                                </View>
                            </View>
                    }

                    <Text style={{ fontSize: 16, margin: 10, textAlign: 'center' }}>or</Text>
                    <AppInput
                        placeholder='Name'
                        value={this.state.name}
                        onChangeText={(name) => this.setState({ name })}
                    />
                    <AppInput
                        placeholder='Donation Amount'
                        value={this.state.amount}
                        errorMessage={this.state.error}
                        keyboardType='numeric'
                        onChangeText={(amount) => this.setState({ amount })}
                    />

                </ScrollView>

                <AppButton
                    title={'Pay from Sadadbh'}
                    containerStyle={{ marginBottom: margin.xl }}
                    buttonStyle={{ alignSelf: 'flex-end' }}
                    onPress={this.onPayFromSaddad}
                />
            </View>
        )
    }

    // render() {
    //     return (
    //         <View style={[baseStyle.appContainer, baseStyle.paddingLg]}>
    //             <ScrollView>
    //                 <Text style={styles.heading}>{this.props.userlang.program_to_donate}</Text>
    //                 <AppInput
    //                     label={this.props.userlang.program_name}
    //                     editable={false}
    //                     value={this.state.name}
    //                     placeholder="Program..." />

    //                 <Text style={styles.subHeading}>{this.props.userlang.your_donation}</Text>
    //                 <View style={styles.cardBorder}>
    //                     <Text style={styles.heading}>{this.props.userlang.select_amount}</Text>
    //                     <View style={styles.row}>
    //                         <AppButton
    //                             title={`${this.props.userlang.sar}10 `}
    //                             buttonStyle={styles.btnStyle}
    //                             titleStyle={this.state.selectedAmount == 10 ? { color: colors.green } : { color: colors.black }}
    //                             containerStyle={this.state.selectedAmount == 10 ? [styles.btnContainer, styles.activeBtn] : styles.btnContainer}
    //                             onPress={() => this.setState({ selectedAmount: 10 })}
    //                         />
    //                         <AppButton
    //                             title={`${this.props.userlang.sar}50 `}
    //                             buttonStyle={styles.btnStyle}
    //                             titleStyle={this.state.selectedAmount == 50 ? { color: colors.green } : { color: colors.black }}
    //                             containerStyle={this.state.selectedAmount == 50 ? [styles.btnContainer, styles.activeBtn] : styles.btnContainer}
    //                             onPress={() => this.setState({ selectedAmount: 50 })}
    //                         />
    //                         <AppButton
    //                             title={`${this.props.userlang.sar}80 `}
    //                             buttonStyle={styles.btnStyle}
    //                             titleStyle={this.state.selectedAmount == 80 ? { color: colors.green } : { color: colors.black }}
    //                             containerStyle={this.state.selectedAmount == 80 ? [styles.btnContainer, styles.activeBtn] : styles.btnContainer}
    //                             onPress={() => this.setState({ selectedAmount: 80 })}
    //                         />
    //                         <AppButton
    //                             title={this.props.userlang.other}
    //                             buttonStyle={styles.btnStyle}
    //                             titleStyle={this.state.selectedAmount == 'other' ? { color: colors.green } : { color: colors.black }}
    //                             containerStyle={styles.btnContainer}
    //                             onPress={() => this.setState({ selectedAmount: 'other' })}
    //                         />
    //                     </View>
    //                     < this.renderTextInputBox />
    //                 </View>

    //                 <Text style={styles.heading}>{this.props.userlang.yourinfo}</Text>
    //                 <AppInput
    //                     label={this.props.userlang.f_name}
    //                     placeholder={this.props.userlang.f_name}
    //                     value={this.props.f_name}
    //                     inputContainerStyle={baseStyle.marginBottomSm} />

    //                 <AppInput
    //                     label={this.props.userlang.l_name}
    //                     placeholder={this.props.userlang.l_name} value={this.props.l_name}
    //                     inputContainerStyle={baseStyle.marginBottomSm}
    //                 />
    //                 <AppInput
    //                     label={this.props.userlang.mobile}
    //                     placeholder={this.props.userlang.mobile}
    //                     value={this.props.mobile}
    //                     inputContainerStyle={baseStyle.marginBottomSm}
    //                 />
    //                 <AppInput
    //                     label={this.props.userlang.email}
    //                     placeholder={this.props.userlang.email}
    //                     editable={false} value={this.props.email}
    //                     inputContainerStyle={baseStyle.marginBottomSm}
    //                 />


    //                 <AppButton
    //                     title={this.props.userlang.pay}
    //                     onPress={() => this.paymentCheck()}
    //                 />
    //             </ScrollView>
    //         </View>
    //     );
    // }

    _paymentView = () => {

        return (
            <Modal
                visible={this.state.paymentmodalvisible}
                animationType='slide'
                onRequestClose={() => this.onPaymetGoBack()}
            >

                <SafeAreaView style={{ flex: 1 }}>
                    <TouchableOpacity
                        onPress={this.onPaymetGoBack}
                        style={{
                            height: 45
                        }}>
                        <Image source={require('../../assets/back.png')} style={{
                            width: 25,
                            height: 25,
                            marginLeft: 20,
                            resizeMode: 'contain'
                        }} />
                    </TouchableOpacity>
                    {this.renderActivity()}
                    <WebView
                        ref={(webView) => {
                            this.webView = webView
                        }}
                        source={{ uri: this.state.payment_url }}
                        onNavigationStateChange={this.onNavigationStateChange}
                        onLoadStart={() => this.setState({ activity: true })}
                        onLoadEnd={() => this.setState({ activity: false })}
                    />
                </SafeAreaView>
            </Modal>
        )
    }

    renderActivity = () => {
        if (this.state.activity)
            return (

                <View style={{
                    position: 'absolute',
                    backgroundColor: 'rgba(0,0,0,0)',
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                    alignItems: 'center',
                    justifyContent: 'center',
                    zIndex: 1
                }} >
                    <ActivityIndicator size="large" color="#4ABEA1" />
                </View>
            )
        else
            return null
    }
}

const styles = {
    heading: {
        fontSize: fonts.xl,
        fontWeight: 'bold',
        marginVertical: margin.xl
    },
    subHeading: {
        fontSize: fonts.xl
    },
    btnStyle: {
        backgroundColor: colors.white,
        alignSelf: 'center',
    },
    btnContainer: {
        borderColor: colors.btnBorder,
        borderWidth: 1,
        paddingHorizontal: padding.sm,
        borderRadius: radius.sm,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        justifyContent: 'space-between',
        marginVertical: margin.lg
    },
    activeBtn: {
        borderColor: colors.green,
    }
}

const mapStateToProps = state => {
    return {
        f_name: state.userDetail.f_name,
        l_name: state.userDetail.l_name,
        mobile: state.userDetail.mobile,
        email: state.userDetail.email,
        userlang: state.rootReducer.userLanguage,
        selected_language: state.userDetail.userlanguage,
    }
}

export default connect(mapStateToProps, { setUser })(DonateNow);