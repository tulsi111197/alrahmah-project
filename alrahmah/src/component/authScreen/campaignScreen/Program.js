import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, FlatList, ActivityIndicator, Image, Modal, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image';
import { Tile, Text, colors } from 'react-native-elements';
import { baseStyle, padding } from '../../style';
import fetchUserData from '../../api unitlity function/fetchuserdata';
import { prgramdetail } from '../../api utility/apiutility';
import { setProgramDetails } from '../../store/reducer/action/programdetailaction';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

class Program extends Component {

    state = {
        token: '',
        lang: 'en',
        isloading: false,
    }

    componentDidMount = async () => {
        let token = await AsyncStorage.getItem('APITOKEN');
        let lang = await AsyncStorage.getItem('LANGUAGE');
        this.setState({ lang })
        this.setState({ token });
        console.log(this.props.selected_language);
    }

    navigateUserToProgramDetails = async ({ item }) => {
        this.setState({ isloading: true });
        let uri = `${prgramdetail}${item.id}`
        try {
            let response = await fetchUserData(uri, this.state.token, this.props.selected_language);
            await this.props.setProgramDetails(response.data);
            this.setState({ isloading: false });
            this.props.navigation.navigate('programDetail');
        }
        catch (error) {
            this.setState({ isloading: false })
        }
    }

    renderActivity = () => {
        if (this.state.isloading)
            return (
                <Modal
                    visible={true}
                    transparent={true}
                >
                    <View style={{
                        position: 'absolute',
                        backgroundColor: 'rgba(0,0,0,0.3)',
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }} >
                        <ActivityIndicator size="large" color="#4ABEA1" />
                    </View>
                </Modal>

            )
        else
            return null
    }

    render() {

        return (
            <View style={{ ...baseStyle.appContainer }}>
                <this.renderActivity />
                <FlatList
                    data={this.props.program_list}
                    keyExtractor={item => item.id}
                    refreshing={true}
                    renderItem={({ item }) => {
                        if (item.lang_programs) {
                            return (
                                <View>
                                    <TouchableOpacity
                                        style={{ ...styles.contentContainer }}
                                        onPress={() => this.navigateUserToProgramDetails({ item })}
                                    >
                                        {/* <Tile
imageSrc={{ uri: item.image }}
title={item.lang_programs ? item.lang_programs.heading : ''}
titleStyle={{ textAlign: 'center' }}
imageContainerStyle={{ borderRadius: 50 }}
containerStyle={{ width: '100%', alignSelf: 'center', borderRadius: 50 }}
onPress={() => this.navigateUserToProgramDetails({ item })}
onLoadE
> */}

                                        <FastImage
                                            source={{ uri: item.image }}
                                            style={{
                                                width: '100%', alignSelf: 'center', borderRadius: 10,
                                                height: 250,
                                                backgroundColor: colors.greyOutline
                                            }}
                                        />
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={{ ...styles.contentContainer }}
                                        onPress={() => this.navigateUserToProgramDetails({ item })}
                                    >

                                        <Text style={{
                                            textAlign: 'center',
                                            fontSize: 25,
                                            fontWeight: '400',
                                            marginTop: 20
                                        }}>
                                            {item.lang_programs ? item.lang_programs.heading : ''}
                                        </Text>

                                        <Text style={{ textAlign: 'center', fontSize: 16 }}>
                                            {
                                                item.lang_programs ? item.lang_programs.program_short_desc : ''
                                            }
                                        </Text>

                                    </TouchableOpacity>
                                </View>
                            )
                        }
                        return null
                    }

                    }
                />
            </View>
        );
    }
}

const styles = {
    contentContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: padding.lg,

    },
    titleStyle: {
        textAlign: 'center'
    }

}


const mapStateToProp = state => {
    return {
        program_list: state.rootReducer.programlist,
        userlang: state.rootReducer.userLanguage,
        selected_language: state.userDetail.userlanguage,
    }
}


export default connect(mapStateToProp, { setProgramDetails })(Program);