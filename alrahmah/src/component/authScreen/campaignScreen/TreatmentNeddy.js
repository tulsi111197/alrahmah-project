import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, FlatList, Modal, ActivityIndicator, Image, Dimensions, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Text } from 'react-native-elements';
import { baseStyle, fonts, colors, margin, padding, radius, } from '../../style';
import fetchsearchData from '../../api unitlity function/fetchuserdata';
import AppTile from '../../utitlity/AppTile';
import ModalDropdown from 'react-native-modal-dropdown';
import { campaingdetails, search } from '../../api utility/apiutility';
import { setCampaignDetails } from '../../store/reducer/action/campaingDetailsAction';
import AppInput from '../../utitlity/AppInput';

class TreatmentNeedy extends Component {
    state = {
        categories_id: 1,
        campaign_list: [],
        token: '',
        isloading: false,
        resultfound: '',
        lang: 'en',
        categorie_name: '',
        categorie_image: '',
    }

    componentDidMount = async () => {
        let token = await AsyncStorage.getItem('APITOKEN');
        let lang = await AsyncStorage.getItem('LANGUAGE');
        if (lang == 'ar')
            this.setState({ lang: 'ar' })
        this.setState({ token });

        this.setState({ categories_id: this.props.navigation.getParam('categoriesid') });
        this._toggleDropDownImage(this.props.navigation.getParam('categoriesid'));
        this.setState({ campaign_list: this.props.campaign_list });
    }

    calculatePercentages = (item) => {
        let percentages = (item.gain_fund / item.needed_fund) * 100
        return (parseInt(percentages));
    }

    calculateDay = ({ item }) => {
        if (item.campaign_end_date) {
            let d = new Date();
            const year = item.campaign_end_date.slice(0, 4) - d.getFullYear();
            const month = item.campaign_end_date.slice(6, 7) - d.getMonth();
            const day = item.campaign_end_date.slice(9, 10) - d.getDate();
            let rem = (day + (month * 30) + (year * 365));
            if (rem < 0) {
                return 1
            }
            return rem
        }
        return 1
    }

    navigateUserToCampaignDetails = async ({ item }) => {
        try {
            this.setState({ isloading: true })
            let response = await this.fetchCampaignDetailsInRedux(this.state.token, item.id);
            await this.props.setCampaignDetails(response.data);
            this.props.navigation.navigate('campaignDetail');
            this.setState({ isloading: false })
        }
        catch (error) {
            this.setState({ isloading: false })
        }
    }

    fetchCampaignDetailsInRedux = async (token, id) => {
        let url = `${campaingdetails}${id}`;
        let result = await fetch(url, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`,
                'X-localization': this.props.selected_language
            }
        })
        let response = await result.json();
        return response;
    }

    searchQuery = async (query) => {
        if (query.length > 3) {
            let result = await fetchsearchData(search + query, this.state.token, 'en');
            this.setState({ resultfound: result.data.length + ' results' });
            this.setState({ campaign_list: result.data });
        }
        if (query == '') {
            this.setState({ campaign_list: this.props.campaign_list });
            this.setState({ resultfound: '' });
        }
    }

    renderActivity = () => {
        if (this.state.isloading)
            return (
                <Modal
                    visible={true}
                    transparent={true}
                >
                    <View style={{
                        position: 'absolute',
                        backgroundColor: 'rgba(0,0,0,0.3)',
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }} >
                        <ActivityIndicator size="large" color="#4ABEA1" />
                    </View>
                </Modal>
            )
        else
            return null
    }

    renderListView = ({ item }) => {

        if (item.lang_campaing) {
            if (this.state.categories_id == 'All' || item.categories_id == this.state.categories_id) {

                return (
                    <View style={{ flex: 1}}>
                        <AppTile
                            source={{ uri: item.campaing_cover_image }}
                            title={item.lang_campaing.campaing_name}
                            height={180}
                            width={370}
                            overlayStyle={{borderRadius:180/5}}
                            onPress={() => this.navigateUserToCampaignDetails({ item })}
                            containerStyle={{ marginBottom:margin.lg,borderRadius:180/5,overflow:'hidden'}}
                            caption={
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ flex: 1, fontSize: fonts.xl, color: colors.white }}>SAR {item.campaing_amount}</Text>
                                    <Text style={{ fontSize: fonts.xl, color: colors.white }}>{this.calculateDay({ item })} Day Left </Text>
                                </View>
                            }
                        />
                    </View>
                )
            }
        }
        return null
    }

    _toggleDropDownImage = (index) => {

        if (index == 0 || index == 'All') {
            this.setState({ categorie_name: 'ALL' });
            this.setState({ categories_id: 'All' });
            this.setState({ categorie_image: require('../../assets/all.png') })
        }
        else if (index == 1) {
            this.setState({ categorie_name: 'Device' });
            this.setState({ categories_id: 1 });
            this.setState({ categorie_image: require('../../assets/smartwatch.png') })
        }
        else if (index == 2) {
            this.setState({ categorie_name: 'Treatment' });
            this.setState({ categories_id: 2 });
            this.setState({ categorie_image: require('../../assets/heart.png') })
        }
        else if (index == 3) {
            this.setState({ categorie_name: 'Medicine' });
            this.setState({ categories_id: 3 });
            this.setState({ categorie_image: require('../../assets/meds.png') })
        }
    }

    renderCategoryById = () => {
        switch (this.state.categories_id) {
            case 1:
                return this.props.userlang.device
            case 2:
                return this.props.userlang.treatment
            case 3:
                return this.props.userlang.medicine
            case 'All':
                return this.props.userlang.all
            default:
                return ''
        }
    }


    render() {
        console.log('category id --->', this.renderCategoryById());
        return (



            <View style={[baseStyle.appContainer, baseStyle.paddingLg]}>
                <View>
                <AppInput
                        inputStyle={this.props.selected_language === 'ar' && Platform.OS === 'android' ? { textAlign: 'right' }: {}}
                        placeholder={this.props.userlang.search}
                       inputContainerStyle={{ paddingVertical:28,}}
                        onChangeText={(query) => this.searchQuery(query.trim())}
                        lang={this.props.language}
                    />
                </View>
            
                    <ModalDropdown
                        options={
                            [
                                this.props.userlang.all,
                                this.props.userlang.device, this.props.userlang.treatment,
                                this.props.userlang.medicine
                            ]}
                        dropdownStyle={{ width: '100%'}}
                        style={{marginTop:100,marginBottom:margin.lg}}
                        dropdownTextStyle={{ fontSize: 18 }}
                        accessible={true}
                        onSelect={(index) => this._toggleDropDownImage(index)}
                        animated={true}
                        scrollEnabled={false}
                    >
                        <View style={styles.dropdownStyle}>
                        <Image
                            source={this.state.categorie_image}
                            style={{
                                width: 25,
                                height: 25,
                                resizeMode: 'contain'
                            }}
                        />
                        <Text style={{fontSize: 16,marginLeft:margin.lg }}>{this.renderCategoryById()}</Text>
                        </View>
                    </ModalDropdown>
                
                    <View >
                    
                </View>
              

            
                <FlatList
                    data={this.state.campaign_list}
                    keyExtractor={item => item.id}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) => this.renderListView({ item })}
                />
                <this.renderActivity />
            </View>
        );
    }
}

const styles = {
    circularWrapper: {
        position: 'absolute',
        bottom: 0,
        right: 20,
        left: 'auto'
    },
    searchBodyStyle: {
        backgroundColor: '#DEDEDE',
        borderWidth: 0.4,
        flexDirection: 'row'

    },
    dropdownStyle:{
        flexDirection:'row',
        alignItems:'center',
        width:'100%',
        borderWidth:1,
        borderColor:colors.borderColor,
        paddingVertical:padding.lg,
        borderRadius:radius.lg,
        paddingHorizontal:padding.md
    }
}

const mapStateToProp = state => {
    return {
        selected_language: state.userDetail.userlanguage,
        campaign_list: state.rootReducer.dashboardresponse.campaign_list,
        userlang: state.rootReducer.userLanguage,
        language: state.userDetail.userlanguage,
    }
}


export default connect(mapStateToProp, { setCampaignDetails })(TreatmentNeedy);
