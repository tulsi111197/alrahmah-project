import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import { connect } from 'react-redux';
import { baseStyle, fonts, colors } from '../../style';
import AppButton from '../../utitlity/AppButton';
import { setTotalDonateAmount, setRootReduser } from '../../store/reducer/action/RootReducerAction';

class PaymentDone extends Component {
    componentDidMount = () => {
        let amount = parseInt(this.props.total_donate) + parseInt(this.props.navigation.getParam('amount'));
        this.props.setTotalDonateAmount(amount);
    }

    render() {
        return (
            <View style={[baseStyle.appContainer, baseStyle.paddingLg]}>
                <View style={styles.container}>
                    <Image
                        source={require('../../assets/thankyou-img.png')}
                        style={styles.imgStyle}
                    />
                    <Text style={styles.heading}> {this.props.userlang.thanks} </Text>
                    <Text style={styles.textStyle}>{this.props.userlang.amount_received}</Text>
                </View>
                <AppButton
                    title={this.props.userlang.continue}
                    buttonStyle={{ width: '100%' }}
                    onPress={() => this.props.navigation.navigate('dashboard')}
                />

            </View>
        );
    }
}

const styles = {
    imgStyle: {
        width: 350,
        height: 250,
        resizeMode: 'contain',
    },
    heading: {
        fontSize: fonts.xlarge,
        fontWeight: 'bold',
    },
    textStyle: {
        fontSize: fonts.xl,
        color: colors.textMuted,
        textAlign: 'center'
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    }
}
const mapStateToProps = state => {
    return {
        userlang: state.rootReducer.userLanguage,
        total_donate: state.rootReducer.dashboardresponse.total_donate,
        campaign: state.campaingDetails.campaignDetail,
        mydonation: state.rootReducer.mydonation
    }
}

export default connect(mapStateToProps, { setTotalDonateAmount, setRootReduser })(PaymentDone);