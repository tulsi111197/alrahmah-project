import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ScrollView, Dimensions, Alert } from 'react-native';
import { Tile, Text, Header } from 'react-native-elements';
import { baseStyle, padding, colors, fonts, margin } from '../../style';
import AppTile from '../../utitlity/AppTile';
import { OtherContent } from '../../Constent/AppConstent';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import AppButton from '../../utitlity/AppButton';
import HTML from 'react-native-render-html';
import {
    getFlexDirection, getAlignItems, isLangArabic,
    getTextAlign
} from '../../Constent/helperMethods/langMethods';


class ProgramDetail extends Component {

    callLogout = async () => {
        await AsyncStorage.clear();
        await this.props.navigation.navigate('welcome');
    }

    navigateToVolenter = () => {
        if (OtherContent.getToken() == null) {
            Alert.alert(
                '',
                this.props.userlang.you_need_to_login_first,
                [
                    { text: this.props.userlang.cancel, onPress: () => console.log('Cancel Pressed') },
                    { text: this.props.userlang.ok, onPress: () => this.callLogout() },
                ],
                { cancelable: false }
            )
        }
        else {
            this.props.navigation.navigate('voulenteer')
        }

    }

    renderVolenterButton = () => {

        if (this.props.navigation.getParam('from') === 'myprogram')
            return null
        return (
            <AppButton
                title={this.props.userlang.become_volunteer}
                buttonStyle={baseStyle.bgLabelBlackColor}
                onPress={() => this.navigateToVolenter()}
            />
        )
    }

    render() {
        console.log('program detail --->', this.props.programDetail);
        if (this.props.programDetail.lang_programs == null) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                    <Text>{this.props.userlang.no_data} </Text>
                </View>
            )
        }
        const lang = this.props.language;

        return (
            <View style={baseStyle.appContainer}>
                <ScrollView>
                    <AppTile
                        source={{ uri: this.props.programDetail.image }}
                        height={450}
                        containerStyle={{ borderRadius: 0 }}
                    />
                    <View style={baseStyle.paddingLg}>
                        <Text style={{
                            ...styles.heading,
                            textAlign: getTextAlign(lang)
                        }}>{this.props.programDetail.lang_programs.heading}</Text>
                        <Text style={{
                            ...styles.text,
                            textAlign: getTextAlign(lang)
                        }}>{this.props.programDetail.lang_programs.program_short_desc}</Text>
                        <Text style={{
                            ...styles.heading,
                            textAlign: getTextAlign(lang)
                        }}>{this.props.userlang.about_program}</Text>
                        {/* <Text style={styles.text}>{this.props.programDetail.lang_programs.program_desc}</Text> */}
                        <View style={{
                            alignItems: isLangArabic(lang) ? 'flex-end' : 'flex-start'
                        }}>
                            <HTML html={
                                this.props.programDetail.lang_programs.program_desc
                            } imagesMaxWidth={Dimensions.get('window').width} />
                        </View>

                        {/* <Text style={styles.heading}>Objectives of the program</Text>
                        <Text style={styles.text}>hospital with which the association has sponsorship
                        contracts, and receives the treatment services he needs without bearing the value.</Text>

                        <View style={{ flexDirection: 'row', marginBottom: margin.lg, alignItems: 'center' }}>
                            <Icon name="circle" color='#4ABEA1' iconStyle={{ marginTop: 10 }} />
                            <Text style={styles.textStyle}>ospital with which the association has ship contracts, and receives.</Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginBottom: margin.lg, alignItems: 'center' }}>
                            <Icon name="circle" color='#4ABEA1' iconStyle={{ marginTop: 10 }} />
                            <Text style={styles.textStyle}>ospital with which the association has ship contracts, and receives.</Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginBottom: margin.lg, alignItems: 'center' }}>
                            <Icon name="circle" color='#4ABEA1' iconStyle={{ marginTop: 10 }} />
                            <Text style={styles.textStyle}>ospital with which the association has ship contracts, and receives.</Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginBottom: margin.lg, alignItems: 'center' }}>
                            <Icon name="circle" color='#4ABEA1' iconStyle={{}} />
                            <Text style={styles.textStyle}>ospital with which the association has ship contracts, and receives.</Text>
                        </View> */}
                    </View>
                    <View style={[baseStyle.paddingLg, baseStyle.justifyContentEnd]}>
                        <AppButton
                            title={this.props.userlang.donate_now}
                            buttonStyle={{alignSelf:'flex-end'}}
                            onPress={() => this.props.navigation.navigate('donate', { from: 'program', program_id: this.props.programDetail.lang_programs.id, program_name: this.props.programDetail.lang_programs.heading })}
                        />
                        {/* {this.renderVolenterButton()} */}
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = {
    backImg: {
        width: 20,
        height: 15,
        resizeMode: 'contain',
        marginLeft: padding.lg
    },
    heading: {
        fontSize: fonts.xl,
        fontWeight: 'bold',
        marginVertical: margin.lg
    },
    bulletStyle: {
        colors: colors.green
    },
    textStyle: {
        flex: 1,
        paddingLeft: padding.lg,
        fontSize: fonts.xl,
    },
    text: {
        fontSize: fonts.lg,
        marginBottom: margin.md
    }
}

const mapStateToProp = state => {
    return {
        programDetail: state.programDetails.programDetail,
        userlang: state.rootReducer.userLanguage,
        language: state.userDetail.userlanguage,
    }
}


export default connect(mapStateToProp, null)(ProgramDetail);