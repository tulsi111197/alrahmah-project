import { campadonation } from '../api utility/apiutility';

export default fetchUserDataByAPI = async (token, id, amount, transectionid) => {
    let response = fetch(campadonation, {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "campaign_id": id,
            "donate_amount": amount,
            "transaction_id": transectionid
        })
    })
    return response
}
