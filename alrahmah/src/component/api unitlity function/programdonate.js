import { programdonate } from '../api utility/apiutility';
 
export default fetchUserDataByAPI = async (token, id, amount, transectionid) => {

    let response = fetch(programdonate, {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "program_id": id,
            "donate_amount": amount,
            "transaction_id": transectionid
        })
    })
    return response
}
