import { notificationApi } from '../api utility/apiutility';
import { Platform } from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import { OtherContent } from '../Constent/AppConstent';

export default fetchUserDataByAPI = async (data) => {
    let device = Platform.OS === 'ios' ? 'ios' : 'android';

    let response = await fetch(notificationApi, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            device_token: OtherContent.getDeviceToken(),
            device_type: Platform.OS,
            message: data
        })
    });
    let result = await response.json();
    return result;
}