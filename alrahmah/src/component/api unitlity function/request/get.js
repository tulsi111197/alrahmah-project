
import Snackbar from 'react-native-snackbar';
import { headerwithoutBearer, headerwithBearer } from '../../constant/ServiceConstants';
import { colors } from '../../constant/styles/Style';

export const getRequest = async ({ header = headerwithoutBearer, url }) => {
    try {
        const controller = new AbortController();
        setTimeout(() => controller.abort(), 15000);

        const response = await fetch(url, {
            headers: header,
            //signal: controller.signal
        });
        return await response.json();
    }
    catch (e) {
        console.log(e.message);
        if (e.message == 'Network request failed') {
            Snackbar.show({
                text: "No Internet Connection",
                duration: 2000,
                backgroundColor: colors.red
            })
            return false
        }
        if (e.message == 'Aborted') {
            alert('Unable to connect with server');
            return false
        }
    }
};