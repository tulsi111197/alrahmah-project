import React from 'react';
import { Button } from 'react-native-elements';
import { colors, compHeight, margin } from '../style';


export default AppButton = ({ title, buttonStyle, onPress, containerStyle, titleStyle }) => {
    return (
        <Button
            title={title}
            titleStyle={titleStyle}
            onPress={onPress}
            buttonStyle={[styles.buttonStyle, buttonStyle]}
            containerStyle={[styles.containerStyle, containerStyle]}
        />
    );
}

const styles = {
    buttonStyle: {
        backgroundColor: colors.green,
        borderTopLeftRadius: 30,
        borderBottomLeftRadius: 30,
        height: compHeight.xxl,
        width: '60%',
    },
    containerStyle: {
        marginBottom: margin.lg,

    }
}