import React from 'react';
import { Slider } from 'react-native-elements'
import { compHeight, radius, colors } from '../style';


export default AppSlider = ({ value, trackStyle, maximumValue, minimumValue }) => {
    return (
        <Slider
            value={value}
            disabled={true}
            minimumTrackTintColor={'#4ABEA1'}
            animateTransitions={true}
            trackStyle={[styles.trackStyle, trackStyle]}
            thumbStyle={{ backgroundColor: null }}
            maximumValue={maximumValue}
            minimumValue={minimumValue}
        />
    )
}

const styles = {
    trackStyle: {
        height: 12,
        borderRadius: radius.sm,
        borderColor: colors.borderColor,
        borderWidth: 1,
    }
}