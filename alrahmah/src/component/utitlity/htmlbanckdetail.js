export const HTML =  ` <html>
<head>
    <title>Title of the document</title>
    <style>
        table,
        th,
        td {
            padding: 10px;
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>

<body>
    
        <h1>Account Information</h1>
        <table>
            <tr>
                <th>Account Holder Name</th>
                <th>mohan</th>
            </tr>
            <tr>
                <td>Account Name</td>
                <td>123</td>
            </tr>
            <tr>
                <td>Bank Name</td>
                <td>sbi</td>
            </tr>
            <tr>
                <td>Branch Name</td>
                <td>bkn</td>
            </tr>
            <tr>
                <td>IFSC Code</td>
                <td>22</td>
            </tr>
        </table>
    
</body>

</html>

</html>`;

export const a_HTML =  ` <html>
<head>
    <title>Title of the document</title>
    <style>
        table,
        th,
        td {
            padding: 10px;
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>

<body>
    
        <h1>معلومات الحساب</h1>
        <table>
            <tr>
                <th>اسم صاحب الحساب</th>
                <th>mohan</th>
            </tr>
            <tr>
                <td>أسم الحساب</td>
                <td>123</td>
            </tr>
            <tr>
                <td>اسم البنك</td>
                <td>sbi</td>
            </tr>
            <tr>
                <td>اسم الفرع</td>
                <td>bkn</td>
            </tr>
            <tr>
                <td>كود IFSC</td>
                <td>22</td>
            </tr>
        </table>
    
</body>

</html>

</html>`;