import React from 'react';
import { Avatar, Progress } from 'react-native-elements';
import { padding, colors, margin } from '../style'


export default AppAvatar = ({ title, size, onPress, source,containerStyle }) => {
    return (
        <Avatar
            rounded
            size={size}
            source={source}
            title={title}
            onPress={onPress}
            activeOpacity={0.7}
            containerStyle={[containerStyle]}
        />
    );
}

const styles = {
   
}