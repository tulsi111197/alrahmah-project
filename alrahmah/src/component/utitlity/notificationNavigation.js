import React from 'react';
import firebase, { Notification, NotificationOpen } from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage'

class NotificationNavigation extends React.Component {
  async componentDidMount() {
    this.removeNotificationOpenedListener = firebase.notifications().
      onNotificationOpened((notificationOpen) => {
        // Get the action triggered by the notification being opened
        const action = notificationOpen.action;

        // Get information about the notification that was opened
        const notification = notificationOpen.notification;
        console.log('----NotificationOpen---', notification);
        console.log('-----', notification._body);
        this.props.navigation.navigate('donation');
      });
  }
  render() {
    this.props.navigation.navigate('SplashScreen');
    return (<></>);
  }
}

export default NotificationNavigation;