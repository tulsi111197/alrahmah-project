import React from 'react';
import { ListItem } from 'react-native-elements'

export default AppListItem = ({ title, subtitle, onPress, chevron, leftAvatar, leftIcon, subtitleStyle, leftAvatarStyle, containerStyle, rightElement, titleStyle }) => {
    return (
        <ListItem
            title={title}
            titleStyle={titleStyle}
            subtitle={subtitle}
            subtitleStyle={subtitleStyle}
            leftIcon={leftIcon}
            chevron={chevron}
            onPress={onPress}
            rightElement={rightElement}
            containerStyle={containerStyle}
            leftAvatar={leftAvatar}
            leftAvatarStyle={leftAvatarStyle}
        />
    );
}