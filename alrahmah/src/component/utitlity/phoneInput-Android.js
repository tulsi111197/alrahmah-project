import React from 'react';
//import { Input } from 'react-native-elements';
import { colors, compHeight, radius, padding, margin } from '../style'
import { View, Text, TextInput, TouchableOpacity } from 'react-native';

function AppInput({ onPress, code, secureTextEntry, onSubmitEditing, returnKeyType,
    onKeyPress, editable, errorStyle, inputRef, inputStyle, label, autoCapitalize,
    errorMessage, keyboardAppearance, placeholder, onFocus, autoFocus, value,
    keyboardType, inputContainerStyle, containerStyle, onChangeText,
    lang
}) {
    return (
        <View style={{ flex: 1, marginBottom: 10 }} >
            <Text style={styles.labelStyle}>{label}</Text>


            <View style={{ flexDirection: 'row', alignItems: 'center', borderRadius: 5 }}>
                <TouchableOpacity onPress={onPress} style={{ width: '25%', paddingVertical: 13, paddingHorizontal: padding.lg, backgroundColor: colors.inputColor }}>
                    <Text style={{ color: colors.black, fontSize: 16 }} >{code}</Text>
                </TouchableOpacity>
                <View
                    style={{

                        marginLeft: 10,
                        //padding: padding.lg,
                        borderRadius: 5,
                        width: '75%',
                        height: 50,
                        backgroundColor: colors.inputColor
                    }}
                >
                    <TextInput
                        value={value}
                        style={{
                            color: colors.black, fontSize: 16,
                            textAlign: lang =='ar' ? 'right' : 'left'
                        }}
                        placeholder={placeholder}
                        onChangeText={onChangeText}

                    />

                </View>
            </View>
            {/* <Input
ref={inputRef}
autoCapitalize={autoCapitalize}
keyboardType={keyboardType}
keyboardAppearance={keyboardAppearance}
onSubmitEditing={onSubmitEditing}
secureTextEntry={secureTextEntry}
errorMessage={errorMessage}
errorStyle={[styles.errorStyle, errorStyle]}
autoCorrect={false}
autoFocus={true}
value={value}
label={label}
labelStyle={styles.labelStyle}
onFocus={onFocus}
inputStyle={[styles.inputStyle, inputStyle]}
autoFocus={autoFocus}
placeholderTextColor={colors.textColor}
placeholder={placeholder}
inputContainerStyle={[styles.inputContainerStyle, inputContainerStyle]}
containerStyle={[styles.containerStyle, containerStyle]}
onChangeText={onChangeText}
editable={editable}
onKeyPress={onKeyPress}
returnKeyType={returnKeyType}
/> */}
        </View>
    )
};
const styles = {
    inputContainerStyle: {
        marginTop: margin.md,
        marginBottom: margin.xl,
        borderColor: 'transparent',
        borderRadius: radius.sm,
        backgroundColor: colors.inputColor,
    },
    containerStyle: {
        width: '100%',
        paddingHorizontal: 0
    },
    inputStyle: {
        padding: padding.md,
    },
    labelStyle: {
        color: colors.labelColor,
        fontWeight: '500',
        fontSize: 16,
        marginBottom: margin.sm
    },
    errorStyle: {
        paddingTop: -10
    }
};


export default AppInput;