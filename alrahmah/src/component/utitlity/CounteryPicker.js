

function RednderCounteryCodes() {
    if () {
        return (
            <Modal
                visible={this.state.iscodemodel}
                transparent={true}

            >
                <View style={{
                    backgroundColor: 'rgba(0,0,0,0.8)', flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'flex-end'
                }} >
                    <Icon name='ios-close' style={{ fontSize: 40, color: 'white', marginRight: 'auto', marginLeft: 'auto' }} onPress={() => this.setState({ iscodemodel: false })} />

                    <View style={{
                        width: '100%',
                        height: 500,
                        borderRadius: radius.xl,
                        padding: padding.md,
                        backgroundColor: 'bfb6b6'
                    }}>
                        <AppInput label={this.props.lang.search}
                            labelStyle={{ color: colors.black }}
                            inputStyle={{ borderColor: colors.offBlack }}
                            placeholderTextColor={'rgba(0,0,0,0.3)'}
                            placeholder={this.props.lang.search_by_contry_name_and_code}
                            onChangeText={(text) => this.searchCode(text)}
                        />

                        <FlatList
                            data={this.state.filterCounterys}
                            renderItem={({ item }) =>
                                <View style={{ justifyContent: 'center', alignItems: 'flex-start', marginBottom: margin.md }}>
                                    <TouchableOpacity style={{
                                        borderWidth: 1,
                                        padding: padding.lg,
                                        borderRadius: radius.lg,
                                        width: '100%',
                                        backgroundColor: colors.offBlack
                                    }}
                                        onPress={() => {
                                            if (!this.state.forPhone)
                                                this.setState({ iscodemodel: false, countery: item.name })
                                            else
                                                this.setState({
                                                    iscodemodel: false,
                                                    code: item.dial_code,
                                                    countery: item.name,
                                                    flag: item.flag,
                                                    filterCounterys: Counterys

                                                })
                                        }
                                        }
                                    >
                                        {this.state.forPhone ?
                                            <Text style={{ color: 'white' }} > {`${item.flag} ${item.dial_code} ${item.name} `}</Text>
                                            :
                                            <Text style={{ color: 'white' }} > {`${item.flag} ${item.name} `}</Text>
                                        }


                                    </TouchableOpacity>
                                </View>
                            }
                        />
                    </View>


                </View>
            </Modal>
        )
    }
    else
        return null
}