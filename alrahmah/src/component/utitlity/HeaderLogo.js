import React from 'react';
import { TouchableOpacity, Image, View,Text } from 'react-native';
import { baseStyle, colors,margin,padding } from '../style';

export default HeaderLogo = ({imgStyle}) => {
    return (
        <View style={styles.headerContainer}>
            <Image
                source={require('../assets/header_logo.png')}
                style={[styles.imgStyle,imgStyle]}
            />
        </View>
    );
}

const styles = {
headerContainer: {
    flex:1,
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    marginLeft:margin.lg
},
imgStyle:{
    height: 100, 
    width: 100, 
    resizeMode: 'contain'
}
}