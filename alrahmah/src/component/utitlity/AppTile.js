import React from 'react';
import { View, Text } from 'react-native';
import { Tile } from 'react-native-elements'
import { fonts, radius,colors} from '../style';


export default AppTile = ({ source, title, caption, height, width, captionStyle, onPress, titleStyle, containerStyle,overlayStyle,icon,imageContainerStyle }) => {
    return (
        <View style={{flex:1}}>
            <Tile
                featured
                imageSrc={source}
                width={width}
                height={height}
                icon={icon}
                imageContainerStyle={[styles.imgStyle,imageContainerStyle]}
                iconContainerStyle={{backgroundColor:colors.red}}
                containerStyle={[styles.containerStyle, containerStyle]}
                overlayContainerStyle={[styles.overlayStyle,overlayStyle]}
                titleStyle={[styles.titleStyle, titleStyle]}
                title={title}
                caption={caption}
                onPress={onPress}
                captionStyle={{ fontSize: fonts.lg }, captionStyle}
            />
            </View>
    );
}

const styles = {
    overlayStyle: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        position: 'relative',
    },
    titleStyle: {
        fontSize: 10,
        fontWeight: 'bold',
        textAlign: 'left',

    },
    containerStyle: {
        flex:1,
        alignSelf: 'center',
        borderRadius:radius.lg,
        marginLeft:20,
        marginRight:20,
       
    },
    imgStyle:{
        borderRadius:radius.lg,
      
    }
}