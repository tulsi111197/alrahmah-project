
import React, { Component } from 'react'
import { View, Switch} from 'react-native';
import { colors } from '../style';
import AsyncStorage from '@react-native-community/async-storage';

export default class AppSwitch extends Component{
    constructor() {
        super();
        this.state = {
            switch1Value: false,
        }
    }
     async componentDidMount(){
        const status = await AsyncStorage.getItem('notification_status');
        this.setState({switch1Value: status});
    }
    toggleSwitch1 = async (value) => {
        const status = await AsyncStorage.getItem('notification_status');
        if(this.state.switch1Value === true)
        {
            this.setState({ switch1Value: false })
            AsyncStorage.setItem('notification_status','false');
        }
        else{
            this.setState({ switch1Value: true })
            AsyncStorage.setItem('notification_status','true');
        }
        
    }
    render(){
   return (
         <Switch
         onValueChange = {this.toggleSwitch1}
         value = {this.state.switch1Value}
         style={{ transform: [{ scaleX: .8 }, { scaleY: .8 }] }}
         onTintColor="#4ABEA1"
        />
   )
}
}
const styles = {
   
}