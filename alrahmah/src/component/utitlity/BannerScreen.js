import React, { Component } from 'react';
import { View, Image, ImageBackground} from 'react-native';

export default BannerScreen = ({sliderImg}) => {
    return (
        <View>
            <ImageBackground
                source={sliderImg}
                style={styles.imgStyle}>
                <Image
                    source={require('../assets/mask.png')}
                    style={styles.imgStyle}
                />
            </ImageBackground>
            </View>
    );
}

const styles = {
    imgStyle: {
        height: 500,
        width: 410,
    },
}