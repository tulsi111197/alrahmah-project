import React, { Component } from 'react';
import ImageSlider from 'react-native-image-slider';
import { View, TouchableHighlight, TouchableOpacity } from 'react-native';
import { Text } from 'react-native-elements'
import { baseStyle, margin, colors, fonts, padding } from '../style/RootStyle';
import BannerScreen from '../utitlity/BannerScreen';
import AppButton from '../utitlity/AppButton';

export default class AppImageSlider extends Component {

    render() {
        const images = [
            'https://getingear.ie/images/learn-drive-automatic-car.jpg',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSv8KUbVSMC8RGXxxj18Me-lMfOvB-yFLNpD9c-cWV83lrpCRG6&s',
            'https://s1.cdn.autoevolution.com/images/news/parents-unaware-of-dangers-of-teen-texting-and-driving-52279_1.jpg',

        ];
        return (
            <View style={baseStyle.appContainer}>
                <ImageSlider
                    loopBothSides
                    autoPlayWithInterval={5000}
                    images={images}
                    customSlide={({ index }) => (
                        <View key={index} style={styles.customSlide}>
                            <BannerScreen
                                sliderImg={images}
                            />
                        </View>
                    )}

                    customButtons={(position, move) => (
                        <View style={styles.buttons}>
                            {images.map((image, index) => {
                                return (
                                    <TouchableHighlight
                                        key={index}
                                        underlayColor="#ccc"
                                        onPress={() => move(index)}
                                        style={styles.button}
                                    >
                                        <Text style={position === index && styles.buttonSelected}></Text>
                                    </TouchableHighlight>
                                );
                            })}
                        </View>
                    )}
                />


                <View style={{ padding: padding.lg, marginBottom: margin.xl }}>
                    <Text h3 style={styles.headingStyle} >GIVE A HELPING HAND TO NEEDy</Text>
                    <AppButton
                        title="Create New Account"
                        onPress={() => this.props.navigation.navigate('create')}
                    />
                    <View style={[baseStyle.flexRow, baseStyle.marginTopLg, baseStyle.justify]}>
                        <Text style={styles.bottomText}>Already have a Account?</Text>
                        <TouchableOpacity><Text style={styles.loginText}>Log In</Text></TouchableOpacity>
                    </View>
                </View>
            </View>

        );
    }
}

const styles = {

    customSlide: {
        backgroundColor: colors.white,
        flex: 1

    },
    buttonSelected: {
        backgroundColor: colors.black
    },
    button: {
        backgroundColor: colors.green,
        width: 30,
        height: 5,
        borderRadius: 8,
        margin: 5,
    },
    buttons: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: margin.xl

    },
    headingStyle: {
        color: '#3A3A3A',
        textTransform: 'uppercase',
        textAlign: 'center',
        marginBottom: margin.lg
    },
    loginText: {
        color: colors.green,
        fontSize: fonts.lg,

    },
    bottomText: {
        fontSize: fonts.lg,
        textAlign: 'center'
    }
}