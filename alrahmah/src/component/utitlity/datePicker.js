import React from 'react';
import { Input } from 'react-native-elements';
import { colors, compHeight, radius, padding, margin } from '../style'
import { View, Text, Dimensions } from 'react-native';
import DatePicker from 'react-native-datepicker';
export default datePicker = ({ placeholder, mindate,
    maxdate, onDateChange,
    date, label,
    lang
}) => {
    return (
        <View>
            <Text style={styles.labelStyle, {
                fontSize: 15,
                textAlign: lang == 'ar' ? 'right' : 'left'
            }}>{label}</Text>
            <View style={styles.inputContainerStyle}  >

                <DatePicker

                    date={date}
                    mode="date"
                    placeholder={placeholder}
                    format="DD-MM-YYYY"
                    minDate={mindate}
                    maxDate={maxdate}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    showIcon={false}
                    style={{
                        width: '100%',
                        textAlign: lang == 'ar' ? 'right' : 'left'
                    }}
                    customStyles={{
                        dateInput: {
                            textAlign: 'left',
                            borderWidth: 0,
                            fontSize: 30,
                            marginLeft: 5,
                            alignItems: lang == 'ar' ? 'flex-end' : "flex-start",
                        },
                        dateText: {
                            textAlign: 'left',
                            marginLeft: 0,
                            fontSize: 15,
                        },
                        placeholderText: {
                            color: colors.textColor,
                            fontSize: 16,
                            textAlign: lang == 'ar' ? 'right' : 'left'
                        }


                        // ... You can check the source to find the other keys.
                    }}
                    onDateChange={onDateChange}
                />
            </View>
        </View>
    )
};
const styles = {
    inputContainerStyle: {
        padding: 5,
        marginTop: margin.md,
        marginBottom: margin.lg,
        borderColor: 'transparent',
        borderRadius: radius.sm,
        backgroundColor: colors.inputColor,
        height: 50,
    },
    containerStyle: {
        width: '100%',
        paddingHorizontal: 0
    },
    inputStyle: {
        padding: padding.md,
    },
    labelStyle: {
        color: 'black',
        fontWeight: '500',
        fontSize: 30
    },
    errorStyle: {
        paddingTop: -10
    }
};

