
import React from 'react';
import { View, StyleSheet, Text, Image, TouchableWithoutFeedback, Platform } from 'react-native';
import { colors, fonts, padding } from '../style';
import FastImage from 'react-native-fast-image'
/**
* Override styles that get passed from props
**/
// propStyle = (percent, base_degrees) => {
// const rotateBy = base_degrees + (percent * 3.6);
// return {
// transform: [{ rotateZ: `${rotateBy}deg` }]
// };
// }

// renderThirdLayer = (percent) => {
// if (percent > 50) {
// /**
// * Third layer circle default is 45 degrees, so by default it occupies the right half semicircle.
// * Since first 50 percent is already taken care by second layer circle, hence we subtract it
// * before passing to the propStyle function
// **/
// return <View style={[styles.secondProgressLayer, propStyle((percent - 50), 45)]}></View>
// } else {
// return <View style={styles.offsetLayer}></View>
// }
// }

export default AppCircularImg = ({ source, onPress, profileCircle, onLoadEnd, onLoadStart }) => {
  return (
    <TouchableWithoutFeedback onPress={onPress} >
      <View style={[styles.profileCircle, profileCircle]}>
        {
          Platform.OS === 'android'
            ?
            <Image source={source} style={{ height: 100, width: 100, borderRadius: 100 / 2 }}
              onLoadStart={onLoadStart}
              onLoadEnd={onLoadEnd} />
            :
            <FastImage source={source} style={{ height: 100, width: 100, borderRadius: 100 / 2 }}
              onLoadStart={onLoadStart}
              onLoadEnd={onLoadEnd} />
        }
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 120,
    height: 120,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },

  profileCircle: {
    borderColor: colors.white,
    borderWidth: 4,
    borderRadius: 120 / 2,
    padding: padding.sm
  }
  // firstProgressLayer: {
  // width: 120,
  // height: 120,
  // borderWidth: 4,
  // borderRadius: 100,
  // position: 'absolute',
  // borderLeftColor: 'transparent',
  // borderBottomColor: 'transparent',
  // /borderRightColor: colors.white,
  // borderTopColor: colors.white,
  // transform: [{ rotateZ: '-135deg' }]
  // },
  // secondProgressLayer: {
  // width: 120,
  // height: 120,
  // position: 'absolute',
  // borderWidth: 4,
  // borderRadius: 100,
  // borderLeftColor: 'transparent',
  // borderBottomColor: 'transparent',
  // borderRightColor: colors.white,
  // borderTopColor: colors.white,
  // transform: [{ rotateZ: '45deg' }]
  // },
  // offsetLayer: {
  // width: 120,
  // height: 120,
  // position: 'absolute',
  // borderWidth: 4,
  // borderRadius: 100,
  // borderLeftColor: 'transparent',
  // borderBottomColor: 'transparent',
  // borderRightColor: colors.white,
  // borderTopColor: colors.white,
  // transform: [{ rotateZ: '-135deg' }]
  // },
  // display: {
  // position: 'absolute',
  // fontSize: fonts.lg,
  // fontWeight: 'bold'
  // }
});