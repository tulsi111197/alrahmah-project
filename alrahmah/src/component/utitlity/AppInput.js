import React from 'react';
import { Input } from 'react-native-elements';
import { colors, compHeight, radius, padding, margin } from '../style'
import { View } from 'react-native';

export default AppInput = ({ secureTextEntry, onSubmitEditing,
  labelSearchStyle, returnKeyType, onKeyPress, editable, errorStyle,
  inputRef, inputStyle, label, autoCapitalize, errorMessage, keyboardAppearance,
  placeholder, onFocus, autoFocus, value, keyboardType, inputContainerStyle,
  containerStyle, onChangeText, lang
}) => {
  return (
    <View style={{ flex: 1, marginBottom: 10 }}  >
      <Input
        ref={inputRef}
        autoCapitalize={autoCapitalize}
        keyboardType={keyboardType}
        // keyboardType='numeric'
        keyboardAppearance={keyboardAppearance}
        onSubmitEditing={onSubmitEditing}

        secureTextEntry={secureTextEntry}
        errorMessage={errorMessage}
        errorStyle={{
          ...styles.errorStyle, errorStyle,
          textAlign: lang == 'ar' ? 'right' : 'left'
        }}
        autoCorrect={false}
        autoFocus={true}
        value={value}
        label={label}
        labelStyle={{ ...styles.labelStyle, labelSearchStyle, textAlign: lang == 'ar' ? 'right' : 'left' }}
        onFocus={onFocus}
        inputStyle={{
          padding: padding.md,
          ...inputStyle,
          textAlign: lang == 'ar' ? 'right' : 'left'
        }}
        autoFocus={autoFocus}
        placeholderTextColor={colors.textColor}
        placeholder={placeholder}
        inputContainerStyle={[styles.inputContainerStyle, inputContainerStyle]}
        containerStyle={[styles.containerStyle, containerStyle]}
        onChangeText={onChangeText}
        editable={editable}
        onKeyPress={onKeyPress}
        returnKeyType={returnKeyType}
      />
    </View>
  )
};
const styles = {
  inputContainerStyle: {
    marginTop: margin.md,
    marginBottom: margin.xl,
    borderColor: 'transparent',
    borderRadius: 100,
    paddingVertical: padding.md,
    paddingLeft: padding.md,
    backgroundColor: colors.inputColor,
  },
  containerStyle: {
    width: '100%',
    paddingHorizontal: 0,
    marginBottom: margin.xl
  },
  inputStyle: {
    padding: padding.md,
  },
  labelStyle: {
    color: colors.labelColor,
    fontWeight: '500'
  },
  errorStyle: {
    paddingTop: -10
  }
};

