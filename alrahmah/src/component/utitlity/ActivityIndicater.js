import React from 'react';
import {
    ActivityIndicator
} from 'react-native';

export default activityIndicate = () => {
    return (
        <ActivityIndicator size="large" color="#4ABEA1" />
    )
}