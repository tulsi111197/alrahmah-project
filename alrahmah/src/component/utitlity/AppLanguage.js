import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { padding, margin } from '../style';
import { connect } from 'react-redux';
import { setLanguageArabic, setLanguageEnglish } from '../store/reducer/action/userAction';
import { setStaticLangArabic, setStaticLangEnglish } from '../store/reducer/action/RootReducerAction';
import HeaderLogo from '../utitlity/HeaderLogo'

class AppLanguage extends Component {

    // componentDidMount() {
    //     this.props.setLanguageEnglish();
    //     this.props.setStaticLangEnglish();
    // }

    updateLanguage = ({ lang }) => {
        if (lang == 'ar') {
            this.props.setStaticLangArabic();
            this.props.setLanguageArabic();
            return 0;
        }
        this.props.setLanguageEnglish();
        this.props.setStaticLangEnglish();
        return 0;
    }

    render() {
        return (
            <View style={styles.languageWrapper}>


                <TouchableOpacity onPress={() => this.updateLanguage({ lang: 'en' })}>
                    <Text style={{
                        ...styles.languageTitle, color: this.props.selected_language == 'en' ?
                            'green'
                            :
                            'black'
                    }}>English</Text>
                </TouchableOpacity>

                <Text style={{ paddingHorizontal: padding.md }}>/</Text>
                <TouchableOpacity onPress={() => this.updateLanguage({ lang: 'ar' })}>
                    <Text style={{
                        ...styles.languageTitle, color: this.props.selected_language == 'ar' ?
                            'green'
                            :
                            'black'
                    }}>لغة</Text>
                </TouchableOpacity>

            </View>
        )
    }

}

const mapStateToProp = state => {
    return {
        selected_language: state.userDetail.userlanguage,
        userlang: state.rootReducer.userLanguage,
    }
}

export default connect(mapStateToProp, { setLanguageArabic, setLanguageEnglish, setStaticLangArabic, setStaticLangEnglish })(AppLanguage);

const styles = {
    languageWrapper: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginRight: margin.lg,
    },
    languageTitle: {
        fontSize: 16,
    }
}