let baseuri = 'http://104.248.3.101/alr/public/api/user/';
//let baseuri = 'http://104.248.3.101/alrstaging/public/api/user/'; // for staging

export const tokenforslider = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiNTA4NjFjNWVlYzk3N2E5ODNjOWI2MzA4ZDdkYzgwNGRkNTE5OTRhN2FlODc1YTQ1NjIxODFjNmI2ZDUwY2MyZTk1ZjlmYTAxZjQ0NTc2YzEiLCJpYXQiOjE1ODU2NjI2MzMsIm5iZiI6MTU4NTY2MjYzMywiZXhwIjoxNjE3MTk4NjMzLCJzdWIiOiIyOSIsInNjb3BlcyI6W119.hq4xGOM10qIKtqNn3uFh5K1l1KrHUIwrW4R3PRnb-IPFzGhWp8QRFvsrSVfdyD2RfeKxJE27qyEjbZ6yTh65iOZKUobXVD4dFo6zCsX1BpChXDQgYWyyZpLlc0qIhvVrY2qkUR_kaaaWluZIHocB84M_6XV4WyhWDsUQDN_71HDZWGw0d8bGEowZla636HTNtw0IyQ7tFBFmzRRdz9c5li_2x38bBjtBf1hg7YrCTDG8AptWh7p9CQwLXJeF1HoohR4_PblEJilL7L9OdnA3bR8W9n0cop10KT7MNlyvuOcgtmpCgiDA--1Hjrf9I8JiVlwxhvKKjodAayc87ToJpA2ocvqq2lZ2t35Z_mnjqEj4MZc6DCiZjE6ntspDTLSFtZiXjIsHtX8MbFTDN9MlAS9W0UPlWLU5zmh_NNTWeit5D5VpKrpuZ5B81kYxAUF0vvKorEhIyNZ6IyAJAsLiinOTUiq_jWCC6o9sdkd7zl5KaplGIlUHcV-13nfMI9Kn-FthLx2V344N6KCTaazpyO5QG-Sf23lja_VCGsMGoypysAK9bYlcGAMq0Li72gKwZgA3VMS-9k5Rz89NxXBgsbLqae6VB9-zi5YQHeIzo8nd3w2kmB_i0_ldl1T_eQacsrIPXzvt4x54I7v-4vxXdj-XJyy22ijYIpdYl5nDYIg';
export const userauthentication = 'https://www.getpostman.com/collections/100189284b0ba0f193ca';

export const dashboard = `${baseuri}dashboard`;
export const campaingdetails = `${baseuri}campaing-details/`;
export const mydonationuri = `${baseuri}my-donations`;
export const login = `${baseuri}login`;
export const verifyotp = `${baseuri}verify-otp`;
export const signup = `${baseuri}register`;
export const slider = `${baseuri}banner`;
export const campadonation = `${baseuri}campaing-donate`;
export const search = `${baseuri}search-campaign?query=`;
export const programlist = `${baseuri}programs-list`;
export const prgramdetail = `${baseuri}programs-details/`;
export const volunteer = `${baseuri}become-volunteer`
export const profileupdate = `${baseuri}profile-update`;
export const blogdetail = `${baseuri}blogs-details/`;
export const bloglist = `${baseuri}blogs-list`;
export const myprogramlist = `${baseuri}my-programs`;
export const storylist = `${baseuri}stories-list`;
export const storydetail = `${baseuri}stories-details/`;
export const programdonate = `${baseuri}programs-donate`;
export const reset_password = `${baseuri}reset-password`;
export const forgot_password = `${baseuri}forgot-password`;
export const resend_otp = `${baseuri}resend-otp`;
export const volunteer_list = `${baseuri}volunteer-list`;
export const notificationApi = 'http://104.248.3.101/alrehamah/public/api/notification-test-send';